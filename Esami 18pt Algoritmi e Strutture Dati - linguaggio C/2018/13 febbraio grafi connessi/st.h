//
// Created by seren on 13/06/2023.
//

#ifndef APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_ST_H
#define APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_ST_H

#include "point.h"

typedef struct hashtable *ST;

ST STinit(int n);
void STfree(ST table);
int STsearchbyname(ST table, char *x);
void STprint(ST table);
//int STinsert(ST table, point x);
//point STgetbyindex(ST table, int index);
void STsort(ST table);
#endif //APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_ST_H

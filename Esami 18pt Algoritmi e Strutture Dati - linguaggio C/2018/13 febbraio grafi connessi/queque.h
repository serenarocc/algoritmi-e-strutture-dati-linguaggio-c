//
// Created by seren on 13/06/2023.
//

#ifndef APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_QUEQUE_H
#define APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_QUEQUE_H
#include "Grafo.h"

typedef struct queue *Q;

Q QUEUEinit();
void QUEUEput(Q q, Edge e);
int QUEUEempty(Q q);
Edge QUEUEget(Q q);
#endif //APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_QUEQUE_H

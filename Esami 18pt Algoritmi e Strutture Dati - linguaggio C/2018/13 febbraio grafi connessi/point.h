//
// Created by seren on 13/06/2023.
//

#ifndef APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_POINT_H
#define APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_POINT_H
typedef struct{
    char *id;
    float x,y;
}point;

point pointCreate(char *id, float x, float y);
float distance(point a, point b);

#endif //APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_POINT_H

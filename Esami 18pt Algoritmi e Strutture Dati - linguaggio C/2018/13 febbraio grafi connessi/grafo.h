//
// Created by seren on 13/06/2023.
//

#ifndef APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_GRAFO_H
#define APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_GRAFO_H

#include <stdio.h>

typedef struct{
    int v;
    int w;
    int wt;
}Edge;
typedef struct grafo *Graph;

Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2, int wt);
void GRAPHremoveE(Graph G, int id1, int i2, int wt);
Graph GRAPHreadFile(FILE *in);
void GRAPHladjcreate(Graph G);
void GRAPHprintmatrix(Graph G);
void GRAPHprintlist(Graph G);
int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetE(Graph G);
int GRAPHgetV(Graph G);
int getDiameter(Graph G, int id);
#endif //APPELLO18_2018_FEB13_GRAFI_CONNESSIONE_GRAFO_H

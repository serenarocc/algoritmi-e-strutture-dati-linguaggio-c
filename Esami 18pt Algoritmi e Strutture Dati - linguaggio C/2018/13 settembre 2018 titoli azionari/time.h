//
// Created by seren on 12/04/2023.
//

#ifndef APPELLO18_2018_SETT13_TITOLI_AZIONARI_TIME_H
#define APPELLO18_2018_SETT13_TITOLI_AZIONARI_TIME_H
typedef struct{  //QUASI ADT
    int a;//anno
    int m;//mese
    int g;//gg
}data_t;

typedef struct{
    int h;//ora
    int m;//minuti
}ora_t;


data_t getData(char *s);
void printData(data_t t);
int datacmp(data_t d1, data_t d2);

ora_t getOra(char *s);
void printOra(ora_t t);
int oracmp(ora_t o1, ora_t o2);

#endif //APPELLO18_2018_SETT13_TITOLI_AZIONARI_TIME_H

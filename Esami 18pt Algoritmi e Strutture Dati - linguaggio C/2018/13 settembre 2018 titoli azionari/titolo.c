//
// Created by seren on 12/04/2023.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "titolo.h"
#include "quotazioni.h"

struct titolo_{ //ADT 1 CLASSE
    char *cod;
    float min;//quotazione minima
    float max;//quotazione massima
    BSTquotazioni bst; //quotazioni giornaliere
};

char *getKey(titolo t){
    return t->cod;
}


titolo titoloNEW(char *cod){ //creazione nuovo nodo (titolo nuovo) da mettere nella lista
    titolo t;
    t = malloc(sizeof(*t));
    t->cod = strdup(cod);
    t->min = t->max = -1.0;
    t->bst = BSTquotazioneInit();
    return t;
}


int titolocmp(char *t1, char *t2){
    return strcmp(t1,t2);
}


void titoloStore(FILE *out, titolo t){
    if(t == NULL)
        return;
    fprintf(out, "%s: MIN=%.2f MAX=%.2f\n",t->cod,t->min, t->max);
    BSTquotazioniStore(out, t->bst);
    printf("\n\n");
}

//
// Created by seren on 12/04/2023.
//

#ifndef APPELLO18_2018_SETT13_TITOLI_AZIONARI_QUOTAZIONE_H
#define APPELLO18_2018_SETT13_TITOLI_AZIONARI_QUOTAZIONE_H

#include "time.h"
#include <stdio.h>


typedef struct quotazione_{
    data_t data;
    float val;
    int q;//quantita
}quotazione;

quotazione quotazioneNEW(data_t d);
void quotazioneStore(FILE *out, quotazione t);
data_t quotazioneGetData(quotazione t);
float quotazioneGetVal(quotazione t);
int quotazioneCheckNull(quotazione t);
quotazione quotazioneSetNull();


#endif //APPELLO18_2018_SETT13_TITOLI_AZIONARI_QUOTAZIONE_H

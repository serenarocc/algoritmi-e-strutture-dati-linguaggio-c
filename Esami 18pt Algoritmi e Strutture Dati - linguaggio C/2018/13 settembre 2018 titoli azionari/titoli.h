//
// Created by seren on 12/04/2023.
//

#ifndef APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLI_H
#define APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLI_H


#include "titolo.h"

typedef struct titoli_ *titoli;


titoli titoliInit();
titolo titoliSearch(titoli list, char *cod);
void titoliInsert(titoli list, titolo t);
void titoliStore(FILE *out, titoli list);
int listempty(titoli t);
void titoliFree(titoli t);


#endif //APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLI_H

//
// Created by seren on 12/04/2023.
//

#include "quotazioni.h"

typedef struct nodo_ *link;
struct nodo_{
    quotazione q;
    link l,r;
    int n;
};
struct BSTquotazioni_{
    link root;
    int count;
};



BSTquotazioni BSTquotazioneInit(){  //C'E UNA LISTA DELLE QUOTAZIONI??
    BSTquotazioni bst;
    bst = malloc(sizeof(*bst));
    bst->root = NULL;
    bst->count = 0;
    return bst;
}

static void treePrint(FILE *out, link root){
    if(root == NULL)
        return;
    treePrint(out, root->l);
    quotazioneStore(out, root->q);
    treePrint(out, root->r);
}


void BSTquotazioniStore(FILE *out, BSTquotazioni bst){
    if(bst->root == NULL)
        return;
    treePrint(out,bst->root);
}


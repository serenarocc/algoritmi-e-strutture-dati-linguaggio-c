//
// Created by seren on 12/04/2023.
//

#ifndef APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLO_H
#define APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLO_H
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "quotazione.h"

typedef struct titolo_ *titolo;//adt 1 classe

char *getKey(titolo t);
int titolocmp(char *t1, char *t2);
titolo titoloNEW(char *cod);
void titoloStore(FILE *out, titolo t);


quotazione ricercaQuotazione(titolo t, data_t d);
void titoloInsertTransazione(titolo t, data_t d, float val, int qta);
void titolominMaxRange(titolo t, float *min, float *max, data_t d1, data_t d2);
void titoloMinMax(titolo t, float *min, float *max);
void titolobilancia(titolo t, int s);



#endif //APPELLO18_2018_SETT13_TITOLI_AZIONARI_TITOLO_H

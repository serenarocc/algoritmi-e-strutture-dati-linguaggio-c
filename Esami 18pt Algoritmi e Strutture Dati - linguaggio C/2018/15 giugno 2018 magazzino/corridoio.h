//
// Created by seren on 20/04/2023.
//

#ifndef APPELLO18_2018_GIUGNO15_MAGAZZINO_CORRIDOIO_H
#define APPELLO18_2018_GIUGNO15_MAGAZZINO_CORRIDOIO_H

#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include "scaffale.h"//DATO CHE HO LA COLLEZIONE DI SCAFFALE DENTRO LA STRUCT DEVO AVETRE LA IMPORT DI SCAFFALE

typedef struct corridoio *CORRIDOIO;//ADT 1 CLASSE


CORRIDOIO  CORRIDOIOinit(int nSC);
SCAFFALE   CORRIDOIOgetSCAFFALE(CORRIDOIO c, int sc);
PACCO      CORRIDOIOgetPACCO(CORRIDOIO c, int sc, int slot, int estrai);
void       CORRIDOIOstore(FILE *out, CORRIDOIO c);
int        CORRIDOIOgetNSC(CORRIDOIO c);
int        CORRIDOIOplacePACCO(CORRIDOIO c, PACCO p, int sc, int slot);
int        CORRIDOIOcheck(CORRIDOIO c, int sc, int slot);


#endif //APPELLO18_2018_GIUGNO15_MAGAZZINO_CORRIDOIO_H

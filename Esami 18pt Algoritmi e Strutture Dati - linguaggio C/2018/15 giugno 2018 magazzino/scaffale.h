//
// Created by seren on 20/04/2023.
//

#ifndef APPELLO18_2018_GIUGNO15_MAGAZZINO_SCAFFALE_H
#define APPELLO18_2018_GIUGNO15_MAGAZZINO_SCAFFALE_H
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include "pacco.h" //devo includere pacco perche' mi serve dentro la struttura

#define N_SLOT 4

typedef struct scaffale *SCAFFALE;


SCAFFALE   SCAFFALEinit();
PACCO      SCAFFALEgetPACCO(SCAFFALE s, int slot, int estrai);
void       SCAFFALEstore(FILE *out, SCAFFALE s);
int        SCAFFALEslotOccupati(SCAFFALE s);
int        SCAFFALEplacePACCO(SCAFFALE s, PACCO p, int slot);
int        SCAFFALEcheck(SCAFFALE s, int slot);
int        SCAFFALEgetSlotLibero(SCAFFALE s);

#endif //APPELLO18_2018_GIUGNO15_MAGAZZINO_SCAFFALE_H

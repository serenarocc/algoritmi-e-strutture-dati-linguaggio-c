//
// Created by seren on 20/04/2023.
//

#ifndef APPELLO18_2018_GIUGNO15_MAGAZZINO_BST_H
#define APPELLO18_2018_GIUGNO15_MAGAZZINO_BST_H


#include<string.h>
#include<stdlib.h>
#include<stdio.h>
//------------------------
#include "pacco.h"//devo importarlo xke mi serve PACCO nella struct

typedef struct bst *BST;


int           BSTempty(BST bst);
PACCO         BSTsearch(BST bst, char *cod);
void          BSTremove(BST bst, char *cod);
void          BSTinsert(BST bst, PACCO p);
void          BSTstore(BST bst, FILE *fp);
BST           BSTinit();

#endif //APPELLO18_2018_GIUGNO15_MAGAZZINO_BST_H

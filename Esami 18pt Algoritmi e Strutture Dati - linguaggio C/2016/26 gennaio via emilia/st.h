//
// Created by seren on 12/06/2023.
//

#ifndef APPELLO18_2016_GENNAIO26_VIA_EMILIA_ST_H
#define APPELLO18_2016_GENNAIO26_VIA_EMILIA_ST_H
#include "string.h"

typedef struct{
    char *name;
    int pop; //popoloazione
    int dist; //distanza
}Item;


Item ItemCreate(char *id, int pop, int dist);

typedef struct hashtable *ST;

ST STinit(int n);
void STfree(ST table);
int STsearchbyname(ST table, char *x);
void STprint(ST table);
int STinsert(ST table, Item x);
Item STgetbyindex(ST table, int index);
void STsort(ST table);


#endif //APPELLO18_2016_GENNAIO26_VIA_EMILIA_ST_H

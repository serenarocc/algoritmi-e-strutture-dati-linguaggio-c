#include <stdio.h>
#include <stdlib.h>

#include "grafo.h"

int main(int argc, char **argv) {
    Graph G;
    int *sol,dimSol=0;
    FILE *in;  //richiesta 1:leggere grafo e memorizzarlo in strutture dati
    in=fopen("../grafo.txt","r");
    G= GRAPHreadFile(in);
    GRAPHprintmatrix(G);
    fclose(in);


    //richiesta 2: verifica
    in= fopen("../soluzione.txt","r");
    sol = readSet(in, G, &dimSol);
    fclose(in);
    if(isIndipendent(G, sol, dimSol)==1)
        printf("L'insieme in input e' un insieme indipendente.\n");
    else
        printf("L'insieme in input non e' un insieme indipendente.\n");


    //richiesta 3: ottimizzazione
    generaMaximalIndipendentSet(stdout, G);
    return 0;
}

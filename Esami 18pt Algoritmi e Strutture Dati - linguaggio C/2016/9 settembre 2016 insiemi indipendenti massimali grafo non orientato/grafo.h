//
// Created by seren on 11/06/2023.
//

#ifndef APPELLO18_2016_SETT9_INSIEMI_INDIPENDENTI_MASSIMALI_GRAFO_NON_ORIENTATO_GRAFO_H
#define APPELLO18_2016_SETT9_INSIEMI_INDIPENDENTI_MASSIMALI_GRAFO_NON_ORIENTATO_GRAFO_H


typedef struct {
    int v;
    int w;
}Edge;  //arco

typedef struct grafo *Graph; //adt 1 classe



Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2);
void GRAPHremoveE(Graph G, int id1, int i2);
Graph GRAPHreadFile(FILE *in);
void GRAPHprintmatrix(Graph G);
int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetE(Graph G);
int GRAPHgetV(Graph G);
int isIndipendent(Graph G, int *sol, int dim);
int *readSet(FILE *in, Graph G, int *dim);
void generaMaximalIndipendentSet(FILE *out, Graph G);
#endif //APPELLO18_2016_SETT9_INSIEMI_INDIPENDENTI_MASSIMALI_GRAFO_NON_ORIENTATO_GRAFO_H

//
// Created by seren on 28/03/2023.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "st.h"
struct hashtable{
    int n;
    int max;
    char **v;
};


ST STinit(int max){//come parametro ha il num di vertici
    ST table;
    table = malloc(sizeof(*table));
    table->max = max;//come parametro ha il num di vertici
    table->n = 0;
    table->v = malloc(max* sizeof(char*));//vettore lungo il num di vertici di tipo char
    return table;
}

void STfree(ST table){
    int i;
    free(table->v);
    free(table);
}

int STsearchbyname(ST table, char *key){ //in key ho il nome del vertice che devo inserire in ST
    int i;
    for(i=0; i<table->n; i++){//per tutti i vertici che ho
        if(strcmp(table->v[i],key) == 0)//trovo corrispondenza
            return i;//ritorno la posizione del vertice dentro il vettore
    }
    return -1;//non c'è corrispondenza
}

char *STgetbyindex(ST table, int index){
    return table->v[index];
}

void STprint(ST table){
    int i;
    for(i=0; i<table->n; i++)
        printf("%2d | %10s\n",i,table->v[i]);
    printf("\n\n");
}

int STinsert(ST table, char *x) { //x è la stringa del nome del vertice
    table->v[table->n++] = strdup(x);//copio il nome del vertice che ho a disposizione
    return 1;
}

int STgetN(ST table){
    return table->n;
}

#include <stdio.h>
#include "graph.h"
int main(int argc, char **argv) {
    Graph G;
    FILE *in;
    int k, j;
    in=fopen("../grafo.txt","r");
    G=GRAPHload(in);
    fclose(in);

    GRAPHprintmatrix(G);//stampo la matrice che indica quali vertici sono connessi da un arco

    printf("Inserisci k: ");//inserimento da tastiera di k per calcolare K-CORE
    scanf("%d", &k);
    generaKcore(G, k);//passo il grafo e k

    printf("Inserisci j: ");//inserimento da tastiera di j per calcorare J-EDGE CONNECTED
    scanf("%d", &j);
    //NON MI E' CHIARO
    checkisjedgeconnected(G, j);//passo il grado e j

    return 0;
}

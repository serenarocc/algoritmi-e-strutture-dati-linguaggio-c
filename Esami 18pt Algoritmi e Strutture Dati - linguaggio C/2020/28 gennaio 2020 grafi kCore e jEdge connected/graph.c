//
// Created by seren on 28/03/2023.
//
#include <stdlib.h>
#include <stdio.h>
#include "graph.h"
#include "st.h"

struct grafo{
    int V;//vertici
    int E;//archi
    int **madj; //matrice per indicare quali archi ci sono tra i vertci
    ST table;
    int *degree; //vett per indicare il grado di ogni vertice
    int *deleted; //vett per vertici cancellati
};


static Edge EdgeCreate(int v, int w);
static void insertE(Graph g, Edge e);
static void removeE(Graph G, Edge e);


int GRAPHgetE(Graph G){
    return G->E;
}

int GRAPHgetV(Graph G){
    return G->V;
}


Graph GRAPHinit(int V){//come parametro ha il num di vertici
    Graph g;
    g=malloc(sizeof (*g));
    g->V=V; //dico a struct quanti vertci ci nono
    g->E=0;//al momento ho 0 archi
    g->table=STinit(V);//come parametro ha il num di vertici
    g->madj= malloc(V*sizeof(int*));//allocazione vettore di dim num di vertici per poi andare a costruire la matrice
    for(int i=0;i<V;i++){
        g->madj[i]=calloc(V,sizeof (int));//allocazione matrice che serve per vedere quali archi ci sono tra i vertici
    }
    g->degree = malloc(g->V* sizeof(int));//vettore che indica i gradi di ogni vertice
    g->deleted = malloc(g->V* sizeof(int));//vettore che indica quali vertici sono stati cancellati
    for(int i=0; i<V; i++)
        g->deleted[i] = 0;//inizzializzo tutti gli archi come non cancellati=0
    return g;

}


void GRAPHfree(Graph g){
    int i;
    for(i=0; i < g->V; i++) {
        free(g->madj[i]);
    }
    free(g->madj);
    free(g);
}

static Edge EdgeCreate(int v, int w){//creazione arco tra i 2 vertici v-->w
    Edge e;
    e.v = v;
    e.w = w;
    return e;
}

void GRAPHinsertE(Graph G, int id1, int id2){
    insertE(G, EdgeCreate(id1, id2));
}

void GRAPHremoveE(Graph G, int id1, int id2){
    removeE(G,EdgeCreate(id1,id2));
}


Graph GRAPHload(FILE *in){
    int n,i,j;
    char id1[21], id2[21];
    ST st;
    Graph G;
    fscanf(in,"%d",&n);//n=num di vertici
    G=GRAPHinit(n);//parametro ha il num di vertici
    for(i=0;i<n; i++) {//per ogni vertice
        fscanf(in, "%s", id1);//salvo il nome del verice
        STinsert(G->table, id1);//aggiungo il vertice a ST
    }
    while(fscanf(in, "%s %s\n", id1, id2) == 2){ //prendo parametri degli archi
        i = STsearchbyname(G->table, id1);
        j = STsearchbyname(G->table, id2);
        insertE(G, EdgeCreate(i, j));
    }
    return G;
}


static void removeE(Graph G, Edge e){
    int v = e.v, w = e.w;
    if(G->madj[v][w] != 0){
        G->madj[v][w] = 0;
        G->madj[w][v] = 0;
        G->E--;
    }
}

static void insertE(Graph g, Edge e){//inserisco arco all'interno della matrice
    int v = e.v, w = e.w;
    if(g->madj[v][w] == 0)
        g->E++;//incremento il contatore degli archi
    g->madj[v][w] = 1; //impongo 1 per allocare arco
    g->madj[w][v] = 1;
}

void GRAPHprintmatrix(Graph G){//stampo la matrice che indica quali vertici sono connessi da un arco
    int i,j;
    for(i=0; i< G->V; i++){
        for(j=0; j< G->V; j++)
            printf("%3d ",G->madj[i][j]);
        printf("\n");
    }
}

static int calcolagrado(Graph G){
    int i, j, flag =1 ;
    for(i=0; i<G->V; i++) {//inizzializzo il grado di ogni verice a 0
        G->degree[i] = 0;
    }
    //CALCOLO DEL GRADO DEI VERTICI DEL GRAFO   ----NB----
    //scansiono tutta la matrice e ogni volta che trovo che al vertice corrisponde un arco, incremento il grado
    for(i=0; i<G->V; i++)
        for(j=0; j<G->V; j++)
            if(G->madj[i][j] == 1)
                G->degree[i]++;
    return flag;//i gradi sono stati calcolati
}

void generaKcore(Graph G, int k){
    int i, j, flag = 1;
    calcolagrado(G);
    while(flag){
        for(i=0; i<G->V; i++)//per tutti i vertici
            if(G->deleted[i] == 0 && G->degree[i] < k){ //se il vertice non e' gia' in precendenza stato rimosso e se il grado del vertice e' minore di k lo posso eliminare quel vertice
                G->deleted[i] = 1; //elimino il vertice dato che ha grado minore di k
                for(j=0; j<G->V; j++){
                    if(G->madj[i][j] == 1) { //se e' segnalata ancora la presenza dell'arco tra due vertici
                        G->madj[i][j] = -1; //elimino l'arco tra i due vertici
                    }
                }
            }
        calcolagrado(G);//dopo che ho eliminato un vertice, ricalcolo nuovamente i gradi di tutti i vertici

        //DA QUI IN POI NON MI E' CHIARO
        flag = 0;
        for(i=0; i<G->V; i++){//per tutti i vertici
            if(G->deleted[i] == 0 && G->degree[i] < k)//se vertice non e' stato cancellato e se il grado e'<k
                flag = 1;
        }
    }
    for(i=0; i<G->V; i++){ //NON MIJ E' CHIARO . STAMPA TUTTO IL GRAFO?
        if(G->deleted[i] == 0)
            printf("%s\n", STgetbyindex(G->table, i));
    }
}

static Edge *GRAPHEdges(Graph G){
    int i, j, k=0;
    Edge *e = malloc(G->E* sizeof(Edge));
    for(i=0; i<G->V; i++)
        for(j=i; j<G->V; j++)
            if(G->madj[i][j] != 0)
                e[k++] = EdgeCreate(i, j);
    return e;
}

static void dfsR(Graph G, Edge e, int *visited){
    int i;
    visited[e.w] = 1;
    for(i=0; i<G->V; i++){
        if(visited[i]==0 && G->madj[e.w][i]!=0)
            dfsR(G, EdgeCreate(e.w, i), visited);
    }
}

static int is_disconnetted(Graph G, Edge *sol, int j){
    int i, flag = 0, *visited;
    visited = (int*) malloc(G->V* sizeof(int));
    if(visited == NULL)
        return -1;
    for(i=0;i<G->V; i++){//inizzializzo vettore a 0
        visited[i] = 0;
    }
    for(i=0; i<j; i++){
        removeE(G, sol[i]);
    }
    dfsR(G, EdgeCreate(0,0), visited);
    for(i=0; i<G->V; i++){
        if(visited[i] == 0)
            flag = 1;
    }
    for(i=0; i<j; i++)
        insertE(G, sol[i]);
    free(visited);
    return flag;
}
//combinazioni semplici
static int combinazioni(Graph G, Edge *sol, int pos, int start, int j, Edge *val){
    int i;
    if(pos == j){
        if(is_disconnetted(G, sol, j)==1)
            return 1;
        else
            return 0;
    }
    for(i=start; i<G->E; i++){
        sol[pos] = val[i];
        if(combinazioni(G, sol, pos+1, i+1, j, val)==1)
            return 1;
    }
    return 0;
}
void checkisjedgeconnected(Graph G, int j){
    int i, flag =1;
    Edge *sol, *val;
    sol = malloc(j*sizeof(Edge));
    val = GRAPHEdges(G);
    for(i=1; i<j && flag==1; i++)
        if(combinazioni(G, sol, 0, 0, i, val))
            flag = 0;
    if(!flag)
        printf("G non j-edge-connected.\n");
    else if(combinazioni(G, sol, 0, 0, j, val))
        for(i=0; i<j; i++)
            printf("%s -> %s\n", STgetbyindex(G->table, sol[i].v),STgetbyindex(G->table, sol[i].w));
    else
        printf("G non e' j-edge-connected.");
}


//
// Created by seren on 28/03/2023.
//

#ifndef APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_GRAPH_H
#define APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_GRAPH_H

#include <stdio.h>
typedef struct {
    int v;//vertici
    int w;//archi
}Edge;
typedef struct grafo *Graph; //adt 1 classe


Graph GRAPHload(FILE *in);
Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2);
void GRAPHremoveE(Graph G, int id1, int i2);
int GRAPHgetE(Graph G);
void GRAPHprintmatrix(Graph G);
int GRAPHgetV(Graph G);
void generaKcore(Graph G, int k);
void checkisjedgeconnected(Graph G, int j);

#endif //APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_GRAPH_H

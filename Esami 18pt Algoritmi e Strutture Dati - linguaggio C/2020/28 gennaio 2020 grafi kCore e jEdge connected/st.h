//
// Created by seren on 28/03/2023.
//

#ifndef APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_ST_H
#define APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_ST_H

typedef struct hashtable *ST;

ST STinit(int n);
void STfree(ST table);
int STsearchbyname(ST table, char *key);
void STprint(ST table);
int STinsert(ST table, char *x);
char *STgetbyindex(ST table, int index);
int STgetN(ST table);



#endif //APPELLO18_2020GENNAIO28_GRAFI_KCORE_E_JEDGE_CONNECTED_ST_H

//FUNZIONE 2 NON BUONA
//NON E' STATA FATTA LETTURA DA FILE MA SI PUO' SORVOLARE USANO DIRETTAMENTE LA MATRICE

#include <stdio.h>
#include <stdlib.h>//per malloc

#define N 5//num comuni
#define distMax 8 //data una distanza intera massima distMax, tra comune e colonnina
#define intmax 1000

void generasol1( int **M);
int generaSol1R(int pos, int *sol, int stazMin, int start, int **M);
int checkfobj1(int *sol, int stazMin, int **M);
void generasol2(int numStaz, int **M, int *pop, int *stazComune);
void generasol2R(int pos, int *sol, int *bestsol, int *bestval, int *mark, int n, int **M, int *pop);
int fobj2(int *sol, int numStaz, int **M, int *pop);
int  **malloc2D(int r, int c);

int main() {
    //distanze reciproche tra comuni
    //MATRICE dist[N][N]
        int r=5,c=5;
        int **M = malloc2D(r, c);

        M[0][0]=0;  M[0][1]=8; M[0][2]=10;M[0][3]=7; M[0][4]=12;
        M[1][0]=8;  M[1][1]=0; M[1][2]=7; M[1][3]=9; M[1][4]=11;
        M[2][0]=10; M[2][1]=7; M[2][2]=0; M[2][3]=10;M[2][4]=9;
        M[3][0]=7;  M[3][1]=9; M[3][2]=10;M[3][3]=0; M[3][4]=8;
        M[3][0]=12; M[3][1]=11;M[3][2]=9; M[3][3]=8; M[3][4]=0;

        int pop[N] = {15, 5, 50, 30, 25};
        int stazComune[N] = {1, 1, 4, 3, 2};

        generasol1(M);
        generasol2(2,M,pop,stazComune); //NON HO CAPITO I CALCOLI DELLA RICHIESTA
    return 0;
}

/*funzione obiettivo 1: data una distanza intera massima distMax, determinare il numero minimo stazMin
di stazioni di ricarica e la loro localizzazione in modo che tutti i Comuni distino meno di distMax dalla
stazione di ricarica più vicina. In ogni Comune è localizzata al più una stazione
*/

void generasol1(int **M){
    int stazMin;// numero minimo stazMin di stazioni di ricarica
    int i, *sol;
    sol = malloc(N* sizeof(int));//vettore dim num comuni 5
    for(stazMin = 1; stazMin <N; stazMin++){
        if(generaSol1R(0, sol, stazMin, 0, M)==1){
            printf("Trovata soluzione per funzione obiettivo 1:\n(");
            for(i=0; i<stazMin; i++)
                printf(" %d ",sol[i]);
            printf(") stazMin = %d\n",stazMin);
            return;
        }
    }
}

int generaSol1R(int pos, int *sol, int stazMin, int start, int **M){//combinazioni semplici
    int i;
    if(pos == stazMin){
        if(checkfobj1(sol, stazMin, M))
            return 1;
        return 0;
    }
    for(i=start; i<N; i++){
        sol[pos] = i;
        if(generaSol1R(pos+1, sol, stazMin, i+1,M)==1)
            return 1;
    }
    return 0;
}


int checkfobj1(int *sol, int stazMin, int **M){
    int i,j, flag;
    for(i=0; i<N; i++){
        flag = 0;
        for(j=0; j<stazMin; j++){
            if(M[i][sol[j]] <= distMax)
                flag = 1;
        }
        if(flag == 0)
            return 0;
    }
    return 1;
}


/*
 * funzione obiettivo 2: dato un numero fisso intero di stazioni di ricarica posizionabili sull’insieme del
territorio numStaz, dato il numero massimo di stazioni di ricarica localizzabili in ciascun Comune
memorizzato in un vettore stazComune di interi ≥1, determinare una qualsiasi localizzazione che
minimizzi la distanza, pesata in base alla popolazione e al numero di stazioni localizzate in un dato Comune,
tra ogni Comune e la stazione di ricarica ad esso più vicina */

void generasol2(int numStaz,int **M, int *pop, int *stazComune){
    int *sol, *bestsol,i, bestval = 10000;
    sol = calloc(N,  sizeof(int));
    bestsol = calloc(N, sizeof(int));
    generasol2R(0, sol, bestsol, &bestval, stazComune, numStaz,M,pop);
    for(i=0; i<N; i++){
        if(bestsol[i] != 0){
            printf("%d stazioni in %d\n", sol[i], i);
        }
    }
}
void generasol2R(int pos, int *sol, int *bestsol, int *bestval, int *mark, int n, int **M, int *pop){
    int i, tmpval;
    if(pos == n){
        tmpval = fobj2(sol, n,M,pop);
        if(tmpval < *bestval){
            *bestval = tmpval;
            for(i=0; i<N; i++)
                bestsol[i] = sol[i];
        }
        return;
    }
    for(i=0; i<n; i++){
        if(mark[i] > 0){
            sol[pos]++;
            mark[i]--;
            generasol2R(pos+1, sol, bestsol, bestval, mark, n, M,pop);
            sol[pos]--;
            mark[i]++;
        }
    }
}

int fobj2(int *sol, int numStaz, int **M, int *pop){
    int i,j,min,minpos,val=0;
    for(i=0; i<N; i++){
        min = intmax;
        minpos =-1;
        for(j=0; j<numStaz; j++){
            if(M[i][sol[j]] < min){
                min = M[i][sol[j]];
                minpos = sol[j];
            }
        }
        val += (pop[i]*min)/sol[minpos];
    }
    return val;
}

int  **malloc2D(int r, int c){
    int  **m;
    m=malloc(r*sizeof(int *));
    if (m==NULL) return NULL;
    for(int i=0; i<c; i++){
        m[i]=malloc(c*sizeof(int));
        if (m==NULL) return NULL;
    }
    return m;
}
//
// Created by seren on 22/04/2023.
//

#ifndef APPELLO18_2019_SETTEMBRE18_MAPPA_RISORSE_QUEUE_H
#define APPELLO18_2019_SETTEMBRE18_MAPPA_RISORSE_QUEUE_H

typedef struct queque *Q;

typedef struct P_{
    int r;
    int c;
    int id;
    int dist;
}P;

Q       QUEUEinit(void);
int     QUEUEempty(Q q);
void    QUEUEput (Q q, P p);
P       QUEUEget(Q q);
void    QUEUEfree(Q q);


#endif //APPELLO18_2019_SETTEMBRE18_MAPPA_RISORSE_QUEUE_H

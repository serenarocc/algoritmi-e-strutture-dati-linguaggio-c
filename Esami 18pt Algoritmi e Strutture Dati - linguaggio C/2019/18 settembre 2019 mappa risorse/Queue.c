//
// Created by seren on 22/04/2023.
//
#include <stdio.h>
#include <stdlib.h>

#include "Queue.h"



typedef struct QUEQUEnode *link;
struct QUEQUEnode{//QUASI ADT
    P p;
    link next;
};

struct queque {//ADT 1 CLASSE
    link head;
    link tail;
    int size;
};

static link NEW(P p, link next){
    link x;
    x=malloc(sizeof (*x));
    x->p=p;
    x->next=next;
    return x;
}

Q QUEQUEint(){
    Q q;
    q=malloc(sizeof (*q));
    q->head=NULL;
    q->size=0;
    return q;
}


int QUEQUEempty(Q q){
    return q->head==NULL;
}

int QUEQUEsize(Q q){
    return q->size;
}


void QUEQUEput(Q q,P p){
    if(q->head==NULL){
        q->tail=NEW(p,q->head);
        q->head=q->tail;
        q->size= q->size +1;
        return;
    }
    q->tail->next = NEW(p, q->tail->next) ;
    q->tail = q->tail->next;
    q->size += 1;
}

P QUEUEget(Q q){
    P p = q->head->p;
    link t = q->head->next;
    free(q->head);
    q->head = t;
    q->size--;
    return p;
}


static void LINKfree(link l) {
    if (l != NULL) {
        LINKfree(l->next);
        free(l);
    }
}

void QUEUEfree(Q q) {
    LINKfree(q->head);
    free(q);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "divisioni.h"
#include "dipendenti.h"
/*
typedef struct associazioni_t *associazioni;
typedef struct{
    int matrice;
    char mestiere; // o a t i
    char siglaDiv[11]; //D1 D2
}associazioni_t;
*/
int main(int argc, char **argv) {
    //RICHIESTA 1
    dipendente_t *dipendenti;
    divisioni_t *divisioni;
    associazioni_t *A;
    int n;//num dipendenti
    int D;//num divisioni
    FILE *fp1;
    fp1=fopen("../dipendenti.txt","r");
    fscanf(fp1,"%d\n",&n);
    dipendenti=malloc(n*sizeof(dipendente_t));
    printf("File dipendenti:\n");
    for(int i=0;i<n;i++){
        fscanf(fp1,"%d %s %s %d %d %d %d\n",&dipendenti[i].matr, dipendenti[i].nome, dipendenti[i].cognome,&dipendenti[i].o,&dipendenti[i].a,&dipendenti[i].t,&dipendenti[i].i);
        printf("%d %s %s o:%d a:%d t:%d i:%d\n",dipendenti[i].matr, dipendenti[i].nome, dipendenti[i].cognome,dipendenti[i].o,dipendenti[i].a,dipendenti[i].t,dipendenti[i].i);
    }
    fclose(fp1);
    FILE *fp2;
    printf("\n\nFile divisioni: \n");
    fp2=fopen("../divisioni.txt","r");
    fscanf(fp2,"%d\n",&D);
    divisioni=malloc(D*sizeof (divisioni_t));
    for(int i=0;i<D;i++){ //per tutte le divisioni che ho nel file leggo e salvo i dati una divisione alla volta
        divisioni[i]=divisioniScan(fp2,n);
    }
    fclose(fp2);
    //----------------------------
    //RICHIESTA 2

    FILE *fp3;
    printf("\n\nFile associazioni:\n");
    fp3=fopen("../associazioni.txt","r");
    A=malloc(n*sizeof(associazioni_t));
    for(int i=0; i<n;i++){
        A[i]=letturaAssociazioni(fp3,n);
    }
    //verfica numero minimo di addetti


    int numO=0, numA=0, numT=0, numI=0;
    int  competenza_tot=0,scostamento=0;

    for(int j=0; j<D;j++){//per tutte le divisioni
        for(int i=0; i<n; i++){ //per tutti i dipendenti di A

          printf("%s ",A[i]->siglaDiv);


        /*    if(strcmp(A[i]->siglaDiv,divisioni[j]->sigla)==0){//i codici della divisione sono identici
                if(A[i]->mestiere=='o')
                    numO++;
                if(A[i]->mestiere=='a')
                    numA++;
                if(A[i]->mestiere=='t')
                    numT++;
                if(A[i]->mestiere=='i')
                    numI++;
            }*/
            /*    if(numO!=t->richieste[0][0] || numA!=t->richieste[1][0] ||numT!=t->richieste[2][0] ||numI!=t->richieste[3][0]){
                    printf("Errore: non sono soddisfatti i vincoli di numero mimimo di addetti");
                    exit(-1);
                }
                else{
                    printf("tutto ok");
                }*/
        }

    }



   /* for(int j=0; j<D;j++){//per tutte le divisioni
        for(int i=0; i<n; i++) { //per tutti i dipendenti di A

         //   verifica(divisioni[j], A[i],n, D);
        }
    }*/


    //verifica di competenza totale minima richiesta per ogni tipologia di ogni divisione


    //calcolo scostamento complessivo medio rispetto alla competenza richiesta


    fclose(fp3);
    //-------------------------------
    //RICHIESTA 3
    divisioniFree(divisioni);
    return 0;
}

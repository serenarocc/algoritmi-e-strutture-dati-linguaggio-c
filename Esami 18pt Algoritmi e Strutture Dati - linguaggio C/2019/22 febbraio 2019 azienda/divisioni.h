//
// Created by seren on 27/03/2023.
//

#ifndef APPELLO18_2019_FEB22_AZIENDA_DIVISIONI_H
#define APPELLO18_2019_FEB22_AZIENDA_DIVISIONI_H
#include "stdio.h"
#include "dipendenti.h"

typedef struct divisioni *divisioni_t;//ADT 1 CLASSE
typedef struct associazioni *associazioni_t;





divisioni_t divisioniScan(FILE *fp2, int MAX);
divisioni_t divisioniNEW(char *sigla, int MAX);
void divisioniFree(divisioni_t t);
void divisioniInsertDipendente(divisioni_t t, dipendente_t dip);
void divisioniStore(divisioni_t t);

associazioni_t letturaAssociazioni(FILE *fp3,int n);
void verificaAssociazioni(int n, int D);
void verifica(int n, int D);
#endif //APPELLO18_2019_FEB22_AZIENDA_DIVISIONI_H

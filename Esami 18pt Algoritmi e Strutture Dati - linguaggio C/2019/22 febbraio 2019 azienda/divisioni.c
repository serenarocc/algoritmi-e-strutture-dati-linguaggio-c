//
// Created by seren on 27/03/2023.
//

#include "divisioni.h"
#include "dipendenti.h"
#include <stdlib.h>
#include <string.h>
#include "stdio.h"


struct divisioni{
    char *sigla; //codice della divisione
    dipendente_t *v;
    int n;
    int max;
    int richieste[4][3]; //ogni divisione ha dati interi o a t i che si possono vedere come una matrice 4x3
};

struct associazioni{
    int matrice;
    char mestiere; // o a t i
    char siglaDiv[11]; //D1 D2
};



//funz di creazione divisione
divisioni_t divisioniNEW(char *sigla, int MAX){
    divisioni_t t;
    t=malloc(sizeof(*t));
    t->sigla= strdup(sigla); //copio codice divisione
    t->max=MAX;//num dipendenti totali che si hanno a disposizione nell'azienda
    printf("sigla: %s     max:%d\n", t->sigla,t->max);
    return t;
}

//funz acquisizione divisione
divisioni_t divisioniScan(FILE *fp2, int MAX){ //lego file divisioni e alloco dati
    divisioni_t t;
    int tot=0; //persone che servono per quella divisione
    char cod[11]; //codice che inidica la divisione
    fscanf(fp2,"%s\n",cod); //leggo codice della divisione
    t=divisioniNEW(cod,MAX);
    for(int i=0; i<4; i++){//riempio matrice delle richieste
        fscanf(fp2,"%d %d %d\n",&t->richieste[i][0],&t->richieste[i][1],&t->richieste[i][2]);
        printf("Mij:%d Cminij:%d Rij:%d   ",t->richieste[i][0],t->richieste[i][1],t->richieste[i][2]);
        tot +=t->richieste[i][0];
        printf("tot:%d \n",tot);
    }
    t->n=0;
    t->max=2*tot;
    t->v=malloc(2*tot*sizeof (dipendente_t));
    return t;
}



//funz di distruzione divisione
void divisioniFree(divisioni_t t){
    free(t->v);
    free(t->sigla);
    free(t);
}

//funz inserimento di un dipendente nella divisione
/*
 * void divisioniInsertDipendente(divisioni_t t, dipendente_t dip){
    t->v[t->n++]=dip;
    return;
}
 * */


//funz stampa/salvataggio su file
void divisioniStore(divisioni_t t){
    FILE *fp4;
    fp4=fopen("../stampa.txt","w");
    //for(int i=0;i<t->n,i++){
      //  fprintf(fp4,"dipendente:%d mestiere:%c divisione:%s",t->v[i].matr,t->v[i].tipo,t->sigla);
  //  }
}


associazioni_t letturaAssociazioni(FILE *fp3,int n){
    associazioni_t  A;

    fscanf(fp3,"%d %c %s\n",&A->matrice,&A->mestiere,A->siglaDiv);
    printf("%d %c %s\n",A->matrice,A->mestiere,A->siglaDiv);
    return A;
}



void verifica(int n, int D){
    associazioni_t *a;
    divisioni_t *t;
    int numO=0, numA=0, numT=0, numI=0;
    int  competenza_tot=0,scostamento=0;

    a=malloc(n*sizeof(associazioni_t));
    t=malloc(D*sizeof (divisioni_t));


    for(int j=0; j<D;j++){//per tutte le divisioni
        for(int i=0; i<n; i++){ //per tutti i dipendenti di A

            printf("%s %s",a[i]->siglaDiv,t[j]->sigla);


            if(strcmp(a[i]->siglaDiv,t[j]->sigla)==0){//i codici della divisione sono identici
                if(a[i]->mestiere=='o')
                    numO++;
                if(a[i]->mestiere=='a')
                    numA++;
                if(a[i]->mestiere=='t')
                    numT++;
                if(a[i]->mestiere=='i')
                    numI++;
            }
        /*    if(numO!=t->richieste[0][0] || numA!=t->richieste[1][0] ||numT!=t->richieste[2][0] ||numI!=t->richieste[3][0]){
                printf("Errore: non sono soddisfatti i vincoli di numero mimimo di addetti");
                exit(-1);
            }
            else{
                printf("tutto ok");
            }*/
        }

    }
}


void verificaAssociazioni(int n, int D){
    //verfica numero minimo di addetti
    verifica(n,D);
}
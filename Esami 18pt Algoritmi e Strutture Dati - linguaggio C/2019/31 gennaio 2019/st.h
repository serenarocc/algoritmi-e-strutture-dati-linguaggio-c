//
// Created by seren on 27/03/2023.
//

#ifndef APPELLO18_2019_GEN31_ST_H
#define APPELLO18_2019_GEN31_ST_H

typedef struct hashtable *ST;


ST STinit(int n);
void STfree(ST table);
int STsearchbyname(ST table, char *key);
void STprint(ST table);
int STinsert(ST table, char *x);
int STgetN(ST table);
char *STgetbyindex(ST table, int index);
#endif //APPELLO18_2019_GEN31_ST_H

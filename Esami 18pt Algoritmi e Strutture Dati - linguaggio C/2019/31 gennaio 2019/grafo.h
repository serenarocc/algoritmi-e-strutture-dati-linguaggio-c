//
// Created by seren on 27/03/2023.
//

#ifndef APPELLO18_2019_GEN31_GRAFO_H
#define APPELLO18_2019_GEN31_GRAFO_H


typedef struct{
    int v;
    int w;
}Edge;
typedef struct grafo *Graph;


Graph GRAPHreadFile(FILE *in);
Graph GRAPHinit(int V);
void GRAPHfree(Graph t);

void GRAPHinsertE(Graph G, int id1, int id2);
void GRAPHremoveE(Graph G, int id1, int i2);

void GRAPHprintmatrix(Graph G);

int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetE(Graph G);
int GRAPHgetV(Graph G);


int isKernel(Graph G, int *sol, int dim);
int *kernelRead(FILE *in,Graph G, int *dim);
void GRAPHminimumKernel(FILE *out, Graph G);

#endif //APPELLO18_2019_GEN31_GRAFO_H

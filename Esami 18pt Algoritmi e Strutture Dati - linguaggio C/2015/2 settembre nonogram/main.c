#include <stdio.h>
#include <stdlib.h>

typedef struct{  //vincolo vale sia x ogni riga che x ogni colonna
    int n;  //numero di gruppi
    int *len; //dimensione per ogni gruppo
}vincolo;

typedef struct{
    int N;//num righe
    int M;//num colonne
    vincolo *riga;
    vincolo *colonna;
    int **tab; //matrice di interi 0 e 1
}Nonogram;



/*
Nonogram readFile(FILE *in); //NB tipo nonogram della funz
int check(Nonogram t,int **sol, int n, int m);
void generaSoluzione(Nonogram t);
int generaSoluzioneR(int pos, Nonogram t);

*/






Nonogram readFile(FILE *in){
    int i,j;
    Nonogram table;
    fscanf(in,"%d",&table.N); //num righe n
    table.riga=malloc(table.N*sizeof (vincolo)); //vettore dim N di tipo vincolo
    //lettura le 5 rihe seguenti
    for(i=0;i<table.N;i++){
        fscanf(in,"%d",&(table.riga[i].n)); //sto salvando quanti gruppi ci sono per ogni riga
        for(j=0;j<table.riga[i].n;j++){
            fscanf(in,"%d",&(table.riga[i].len[j]));
        }
    }
    fscanf(in,"%d",&table.M);//salvo il num di colonne
    table.colonna=malloc(table.M*sizeof (vincolo));
    for(i=0;i<table.M;i++){
        fscanf(in,"%d",&table.colonna[i].n);//numero di gruppi per ogni colonna
        table.colonna[i].len= malloc(table.colonna[i].n*sizeof (int));  //per ogni colonna faccio un vett che indica quanti gruppi ho. ongni elemento conterra' la dimensione del gruppo
        for(j=0;j<table.colonna[i].n;j++){
            fscanf(in,"%d",&table.colonna[i].len[j]);
        }
        //adesso creo la matrice tab
        table.tab=malloc(table.N*sizeof (int)); //creo le righe
        for(i=0; i < table.N; i++)
            table.tab[i] = malloc(table.M* sizeof(int));//per ogni riga creo le colonne
        return table;
    }
}







int check(Nonogram t,int **sol, int n, int m){
    int righeok = 1, colonneok = 1;
    //TODO scrivere funzione di check
    return (righeok && colonneok);
}

int generaSoluzioneR(int pos, Nonogram t){
    int i,j;
    if(pos == t.N*t.M){
        return check(t,t.tab,t.N,t.M);
    }
    i = pos / t.N;
    j = pos%t.M;
    t.tab[i][j] = 1;
    if(generaSoluzioneR(pos+1, t))
        return 1;
    t.tab[i][j] = 0;
    if(generaSoluzioneR(pos+1, t))
        return 1;
    return 0;
}
void generaSoluzione(Nonogram t){
    int i,j;
    generaSoluzioneR(0, t);
    for(i=0; i<t.N; i++){
        for(j=0; j<t.M; j++)
            printf("%d ",t.tab[i][j]);
        printf("\n");
    }
}









int main() {
   Nonogram tabella;
   FILE *in;
   in= fopen("../file.txt","r");
   tabella=readFile(in);
   fclose(in);

    int **sol,n,m,i,j;
    in = fopen("../soluzione.txt", "r");
    fscanf(in, "%d %d", &n, &m);
    sol = malloc(n*sizeof(int*)); //matrice sol che contiene tutta  la matrice contenuta in soluzioni.txt
    for(i=0; i<n; i++){
        sol[i] = malloc(m* sizeof(int));
    }

    for(i=0; i<n; i++)  //lettura matrice file
        for(j=0; j<m; j++)
            fscanf(in, "%d", &sol[i][j]);
    fclose(in);


    if(check(tabella, sol, n, m)==1)
        printf("Soluzione valida\n");
    else
        printf("soluzione non valida\n");
    printf("Genero una soluzione:\n\n");
    generaSoluzione(tabella);

    return 0;
}











/*
Nonogram readFile(FILE *in){
    int i,j;
    Nonogram table;
    fscanf(in,"%d",&table.N); //num righe n
    table.riga=malloc(table.N*sizeof (vincolo)); //vettore dim N di tipo vincolo
    //lettura le 5 rihe seguenti
    for(i=0;i<table.N;i++){
        fscanf(in,"%d",&(table.riga[i].n)); //sto salvando quanti gruppi ci sono per ogni riga
        for(j=0;j<table.riga[i].n;j++){
            fscanf(in,"%d",&(table.riga[i].len[j]));
        }
    }
    fscanf(in,"%d",&table.M);//salvo il num di colonne
    table.colonna=malloc(table.M*sizeof (vincolo));
    for(i=0;i<table.M;i++){
        fscanf(in,"%d",&table.colonna[i].n);//numero di gruppi per ogni colonna
        table.colonna[i].len= malloc(table.colonna[i].n*sizeof (int));  //per ogni colonna faccio un vett che indica quanti gruppi ho. ongni elemento conterra' la dimensione del gruppo
        for(j=0;j<table.colonna[i].n;j++){
            fscanf(in,"%d",&table.colonna[i].len[j]);
        }
        //adesso creo la matrice tab
        table.tab=malloc(table.N*sizeof (int)); //creo le righe
        for(i=0; i < table.N; i++)
            table.tab[i] = malloc(table.M* sizeof(int));//per ogni riga creo le colonne
        return table;
    }
}


void generaSoluzione(Nonogram t){
    int i,j;
    generaSoluzioneR(0,t);
    for(i=0;i<t.N;i++){
        for(j=0;j<t.M;j++){
            printf("%d",t.tab[i][j]);
        }
        printf("\n");
    }
}

int generaSoluzioneR(int pos,Nonogram t){
    int i,j;
    if(pos==t.N*t.M){
        return check(t,t.tab,t.N,t.M);
    }
    i=pos/t.N;
    j=pos%t.M;
    t.tab[i][j]=1;
    if (generaSoluzioneR(pos+1,t))
        return 1;
    t.tab[i][j]=0;
    if(generaSoluzioneR(pos+1,t))
        return 1;
    return 0;
}


int check(Nonogram t, int **sol,int n,int m){
    int righeok=1;
    int colonneok=1;
    return (righeok && colonneok);
}

 */
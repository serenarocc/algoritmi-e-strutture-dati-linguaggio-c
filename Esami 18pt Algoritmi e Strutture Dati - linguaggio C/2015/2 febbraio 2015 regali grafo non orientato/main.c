#include <stdio.h>
#include <stdlib.h>
#include "grafo.h"

int generaPartizioni(Graph G, int n, int m, int pos, int *sol, int k);
void generaRegali(Graph G);

int main() {
    FILE *in;
    Graph G;
    int *regali,i;
    in = fopen("../amici.txt", "r");
    G = GRAPHreadFile(in);//lettura file
    fclose(in);
    GRAPHprintmatrix(G);//stampa matrice
    in  = fopen("../soluzioni.txt", "r");
    regali = leggiRegali(in, G); //vett regali contiene per tutte le persone a quale tipologie di regalo appartengono
    fclose(in);
    if(checkRegali(G, regali))
        printf("Soluzione in input valida.\n");
    else
        printf("Soluzione in input non valida.\n");
    generaRegali(G);
    return 0;
}



void generaRegali(Graph G){
    int *sol,i;
    sol = malloc(GRAPHgetV(G)* sizeof(int));
    for(i = 0; i<GRAPHgetV(G); i++){
        if(generaPartizioni(G, GRAPHgetV(G), 0, 0, sol, i)==1) {
            printf("Soluzione ottima con %d tipologie di regali:\n",i);
            for (i = 0; i < GRAPHgetV(G); i++) {
                printf("%s tipologia %d\n",GRAPHVertexname(G,i),sol[i]+1);
            }
        }
    }

}


int generaPartizioni(Graph G, int n, int m, int pos, int *sol, int k){  //algoritmo di er
    int i;
    if(pos >= n){//finisco quando ho controllato tutte le persone (tutti i vertici)
        if(m == k){ //scelgo io quante sottopartizioni voglio-->ovvero corrisponde al numero di tipologie di regalo
            if(checkRegali(G, sol)==1)
                return 1;
            return 0;
        }
        return 0;
    }
    for(i=0; i<m; i++){ //m e' il numero di tipologie di regalo
        sol[pos] = i;
        if(generaPartizioni(G, n, m, pos+1, sol, k)==1)
            return 1;
    }
    sol[pos] = m; //aumento il numero di tipologie, ovvero ingrandisco il numero di sottoinsiemi
    if(generaPartizioni(G, n, m+1, pos+1, sol, k)==1)
        return 1;
    return 0;
}
//
// Created by seren on 10/06/2023.
//

#ifndef APPELLO18_2015_FEBBRAIO2_REGALI_GRAFO_NON_ORIENTATO_GRAFO_H
#define APPELLO18_2015_FEBBRAIO2_REGALI_GRAFO_NON_ORIENTATO_GRAFO_H


typedef struct{   //arco
    int v;
    int w;
}Edge;

typedef struct grafo *Graph;//adt 1 classe

Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2);
void GRAPHremoveE(Graph G, int id1, int i2);
Graph GRAPHreadFile(FILE *in);
void GRAPHladjcreate(Graph G);
void GRAPHprintmatrix(Graph G);
void GRAPHprintlist(Graph G);
int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetE(Graph G);
int GRAPHgetV(Graph G);
int *leggiRegali(FILE *in, Graph G);
int checkRegali(Graph G, int *regali);
int GRAPHmaximumDegree(Graph G);
char *GRAPHVertexname(Graph G, int i);
#endif //APPELLO18_2015_FEBBRAIO2_REGALI_GRAFO_NON_ORIENTATO_GRAFO_H

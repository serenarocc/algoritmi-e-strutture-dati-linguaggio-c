//
// Created by seren on 01/07/2023.
//
#include "stdio.h"
#include "stdlib.h"
#include "st.h"
#include "graph.h"
#define MAX 3

struct orientamento{
    int id;//nome vertice del grafo
    int n,s,o,e;//punti cardinali
    int speciale;//se 1 indica che la tessera e' di inizio o fine, 2= tessera notmale, 0=buco
};

typedef struct orientamento *O;

struct grafo{
    int V;
    int E;
    int **madj;
    ST table;
    struct orientamento **M;
}grafo;

static Edge EdgeCreate(int v, int w);
static void insertE(Graph g, Edge e);
static void removeE(Graph G, Edge e);
void printGraphM(Graph G1);
void modifica_grafo(Graph G,ST st);
int funz_confronta_risultato(Graph G1,int mossa,int N);
int wrapper_funz_confronta_risultato(Graph G1,int *sol, int pos,int N);
void disp_rip(int pos,int *val,int *sol,int *trovato,Graph G3,int N,int T);
void allPath(Graph G,int v, int w,int *visited,int *path,int curr_len,int *trovato);
void wrapper_allPath(Graph G,int N,int T,int *trovato);

int GRAPHgetE(Graph G){
    return G->E;
}

int GRAPHgetV(Graph G){
    return G->V;
}


Graph GRAPHinit(int V){
    int i;
    Graph g;
    g = malloc(sizeof(*g));
    g->V = V;
    g->E = 0;

    g->madj = malloc((V*2)* sizeof(float*));
    for(i=0; i<(V*2); i++)
        g->madj[i] = calloc((V*2), sizeof(float));

    //Allocazione scacchiera
    g->M=malloc((V)*sizeof(O*));
    for( i=0 ;i < V; i++) {
        g->M[i]= malloc(V*sizeof (O));
    }
    return g;
}


void GRAPHfree(Graph g){
    int i;
    for(i=0; i < g->V; i++) {
        free(g->madj[i]);
    }
    free(g->madj);
    free(g);
}

static Edge EdgeCreate(int v, int w){
    Edge e;
    e.v = v;
    e.w = w;

    return e;
}

void GRAPHinsertE(Graph G, int id1, int id2){
    insertE(G, EdgeCreate(id1, id2));
}

void GRAPHRemoveE(Graph G, int id1, int id2){
    removeE(G,EdgeCreate(id1,id2));
}


 Graph readFile(Graph G, FILE *in, int N, int T){
    FILE *fin=NULL;
    int n, t;
    int i=0, j=0,nome=0;
    fin = fopen("../grid1.txt", "r");
    //Butto via la prima riga del file
    fscanf(fin, "%d %d ", &n, &t);

    ST st;
    st= STinit(16);

    int vettR[4];
    int contatore=0;

    for(i=0;i<4;i++) {
        vettR[i]=0;
    }

    printf("\n Inizializzazione scacchiera: \n");

    for(i=0; i < N; i++) {
        for (j = 0; j < N; j++) {
            fscanf(fin, "%d %d %d %d", &(G->M[i][j].n), &(G->M[i][j].s), &(G->M[i][j].o), &(G->M[i][j].e));
            printf("M[%d][%d].n=%d %d %d %d\n",i,j, G->M[i][j].n, G->M[i][j].s, G->M[i][j].o, G->M[i][j].e);
            G->M[i][j].id=100+i+j+nome;//nuovo nome del vertice del grafo--> metodo valido solo per matrici 2x2
            STinsert(st,G->M[i][j].id);
        }
        nome=1;
    }
    printf("\nstampa ST: \n");
    STprint(st);
    printf("\n Stampa speciali dopo lettura file:\n ");
    controllo_speciale(G, N);
    G->table=st;
    modifica_grafo(G,st);
    printf("\nstampa madj dopo la prima lettura del file in struct: \n");
    GRAPHprintmatrix(G);
    return G;
}
void controllo_speciale(Graph G,int N) { //dice se la tessera e'di arrivo/ destinazione oppure normale o buca
    int vettR[4];
    int  cnt=0;
    for(int i=0;i<4;i++) {
        vettR[i]=0; //inizzializzaz
    }
    for(int i=0; i<N; i++){ //Seleziono riga k
        for(int j=0; j<N; j++) {//Seleziono elemento [k][w], colonna
            vettR[0] = G->M[i][j].n;
            vettR[1] = G->M[i][j].s;
            vettR[2] = G->M[i][j].o;
            vettR[3] = G->M[i][j].e;

            for(int w=0;w<4;w++) {
                if(vettR[w]==1)
                    cnt++;
            }

            G->M[i][j].speciale=cnt;
            // se  speciale = 0 la tessera e' buca, se =1 la tessera e' arrivo/destinazione oppure =2 se la tessera e' normale
            printf("\nG->M[%d][%d].speciale: %d ",i,j,G->M[i][j].speciale);
            cnt=0;
        }
        printf("\n");
    }
}


Graph GRAPHreadfile(Graph G, FILE *in,int N,int T){
    return readFile(G,in,N,T);
}


static void removeE(Graph G, Edge e){
    int v = e.v, w = e.w;
    if(G->madj[v][w] != 0){
        G->madj[v][w] = 0;
        G->E--;
    }
}

static void insertE(Graph g, Edge e){
    int v = e.v, w = e.w;
    if(g->madj[v][w] == 0)
        g->E++;
    g->madj[v][w] = 1;
    g->madj[w][v] = 1;
}

void GRAPHprintmatrix(Graph G){
    int i,j;
    for(i=0; i<2*(G->V); i++){
        for(j=0; j <2*(G->V); j++)
            printf("%3d ",G->madj[i][j]);
        printf("\n");
    }
}


int GRAPHedges(Graph G, int *v, int i){
    int j,n=0;
    for(j=0; j<G->V; j++)
        if(G->madj[i][j] != 0)
            v[n++] = j;
    return n;
}


//-------------------------------------------------------------------------------
//PROBLEMA DI VERIFICA
int ProbVerifica (Graph G, FILE *in,int N){

    int i,j,Nmosse,mossa,valore=0;
    Graph G1;
    G1= GRAPHinit(N);
    G1=G;

    fscanf(in,"%d",&Nmosse);
    if(Nmosse>MAX) return 0;

    while(fscanf(in,"%d", &mossa)==1){
        valore=funz_confronta_risultato(G1,mossa,N);
        if(valore==0) return 0;
    }
    return valore;
}



int funz_confronta_risultato(Graph G1,int mossa,int N){
//qui confronto se la nuova struttura rispetta le condizioni del testo
int flag=0;
int i,j;
    O tmp= malloc(sizeof (O*));
    tmp->speciale=0;
    tmp->s=0;
    tmp->o=0;
    tmp->n=0;
    tmp->e=0;

    ST st;

  switch (mossa) {
        case 0:
            printf("mossa verso l'alto\n");
            for(i=1;i<N;i++){
                for(j=0;j<N;j++){
                    if(G1->M[i][j].speciale!=0){ //se la tesera che guardo non e' un buco
                        if(G1->M[i-1][j].speciale==0){//se la tessera futura e' un buco
                            G1->M[i-1][j]=G1->M[i][j]; //tessera futura diventa quella che guard
                            G1->M[i][j]=*tmp;//libero la tessera che guardo
                            flag=1;
                            modifica_grafo(G1,G1->table); //AGGIUNTA
                          //  printf("\nstampa madj verso l': \n");
                          //  GRAPHprintmatrix(G1);
                        }
                    }
                }
            }
            break;
        case 1:
              printf("mossa verso il basso\n");
                for(i=0;i<N-1;i++){
                    for(j=0;j<N;j++){
                        if(G1->M[i][j].speciale!=0){
                            if(G1->M[i+1][j].speciale==0){
                                G1->M[i+1][j]=G1->M[i][j];
                                G1->M[i][j]=*tmp;
                                flag=1;
                                modifica_grafo(G1,G1->table); //AGGIUNTAAA
                            //    printf("\nstampa madj: \n");
                             //   GRAPHprintmatrix(G1);
                            }
                        }
                    }
                }
              break;
        case 2:
             printf("mossa verso destra\n");
            for(i=0;i<N;i++){
                for(j=0;j<N-1;j++){
                    if(G1->M[i][j].speciale!=0){ //se la tesera che guardo non e' un buco
                        if(G1->M[i][j+1].speciale==0){//se la tessera futura e' un buco
                            G1->M[i][j+1]=G1->M[i][j];
                            G1->M[i][j]=*tmp;
                            flag=1;
                            modifica_grafo(G1,G1->table); //AGGIUNTAAA
                        //    printf("\nstampa madj: \n");
                         //   GRAPHprintmatrix(G1);
                        }
                    }
                }
            }
              break;
        case 3:
               printf("mossa verso sinistra\n");
                for(i=0;i<N;i++){
                    for(j=1;j<N;j++){
                        if(G1->M[i][j].speciale!=0){ //se la tesera che guardo non e' un buco
                            if(G1->M[i][j-1].speciale==0){//se la tessera futura e' un buco
                                G1->M[i][j-1]=G1->M[i][j];
                                G1->M[i][j]=*tmp;
                                flag=1;
                                modifica_grafo(G1,G1->table); //AGGIUNTAAA
                           //     printf("\nstampa madj: \n");
                            //    GRAPHprintmatrix(G1);
                            }
                        }
                    }
                }
               break;
    default:
        printf("mossa non compreso\n");
        break;
  }
  /*  printf("\nstampa G1 alla fine di fun controlla risultato \n");
    printGraphM(G1);*/
    return flag;

}

void printGraphM(Graph G1){
    int i,j;
    for(i=0;i<G1->V;i++){
        for(j=0;j<G1->V;j++){
            printf("M[%d][%d]:%d  ",i,j,G1->M[i][j].speciale);
        }
        printf("\n");
    }
    printf("\nstampa madj:\n");
    for(i=0;i<2*(G1->V);i++){
        for(j=0;j<2*(G1->V);j++){
            printf("M[%d][%d]:%d  ",i,j,G1->madj[i][j]);
        }
        printf("\n");
    }
}

void modifica_grafo(Graph G,ST st){
     int  i,j;
     int a,b;
     for(i=0;i<G->V;i++){
         for(j=0;j<G->V;j++){
             if(G->M[i][j].s==1 && G->M[i+1][j].n==1){//mio sud, futuro nord
                 a= STsearchByName(st,G->M[i][j].id);
                 b= STsearchByName(st,G->M[i+1][j].id);
                 GRAPHinsertE(G,a,b);
             }
             if(i>0){
                 if(G->M[i][j].n==1 && G->M[i-1][j].s==1){//mio nord, futuro sud
                     a= STsearchByName(st,G->M[i][j].id);
                     b= STsearchByName(st,G->M[i-1][j].id);
                     GRAPHinsertE(G,a,b);
                 }
             }

             if(G->M[i][j].e==1 && G->M[i][j+1].o==1){//mio EST, futuro OVEST
                 a= STsearchByName(st,G->M[i][j].id);
                 b= STsearchByName(st,G->M[i][j+1].id);
                 GRAPHinsertE(G,a,b);
             }
             if(G->M[i][j].o==1 && G->M[i][j-1].e==1){//mio ovest, futuro EST
                 a= STsearchByName(st,G->M[i][j].id);
                 b= STsearchByName(st,G->M[i][j-1].id);
                 GRAPHinsertE(G,a,b);
             }
         }
     }
     G->table=st;
 }


 //--------------------------------------------------------------------------------------------
 //PROBLEMA DI RICERCA E OTTIMIZZAZIONE


void wrapper_allPath(Graph G,int N,int T,int *trovato){
    Graph G2;
    G2= GRAPHinit(N);
    G2=G;

    int partenza,arrivo,flag=0;

    int *visited;
    visited=malloc(2*(G2->V)*sizeof(int));
    for(int i=0;i<2*(G2->V);i++) visited[i]=0;

    int *path;
    path=malloc(T*sizeof(int));

    for(int i=0;i<G2->V;i++){
        for(int j=0;j<G2->V;j++){
            if(G2->M[i][j].speciale==1 && flag==0){
                partenza= STsearchByName(G2->table,G2->M[i][j].id);
                flag=1;
                continue;
            }
            if (G2->M[i][j].speciale==1) arrivo=STsearchByName(G2->table,G2->M[i][j].id);
        }
    }
    allPath(G2,partenza,arrivo,visited, path,0,trovato);
    allPath(G2,arrivo,partenza,visited, path,0,trovato);

    free(visited);
    free(path);
}


void allPath(Graph G,int v, int w,int *visited,int *path,int curr_len,int *trovato) {
    int i;

    visited[v] = 1;
    path[curr_len] = v;
    if (v == w) { //condizione terminazione
        for (int i = 0; i < MAX; i++) {
            printf("%d ", path[i]);
            *trovato=1;
        }
    }

    for (i = 0; i < (2*G->V); i++) {
        if (G->madj[v][i]) {
            if (!visited[i]) {
                    visited[i] = 1;
                    allPath(G,v, w, visited, path, curr_len + 1,trovato);
                    visited[i] = 0;
            }
        }
    }
}


void ProbOttimizzazione(Graph G, int N, int T){
    Graph G3;
    G3= GRAPHinit(N);
    G3=G;

    int *sol;
    sol=malloc(MAX*sizeof(int));
    int *val;
    val=malloc((MAX)*sizeof(int));
    int trovato=0; //flag che modfico solo se trovo cammino
    disp_rip(0,val,sol,&trovato,G3,N,T);

}

void disp_rip(int pos,int *val,int *sol,int *trovato,Graph G3,int N,int T){
    int i;
    //chiamata funz che riceve mosse fino a quel momento. riceve schacchiera originale--<
    //chiata funz verifica cammino con quel set di mosse
    if(pos>=1){
        wrapper_funz_confronta_risultato(G3,sol,pos,N);//  --> costruisco il grafo---> controllo se esiste cammino
        wrapper_allPath(G3,N,T,trovato);
        if(*trovato==1) {
            printf("\n\ncammino ottimale: ");
            for(i=0;i<pos;i++){
                printf("%d ",sol[i]);
            }
            return;
        }
    }

    if(pos>=MAX){
            printf("Si e' superato il numero di mosse consentito.\n");
        return;
    }
    for(i=0;i<4;i++){ //sopra sotto dx sx
        sol[pos]=i;
        disp_rip(pos+1,val,sol,trovato,G3,N,T);
    }
    return;
}


int wrapper_funz_confronta_risultato(Graph G3,int *sol, int pos,int N){
    int i;
    Graph G4;
    G4= GRAPHinit(N);
    G4=G3;
    for(i=0;i<pos;i++){  //sol[pos] e' la mossa su,giu,dx,sx
        funz_confronta_risultato(G4,sol[i],N);
    }
}
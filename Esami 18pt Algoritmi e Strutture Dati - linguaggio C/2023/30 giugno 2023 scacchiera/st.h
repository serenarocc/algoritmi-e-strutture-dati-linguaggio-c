//
// Created by seren on 01/07/2023.
//

#ifndef ESAMEAPA2023_06_30_ST_H
#define ESAMEAPA2023_06_30_ST_H
typedef struct hashtable *ST;

ST STinit(int max);
void STfree(ST table);
int STsearchByName(ST table, int key);
int STsearchByKey(ST table, int key); //equivale a stsearchbyindex
int STinsert(ST table, int x);
void STprint(ST table);
#endif //ESAMEAPA2023_06_30_ST_H

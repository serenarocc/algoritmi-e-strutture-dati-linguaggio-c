#include <stdio.h>
#define MAX 3
#include "graph.h"

int main() {
    int flag_verifica=0;
    int N,T,operazione;
    Graph G;
    FILE *in;
    in=fopen("../grid.txt","r");
    fscanf(in,"%d %d",&N,&T);
    G= GRAPHinit(N);
    G=GRAPHreadfile(G,in,N,T);
    fclose(in);

   printf("Inserire da tastiera:\n 0-> problema di verifica\n 1->problema di ricerca e ottimizzazione\n");
   scanf("%d",&operazione);
   switch(operazione){
     case (0):
         printf("Problema di verifica:\n");
         in=fopen("../soluzione.txt","r");
         flag_verifica=ProbVerifica(G,in,N);
         if(flag_verifica==1) printf("Soluzione proposta e' valida");
         else { printf("La soluzione proposta nel file non e' valida");}
         fclose(in);
         break;
     case(1):
         printf("Problema di ricerca e ottimizzazione: \n");
           ProbOttimizzazione( G,  N, T);
         printf("Leggena mosse che puo' avere la tessera:\n 0->verso l'alto;\n1->verso il basso;\n2->verso destra;\n3->verso sinistra");
         break;
     default:
         printf("Comando non valido");
         break;
 }

   return 0;
}

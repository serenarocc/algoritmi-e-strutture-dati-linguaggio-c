//
// Created by seren on 01/07/2023.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "st.h"

struct hashtable{
    int max;
    int n;
    int *v;
};




ST STinit(int max){
    ST table;
    table = malloc(sizeof(*table));
    table->max = max;
    table->n = 0;
    table->v = malloc(max* sizeof(char));
    return table;
}

void STfree(ST table){
    int i;
    free(table->v);
    free(table);
}

int STsearchByName(ST table, int key){
    int i;
    for(i=0; i<table->n; i++){
        if(table->v[i]==key)
            return i;
    }
    return -1;
}

int STgetbyKey(ST table, int key){
    return table->v[key];
}

void STprint(ST table){
    int i;
    for(i=0; i<table->n; i++)
        printf("%2d | %10d\n",i,table->v[i]);
    printf("\n\n");
}

int STinsert(ST table, int x){
    if(table->n == table->max){
       table->v= realloc(table->v,2*table->max*sizeof(int*));
       table->max=2*table->max;
    }
  //  table->v[table->n++] = strdup(x);
    table->v[table->n++] = x;
    return 1;
}
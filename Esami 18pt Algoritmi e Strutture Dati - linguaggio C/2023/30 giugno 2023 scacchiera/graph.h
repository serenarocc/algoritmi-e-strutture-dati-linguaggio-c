//
// Created by seren on 01/07/2023.
//

#ifndef ESAMEAPA2023_06_30_GRAPH_H
#define ESAMEAPA2023_06_30_GRAPH_H
#include <stdio.h>

typedef struct{
    int v;
    int w;
}Edge;

typedef struct grafo *Graph;

Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHprintmatrix(Graph G);
Graph GRAPHreadfile(Graph G, FILE *in,int N,int T);
Graph GRAPHEdgeCreate(Graph G, int v,int w);
int GRAPHgetE(Graph G);
int GRAFHgetN(Graph G);
void GRAPHRemoveE(Graph G, int v,int w);
void GRAPHinsertE(Graph G, int id1, int id2);
void GRAPHprintmatrix(Graph G);
int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetV(Graph G);
void controllo_speciale(Graph G,int N);
int ProbVerifica (Graph G, FILE *in,int N);
int funz_confronta_risultato(Graph G1,int mossa,int N);
void ProbOttimizzazione(Graph G, int N, int T);

#endif //ESAMEAPA2023_06_30_GRAPH_H

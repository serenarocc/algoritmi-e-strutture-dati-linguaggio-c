
// LA PARTE 3 NON E' TERMINANTA, NON DA IN OUTPUT CIO' CHE E' RICHIESTO
//FINO A PARTE1 TUTTO OK
//NON CAPISCO A COSA SERVONO I FILE ASSOCIATI .h e .c


#include <stdio.h>
#include<stdlib.h>
#include<string.h>
//#define R 3   //R=room=sale disponibili
//#define S 5  //S=slot temporali durante il giorno
#define MAX 50
#define TEST 0
#define EMPTY NULL

typedef struct articolo_ {   //QUASI ADT
    char *title;
    char *speaker;
    int slot;
    char *topic;
}articolo;

typedef articolo* ARTICOLO;
//------------------------------------------------------------

typedef struct articoli_ {  //ADT PRIMA CLASSE
    int na; //numero articoli
    ARTICOLO *va; //vettore di articoloi= collezione articoli
}articoli;
typedef articoli* ARTICOLI;

//----------------------------------------------------------

typedef struct rs_{
    int room;
    int slot;
}rs;


//-----------------------------------------------------------

typedef struct schedule_{
    int nr;//num room
    int ns;//num slot
    // Table version NS x NR (rooms as columns make for an easier check of speakers overlap)
    ARTICOLO **table;  //matrice NSxNR = NUMSLOTxNUMROOM
    // Compact versione, a room-initial_slot pair for each paper
    rs *articolo_slot;
    // Params to evaluate goodness of this schedule
    int holes; // Empty slot not at the end of the day. Slot vuoti a inizio giornata
    int disp;// Topic dispersion among rooms= quanti argomenti non si svolgono nella stessa stanza
}schedule;

typedef schedule* SCHEDULE;

//----------------------------------------------------------------------

//PARTE 2
//??????????????
void SCHEDULEplaceArticolo(SCHEDULE s, ARTICOLO a, int index, int room, int slot){
    int i;
    //Place the paper= piazzo l'articolo
    for(i=slot;i<a->slot+slot;i++){
        s->table[i][room]=a;  //table e a sono dello stesso tipo ARTICOLO
    }
    s->articolo_slot[index].room=room;
    s->articolo_slot[index].slot=slot;
}

//????????????????
//mi assiucuro che l'articolo si possa sistemare in base all'orario e allo speaker
int SCHEDULEcheckArticolo(SCHEDULE s, ARTICOLO a, int index, int room, int slot){
    int i, j;
    // Make sure we can place this paper starting from this slot
    //assicuriamoci che possiamo sistemare questo articolo partendo dallo slot definito nel file
    for(i=slot;i<a->slot+slot;i++){  //i<a->slot+slot   sto incrementando della durata della presentazione
        if(s->table[i][room]!=EMPTY) //se lo slot temporaneo e' gia' occupato
            return 0;
        // Make sure the speaker is not in another room already
        //assicuriamoci che il relatore non sia gia' in un'altra stanza
        for(j=0;j<s->nr;j++) { //per tutte le stanze
            if(s->table[i][j] != EMPTY && s->table[i][j]->speaker == a->speaker) //?????????????
                return 0;
        }
    }
    return 1;
}

SCHEDULE SCHEDULEinit(int R,int S, int na){   //?????????????
    int i;
    SCHEDULE s;
    s=calloc(1,sizeof (*s));//e' vettore di dimensione 1 elemento di tipo struct s
    s->nr=R; //num sale assegnazione
    s->ns=S; //assegnazione num slot al gg
    s->articolo_slot= calloc(na,sizeof (rs)); // vettore di lunghezza nnum articoli di tipo struct rs che contiene variabili room e slot

    //creo matrice table dimensione: slot x room
    s->table=calloc(S,sizeof (ARTICOLO*)); //vett di tot slot giornalieri
    for(i=0;i<S;i++){  //ci sono tot slot giornalieri per ogni room
        s->table[i]=calloc(R,sizeof (ARTICOLO));
    }
    for(i=0;i<na;i++){//inizzializzo variabili del vettore srticolo_slot   a -1
        s->articolo_slot[i].room=s->articolo_slot[i].slot=-1;
    }
    return s; //ritorno la struct schedule
}

void check_file(ARTICOLI aa, int R, int S, char *fn){ //come parametro e' stato passato il nome del file come fn
    FILE *fp;
    fp=fopen(fn,"r"); //lettura file: possibile_soluzione.txt
    if(!fp) return;

    int room,slot,i=0,ok=1;
    SCHEDULE s;
    s= SCHEDULEinit(R,S,aa->na);

    // Leggi un file in cui abbiamo una linea per ogni articolo accopiato con una room
    while( (i<aa->na) && (fscanf(fp,"%d %d",&room,&slot)==2)){ //
        if(!SCHEDULEcheckArticolo(s,aa->va[i],i,room,slot)){
            ok=0;  //l'articolo on si puo' sistemare perche' c'e' qualcosa che non va nell'articolo o nello speaker
            break; //esco
        }
        SCHEDULEplaceArticolo(s,aa->va[i],i,room,slot); // paizzo l'articolo
    }
    if(i<aa->na) ok=0;
    if(!ok) printf("posizionamento e' %s valido\n",ok? " " :"non ");
}


//------------------------------------------------------------
//PARTE 3

void SCHEDULEremoveArticolo(SCHEDULE s, ARTICOLO a, int index, int room, int slot){
    //rimozione articolo
    int i;
    for(i=slot;i<a->slot+slot;i++){
        s->table[i][room]=EMPTY;
    }
    s->articolo_slot[index].room=-1;
    s->articolo_slot[index].slot=-1;
}




void SCHEDULEprint(SCHEDULE s){
    int i,j;
    for(i=0;i<s->ns;i++){
        for(j=0;j<s->nr;j++){
            if(s->table[i][j]){
                fprintf(stdout, "%s\t",s->table[i][j]->title);
            }
            else{
                fprintf(stdout,"---\t");
            }
            printf("\n");
        }
        printf("H= %d, D= %d\n",s->holes,s->disp);
    }
}



//CONTROLLARE CORRETTEZZA
void SCHEDULEeval(SCHEDULE s){
    //controllo che non ci siano slot vuoti
    int i,j,k;
    int h=0;
    int d=0;
    //per ogni room
    for(i=0;i<s->nr;i++){
        // For each slot backward
        ARTICOLO last=NULL;
        for(j=s->ns-1;j>=0;j--){
            if(s->table[j][i]){
                last=s->table[j][i];
            }
            if(!last){
                continue;
            }
            if(last && !s->table[j][i]){
                h++;
            }
        }
    }

    s->holes=h;

    //gli argomenti sono separati in stanze diverse
    int **tt=calloc(s->nr,sizeof(int));
    //per ogni stanza
    for(i=0;i<s->nr;i++){
        //per ogni slot occupato dall'inizio
        for(j=0;j<s->ns;){
            if(s->table[k][i]){
                // For each previous paper
                for(k=0;k<j;) {
                    if(s->table[k][i]) {
                        tt[i]=tt[i]+(strcmp(s->table[j][i]->topic, s->table[k][i]->topic)!=0);
                        k=k+s->table[k][i]->slot;
                    }
                    else{
                        k=k+1;
                    }
                }
                j=j+s->table[j][i]->slot;
            }
            else {
                j = j + 1;
            }
        }
        d += tt[i];
    }
    s->disp = d;
    free(tt);
}



void solve_r(ARTICOLI aa, SCHEDULE s, int pos){
    if(pos>=aa->na){
        SCHEDULEeval(s);
        //copia se e' la soluzione migliore
        SCHEDULEprint(s);
        return;
    }

    int i,j;
    //per ogni stanza
    for(i=0;i<s->nr;i++){
        //
        for(j=0;j<=s->ns - aa->va[pos]->slot;j++){
            if(SCHEDULEcheckArticolo(s, aa->va[pos], pos, i, j)) {
                SCHEDULEplaceArticolo(s, aa->va[pos], pos, i, j);
                solve_r(aa, s, pos+1);
                SCHEDULEremoveArticolo(s, aa->va[pos], pos, i, j);
            }
        }
    }
}


void solve(ARTICOLI aa, int R, int S){
    SCHEDULE s;
    s=SCHEDULEinit(R,S,aa->na);
    solve_r(aa,S,0);//funzione ricorsova
}







//------------------------------------------------------------
//PARTE 1

void ARTICOLOstore(FILE *fp,ARTICOLO a){
    if(!fp || !a) return;
    fprintf(fp,"%s %s %d %s\n",a->title,a->speaker,a->slot,a->topic); //stampa a video
}

void ARTICOLIstore(FILE *fp, ARTICOLI aa){
    if(!fp || !aa) return;

    int i;
    for(i=0; i<aa->na; i++){ //per tutti gli elementi del vettore stampo ogni sinolo articolo con i suoi dettagli
        ARTICOLOstore(fp,aa->va[i]);
    }
}

ARTICOLO ARTICOLOinit(){
    ARTICOLO a;
    a=calloc(1,sizeof (*a));
    return a;
}

ARTICOLO ARTICOLOread(FILE *fp){
    ARTICOLO a;
    a= ARTICOLOinit();
    char title_[MAX], speaker_[MAX], topic_[MAX];

    //leggendo da file non riesco direttamente a riempire la struttura quinin salvo in variabili
    //temporanee di tipo char per poi copiare tutto nella struct

    fscanf(fp,"%s %s %d %s\n",title_,speaker_,&a->slot,topic_);
    a->title= strdup(title_);
    a->speaker= strdup(speaker_);
    a->topic= strdup(topic_);

}

ARTICOLI ARTICOLIinit(int na){ //allocazione memoria per collezione/vettore di articoli
    ARTICOLI aa=calloc(1, sizeof(articoli));
    aa->na=na;
    aa->va=calloc(aa->na,sizeof(ARTICOLO));
    return aa;
}

ARTICOLI  ARTICOLIread(char *filename){
    int i;
    int na; //numero di articoli/righe che ci sono nel file
    FILE *fp= fopen(filename,"r");
    if(!fp) return NULL;

    fscanf(fp,"%d\n",&na); //acquisisco prima riga del file che indica quante righe in totale ci sono nel file
    ARTICOLI aa;
    aa=ARTICOLIinit(na);
    if(!aa) return NULL;

    //LEGGO TUTTE LE RIGHE DEL FILE
    for(i=0; i< aa->na; i++){
        aa->va[i]=ARTICOLOread(fp);
    }
    return aa;

}


int main(int argc, char **argv) {
    FILE *in;
    in =fopen("../articoli.txt","r");
    if(in=NULL)
        return -1;

    ARTICOLI aa;
    aa=ARTICOLIread("../articoli.txt");
    if(!aa) return -1;
    ARTICOLIstore(stdout,aa);//gli passo collezione articoli


    int R=3;
    int S=5;

   //PROBLEMA DI VERIFICA
   check_file(aa,R,S,"../possibile_soluzione.txt"); //gli passo la collezione di articoli, il num di stanze e di slot nella giornata

   //PROBLEMA DI RICERCA E OTTIMIZZAIONE
   solve(aa,R,S);
    return 0;
}

#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#include "Graph.h"

int main(int argc, char **argv) {

    Graph labirinto=NULL;


    int PF=20,G=5,M=6;

    /*
     * if (argc != 6) {
        printf("Use: %s <file_dungeon> <file_path> <PF> <G> <M>\n", argv[0]);
        exit(-1);
        }
         int PF = atoi(argv[3]);
         int G = atoi(argv[4]);
         int M = atoi(argv[5]);
    */

    FILE *in;
    in=fopen("../file.txt","r");
    if(in==NULL)
        exit(-1);

    labirinto= GRAPHload(in);
    if(labirinto==NULL)
        exit(-1);
    fclose(in);

    in = fopen("../path.c", "r");
    if (in == NULL)
        exit(-1);
    GRAPHtestPath(labirinto, PF, G, M, in);
    fclose(in);

    GRAPHfindPath(labirinto, PF, G, M);
    GRAPHfree(labirinto);
    return 0;
}

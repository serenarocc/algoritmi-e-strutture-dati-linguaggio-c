//
// Created by seren on 18/04/2023.
//

#ifndef APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_GRAPH_H
#define APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_GRAPH_H
#include <stdlib.h>
#include <stdio.h>
#include "ST.h"
#define MAXL 31

typedef struct G *Graph; //ADT 1 CLASSE GRAFO

Graph GRAPHinit(int nV);
Graph GRAPHload(FILE *);
void  GRAPHfree(Graph g);
char *GRAPHgetName(Graph g, int index);

void GRAPHtestPath(Graph g, int PF, int G, int M, FILE *path);
void GRAPHfindPath(Graph g, int PF, int G, int M);

#endif //APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_GRAPH_H

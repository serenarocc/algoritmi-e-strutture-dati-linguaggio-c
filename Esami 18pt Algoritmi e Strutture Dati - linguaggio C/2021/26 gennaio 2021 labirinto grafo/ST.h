//
// Created by seren on 18/04/2023.
//

#ifndef APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_ST_H
#define APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_ST_H
typedef struct hashtable *ST; //ADT 1 CLASSE

ST   STinit(int maxN);
void STfree(ST ht);
void STinsert(ST ht, char *str, int index);
int  STsearch(ST ht, char *str);
#endif //APPELLO18_2021_GENNAIO26_LABIRINTO_GRAFO_ST_H

//
// Created by seren on 18/04/2023.
//

#include "Graph.h"
#include "ST.h"
typedef struct{
    char nome[MAXL];//nome stanza
    int profondita;
    int tesoro;
    int oro;
}Stanza;

typedef struct{
    int trappola;
    int porta;
}Tunnel;

typedef struct node_{
    Tunnel *t;
    int dest;//?????
    struct node_ *next;
}node;
typedef node* link;

struct G{  //ADT 1 CLASSE GRAFO
    int V;//vertici
    int E;//archi
    link *ladj;
    Stanza *vett;
    ST st;
};

typedef struct solution_{
    int oro;
    int tesoro;
    int ricchezza_tot; //ricchezza totoale cumulata
    int len; //??? lunghezza cammino
    int *path; //vettore che contiene la sequenza di stanze
}solution;
//---------------------------------------------------------
solution SOLinit(int M) {
    solution s;
    s.len = 0;
    s.oro = 0;
    s.ricchezza_tot = 0;
    s.tesoro = 0;
    s.path = (int*) malloc (M * sizeof(int));
    return s;
}


Graph GRAPHinit(int nV){
    Graph g=malloc(sizeof(*g));
    if(g==NULL)
        return NULL;

    g->V=nV;
    g->E=0;

    g->ladj=(link*)calloc(nV,sizeof (link));
    if(g->ladj==NULL)
        return NULL;

    g->st= STinit(nV);
    if(g->st==NULL)
        return NULL;

    g->vett=(Stanza*)calloc(nV, sizeof(Stanza));
    if(g->vett==NULL)
        return NULL;

    return g;
}





link newNode(int stanzaA, Tunnel *t, link next){
    link n=(link)malloc(sizeof (node));
    n->dest=stanzaA;
    n->t=t;
    n->next=next;
}



void GRAPHinsertE(Graph g, int stanzA, int stanzaB, Tunnel *t){
    g->ladj[stanzA]=newNode(stanzaB,t,g->ladj[stanzA]);
    g->E++;
}


Graph GRAPHload(FILE *in){
    int nV;//num stanze/vertici
    int i;
    char stanzaA[MAXL], stanzaB[MAXL];
    int flagTrappola, porta, id1,id2;
    Graph g;
    fscanf(in,"%d",&nV);
    g= GRAPHinit(nV);
    if(g==NULL)
        return NULL;


    //lettura delle stanze nel file
    for(i=0;i<nV;i++){
        fscanf(in,"%s %d %d %d\n",g->vett[i].nome,&g->vett[i].profondita,&g->vett[i].tesoro,&g->vett[i].oro);
        printf("%s %d %d %d\n",g->vett[i].nome,g->vett[i].profondita,g->vett[i].tesoro,g->vett[i].oro);
        STinsert(g->st,g->vett[i].nome,i);
    }

    //lettura dei tunnel nel file
    while(fscanf(in, "%s %s %d %d\n",stanzaA,stanzaB,&flagTrappola,&porta)==4){
        printf("%s %s %d %d\n",stanzaA,stanzaB,flagTrappola,porta);
        id1= STsearch(g->st,stanzaA);
        id2= STsearch(g->st,stanzaB);
        if(id1!=id2 && id1>=0 && id2>=0){
            Tunnel *t;
            t=(Tunnel*)malloc(sizeof (Tunnel));
            t->porta=porta;
            t->trappola=flagTrappola;
            GRAPHinsertE(g,id1,id2,t);
            GRAPHinsertE(g,id2,id1,t); //grafo non orientato
        }
    }
    return g;
}


void GRAPHfree(Graph g){
    int v;
    link iter,del;
    for(v=0;v<g->V;v++){
        for(iter=g->ladj[v];iter;) {
            del = iter;
            iter=iter->next;
            if (v < del->dest) {
                // Tunnels are shared. Avoid double free
                free(del->t);
            }
            free(del);
        }
    }
    free(g->ladj);
    free(g->vett);
    STfree(g->st);
    free(g);
}
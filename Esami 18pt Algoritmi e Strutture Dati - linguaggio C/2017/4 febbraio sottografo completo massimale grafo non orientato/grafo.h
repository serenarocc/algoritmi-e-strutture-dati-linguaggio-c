//
// Created by seren on 12/06/2023.
//

#ifndef APPELLO18_2017_FEB4_SOTTOGRAFO_COMPLETO_MASSIMALE_GRAFO_NON_ORIENTATO_PESATO_GRAFO_H
#define APPELLO18_2017_FEB4_SOTTOGRAFO_COMPLETO_MASSIMALE_GRAFO_NON_ORIENTATO_PESATO_GRAFO_H
#include <stdio.h>
typedef struct{
    int v;
    int w;
    int wt;
}Edge;
typedef struct grafo *Graph;

Graph GRAPHinit(int V);
void GRAPHfree(Graph t);
void GRAPHinsertE(Graph G, int id1, int id2, int wt);
void GRAPHremoveE(Graph G, int id1, int i2, int wt);
Graph GRAPHreadFile(FILE *in);
void GRAPHladjcreate(Graph G);
void GRAPHprintmatrix(Graph G);
void GRAPHprintlist(Graph G);
int GRAPHedges(Graph G, int *v, int i);
int GRAPHgetE(Graph G);
int GRAPHgetV(Graph G);
char *GRAPHVertexname(Graph G, int i);
int GRAPHgetbyName(Graph G, char *id);
int checkCricca(Graph G, int *sol, int dim);


void generaCricche(Graph G);
void generaR(Graph G, int pos, int *sol, int *bestsol, int *bestwt, int start, int n, int k, int *bestdim);


#endif //APPELLO18_2017_FEB4_SOTTOGRAFO_COMPLETO_MASSIMALE_GRAFO_NON_ORIENTATO_PESATO_GRAFO_H

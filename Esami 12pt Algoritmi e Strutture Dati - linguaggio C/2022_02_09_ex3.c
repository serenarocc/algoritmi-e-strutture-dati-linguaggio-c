#include <stdlib.h>
#include <stdio.h>
void wrapper(int N,int k,int **M);
void powerset_comsempl(int pos,int start,int *sol,int N,int k,int **M,int *stop,int *bestInsieme);
int check(int *sol,int k,int **M,int **bestInsieme);
int **malloc2d(int r, int c);

int main(){
/*int M[4][4] = {
            {1,1,0,1},
            {1,1,0,1},
            {0,0,1,0},
            {1,1,0,1}
    };
*/
    int N=7;
    int **m = malloc2d(N, N);

    m[0][0]=1; m[0][1]=1; m[0][2]=0; m[0][3]=0;  m[0][4]=0; m[0][5]=0;  m[0][6]=0;
    m[1][0]=1; m[1][1]=1; m[1][2]=1; m[1][3]=0;  m[1][4]=0; m[1][5]=0;  m[1][6]=0;
    m[2][0]=0; m[2][1]=1; m[2][2]=1; m[2][3]=1;  m[2][4]=1; m[2][5]=1;  m[2][6]=1;
    m[3][0]=0; m[3][1]=0; m[3][2]=1, m[3][3]=1;  m[3][4]=0; m[3][5]=0;  m[3][6]=0;
    m[4][0]=0; m[4][1]=0; m[4][2]=1, m[4][3]=0;  m[4][4]=1; m[4][5]=1;  m[4][6]=1;
    m[5][0]=0; m[5][1]=0; m[5][2]=1, m[5][3]=0;  m[5][4]=1; m[5][5]=1;  m[5][6]=1;
    m[6][0]=0; m[6][1]=0; m[6][2]=1, m[6][3]=0;  m[6][4]=1; m[6][5]=1;  m[6][6]=1;

    int k=2;
    wrapper(N,k,m);
    return 0;
}

void wrapper(int N,int k,int **M){
    int *sol =  malloc(N*sizeof(int));
    int pos=0;
    int start=0;
    int i;
    int stop=0;
    int bestInsieme=0;

    for(i=k+1; i>1 && stop==0; i--){
        powerset_comsempl(pos,start,sol,N,i,M,&stop,&bestInsieme);
    }
}

//non uso alg er perche io non voglio sapere tutti gli insiemi che compongono la soluzione
// io voglio sapere della soluzione solo l'insieme più grande
void powerset_comsempl(int pos,int start,int *sol,int N,int k,int **M,int *stop,int *bestInsieme){
    int i;

   if((*stop)==1) return; //stampa una sola soluzione

    if(pos>=k){
        if(check(sol,k,M,&bestInsieme)){ //gli passo sol di pos con la possibile soluzione = il sottoinsieme che voglio stampare
            (*stop)=1;
            printf("{");
            for(i=0;i<k;i++) printf("%d ",sol[i]);
            printf("}");
            printf("\n");
        }
        return;
    }

    for(i=start;i<N;i++){ //per tutte le n persone. i indica la i-esima persona
        sol[pos]=i; //salvo la persona nel gruppo
        powerset_comsempl(pos+1,i+1,sol,N,k,M,stop,bestInsieme);
    }
}


int check(int *sol,int k,int **M,int **bestInsieme){
    int i,j;

    for(i=0;i<k;i++){ // i tiene conto con di ogni persona con quante amiche è nel gruppo
        for(j=i+1;j<k;j++){ //passo alla prossima persona del gruppo per controllare la stessa cosa
            if(M[sol[i]][sol[j]]==0) {  //non c'e' relazione di amicizia tra le due persone
                return 0;
            }
        }
    }
    return 1;
}


int **malloc2d(int r, int c){
    int **M = malloc(r*sizeof (int *));
    for(int i=0; i<c; i++)
        M[i]= malloc(c*sizeof (int));
    return M;
}

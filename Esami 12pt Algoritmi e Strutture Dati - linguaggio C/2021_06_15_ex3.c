#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct {
    char *s;
    int pos;
    int costo;
}sub;

void powersetCombSemplici(sub *S, int nSubs,char *start,char *end, int *sol, int *bestsol, int j, int pos, int startPower, int *contSostituzioni, int *bestCosto);
void solve(char *start, char *end, sub *S, int nSubs);
int check(sub *S,int *sol,int j, char *start,char *end);

int main() {
    char start[]="passato";
    char end[]="persona";
    int nSubs=8;
    sub *S=malloc(8*sizeof (sub));
    S[0].s="er";  S[0].pos=1; S[0].costo=4;
    S[1].s="ers"; S[1].pos=1; S[1].costo=5;
    S[2].s="sa";  S[2].pos=3; S[2].costo=1;
    S[3].s="so";  S[3].pos=3; S[3].costo=2;
    S[4].s="ato"; S[4].pos=0; S[4].costo=1;
    S[5].s="on";  S[5].pos=4; S[5].costo=2;
    S[6].s="ona"; S[6].pos=4; S[6].costo=3;
    S[7].s="a";   S[7].pos=6; S[7].costo=1;

    solve(start, end,S,nSubs);
    return 0;
}

void solve(char *start, char *end, sub *S, int nSubs){
    int *sol=malloc(nSubs*sizeof(int));
    int *bestsol=malloc(nSubs*sizeof(int));
    int contSostituzioni=0; //contatore delle sostituzioni che applico
    int bestCosto=INT_MAX;//devo minimizzare il costo
    int pos=0;
    int startPower=0;//variabile start del for del powerset


    //  for(int j=0;j<nSubs;j++){ //  ??????????????????????????????
    for(int j=nSubs;j>=1;j--){ //creo partizioni andando a vedere un po' alla volta tutto il dizionario
        powersetCombSemplici(S,  nSubs, start, end,  sol, bestsol,  j,  pos,  startPower, &contSostituzioni,&bestCosto);
    }

    if(contSostituzioni==0){ //non ho fatto sostituzioni
        printf("Non e' stato possibile trovare una soluzione");
    }else{//ho fatto sostituzioni
        printf("Costo migliore: %d\n",bestCosto); //minor costo tot di trasformazioni
        for(int i=0; i<contSostituzioni;i++){
            printf("%s ",S[bestsol[i]].s); //stampo sostituzioni
        }
    }
    free(sol);
    free(bestsol);
}



void powersetCombSemplici(sub *S, int nSubs,char *start,char *end, int *sol, int *bestsol, int j, int pos, int startPower, int *contSostituzioni, int *bestCosto){
    int i;
    int costoparziale;
    if(pos>=j){ // se ho ricorso su tutti gli elementi presi in considerazione del dizionario
        if(check(S,sol, j, start,end)==1){
            costoparziale=0;
            for(i=0;i<j;i++){
                costoparziale=costoparziale + S[sol[i]].costo;
            }

            if(costoparziale==(*bestCosto) && j>(*contSostituzioni)){ // a parita' di cosro scegliere sequenza di sostituzioni piu' lunghe
                (*contSostituzioni) = j;
            }
            else{
                if(costoparziale<(*bestCosto)){
                    (*bestCosto) = costoparziale;
                    (*contSostituzioni)=j;
                    for(i=0;i<j;i++) {
                        bestsol[i]=sol[i];
                    }
                }
            }
        }
        return;
    }

    for(i=startPower; i<nSubs;i++){
        sol[pos]=i;
        powersetCombSemplici(S,  nSubs, start, end,  sol, bestsol,  j,  pos+1,  i+1, contSostituzioni,bestCosto);
    }
}

int check(sub *S,int *sol,int j, char *start,char *end){
int i, indexSost;
    int lungStart=strlen(start);
    char *tmp= malloc(lungStart*sizeof(char));
    strcpy(tmp,start);

    int lungSostituz;
    for(i=0; i<j;i++){ //per tutte le parole del dizionario
        lungSostituz=strlen(S[sol[i]].s);
        for(indexSost=0;indexSost<lungSostituz;indexSost++){ //controllo un carattere alla volta
            //POSIZIONAMENTO
            tmp[S[sol[i]].pos+indexSost] = S[sol[i]].s[indexSost];

            //S[sol[i]].s[indexSost]  =parola del dizionario alla posizione sol[i]. Di questa parrola punto al carattere s[indexSost]
            /*tmp[S[sol[i]].pos+indexSost] :
             *      S[sol[i]].pos+indexSost=  sto prendendo la parola del dizionario che voglio sostituire. Di questa parola sto puntando alla sua posizione
             *      e per non ricorrere su target per cambiare un carattere alla volta faccio: +indexSost
            */
        }
    }

    int flag=strcmp(tmp,end);
    free(tmp);
    if(flag==0){
        return 1;
    }
    else{
        return 0;
    }
}

#include <stdio.h>
#include <stdlib.h>
void soluzione (int *v, int n,int s);
void powersetDispRip(int *v,int*sol,int n, int pos,int s,int *bestsol1, int *bestsol2, int *maxCardinalita, int *trovato);

int main() {
    int n=5;
    int s=4;
    //int v[] = {1, 2, 3,4,5};//insieme S
    int v[] = {7, 5, 3,10,1};//insieme S
    soluzione(v,n,s);
    printf("\ns= %d",s);
    return 0;
}

void soluzione (int *v, int n,int s){ //si suppone che s sia gia' fornito e rispetti le condizioni del testo
    int *sol=malloc(n*sizeof(int));
    int *bestsol1=malloc(n*sizeof(int));
    int *bestsol2=malloc(n*sizeof(int));
    int maxCardinalita = 0;
    int trovato = 0;
    powersetDispRip(v,sol,n,0,s, bestsol1,bestsol2, &maxCardinalita, &trovato);//insieme delle parti modello delle disposizioni semplici
    if (trovato) {
        printf("S1: { ");
        for (int i = 0; i < n; i++) {
            if (bestsol1[i] == 1) {
                printf("%d ", v[i]);
            }
        }
        printf("}\n");

        printf("S2: { ");
        for (int i = 0; i < n; i++) {
            if (bestsol2[i] == 1) {
                printf("%d ", v[i]);
            }
        }
        printf("}\n");
    } else {
        printf("Nessuna soluzione possibile.\n");
    }
    free(sol);
    free(bestsol1);
    free(bestsol2);
}

// powerset disposizioni ripetute
void powersetDispRip(int *v,int*sol,int n, int pos,int s,  int *bestsol1,int *bestsol2, int *maxCardinalita, int *trovato){//insieme delle parti modello delle disposizioni semplici
    int j;
    if(pos>=n){//condizione di terminazione
        int sum1=0,sum2=0, card1=0,card2=0;
        for (int i = 0; i < n; i++) {
            if (sol[i] == 0) {
                sum1 += v[i];//sottoinsieme s1
                card1++;
            } else {
                sum2 += v[i];//sottoinsieme s2
                card2++;
            }
        }
        if (sum1 == (sum2 + s) && card1 + card2 > *maxCardinalita) {//verifica che le condizioni del testo siano rispettate
            *maxCardinalita = card1 + card2;
            *trovato = 1;
            for (int i = 0; i < n; i++) {

                if (sol[i] == 1) {
                    bestsol1[i] = 1;
                } else {
                    bestsol1[i] = 0;
                }

                if (sol[i] == 0) {
                    bestsol2[i] = 1;
                } else {
                    bestsol2[i] = 0;
                }

            }
        }
        return;
    }
    sol[pos]=0;
    powersetDispRip(v,sol,n,pos+1,s,bestsol1,bestsol2, maxCardinalita, trovato);
    sol[pos]=1;//backtrack
    powersetDispRip(v,sol,n,pos+1,s,bestsol1,bestsol2, maxCardinalita, trovato);
    return;
}

//
// Created by seren on 08/02/2023.
//

#ifndef STRUTTURE_EXAM_EX2_ESAME_EX2_H
#define STRUTTURE_EXAM_EX2_ESAME_EX2_H

typedef struct t* link; //adt prima classe
struct t{ //quasi adt
    int val;
    link dx;
    link sx;
    int numFigli;
};


link newnode(link T, int val);
int f(link T, int val);
int camminoMax(link T, int val);

#endif //STRUTTURE_EXAM_EX2_ESAME_EX2_H

//
// Created by seren on 08/02/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include "esame_ex2.h"


link newnode(link T, int val){
    if(T!=NULL){
        if(val>T->val){
            T->dx= newnode(T->dx,val);
        }
        else if(val<T->val){
            T->sx= newnode(T->sx,val);
        }
    }
    else{
        T=(link)malloc(sizeof(struct t));
        T->sx=NULL;
        T->dx=NULL;
        T->val=val;
    }
    return T;
}


int f(link T, int val){
    if(!T){
        return 0;
    }
    if(val>0 || val==0 ){
        camminoMax(T,val);
    }
    else{
        printf("Il cammino massimo comprende valori negativi\n");
        return 0;
    }
}


int camminoMax(link T, int val){
        if(T->val ==val ) return 1;
        if(T->val >val) return 1+ camminoMax(T->sx, val);
        else return 1+camminoMax(T->dx,val);
}

#include <stdio.h>
#include <stdlib.h>
int **malloc2d(int r, int c);
void wrapper_er(int **mat,int n);
void algoritmoEr(int n, int m,int Ninsiemimax,int pos, int *sol, int *bestsol, int **mat, int *minInsieme);
int checksol2(int **mat, int m, int n, int *sol);

int main() {
    int n=5, k=2;
    int **m = malloc2d(n, n);

       m[0][0]=0; m[0][1]=2; m[0][2]=-1; m[0][3]=-1;  m[0][4]=-1;
       m[1][0]=2; m[1][1]=0; m[1][2]=3; m[1][3]=-1;  m[1][4]=-1;
       m[2][0]=-1; m[2][1]=3; m[2][2]=0; m[2][3]=1;  m[2][4]=4;
       m[3][0]=-1; m[3][1]=-1; m[3][2]=1, m[3][3]=0;  m[3][4]=-1;
       m[4][0]=-1; m[4][1]=-1; m[4][2]=4, m[4][3]=1;  m[4][4]=0;

    wrapper_er(m,n);
    for(int i=0;i<n;i++){
       free(m[i]);
    }
    free(m);
    return 0;
}

void wrapper_er(int **mat,int n) {
    int i;
    int *sol = malloc(n * sizeof(int));
    int *bestsol = malloc(n * sizeof(int));
    int minInsieme = n;

    algoritmoEr(n, 0, i, 0, sol, bestsol, mat, &minInsieme);

    free(sol);
    free(bestsol);
}

void algoritmoEr(int n, int m,int Ninsiemimax,int pos, int *sol, int *bestsol, int **mat, int *minInsieme){
    int i;
    if(pos>=n){//condizione terminazione
        if(m<=*minInsieme){
            if(checksol2(mat,m,n,sol)==1){ //verifica validità soluzione
                *minInsieme=m;
                for(i=0;i<n;i++){
                    printf(" %d ", sol[i]);
                }printf("\n");
            }
        }
        return;
    }
    for(i=0;i<m;i++){
        sol[pos]=i;//ricorsione sugli oggetti
        algoritmoEr(n,m,Ninsiemimax,pos+1,sol,bestsol,mat,minInsieme);
    }
    sol[pos]=m;//ricorsione su oggetti e blocchi
    algoritmoEr(n,m+1,Ninsiemimax,pos+1,sol,bestsol,mat,minInsieme);
}

int cityDirectlyConnected2(int **m, int i, int j ){
    if(m[i][j]!=-1 && m[i][j]!=INT_MAX )
        return 1; //ritorno 1 solo se e' direttamente connessa
    return -1; //citta i e j non sono direttamente connesse
}

//avevo gia' controllato in precedenza se i (city1) e' o no gia' direttamente connessa a k (interCity)
int cityInterconneted2(int **m, int city1,int city2,int interCity){ //cerco se city1 e city2 sono interconnesse da interCity
    if(cityDirectlyConnected2(m,interCity,city2)==1 ){
        return INT_MAX;  //m[city1][city2]=INT_MAX
    }
    else{
        return -1; //non c'e' connessione
    }
}

int checksol2(int **mat, int m, int n, int *sol) {
    int i, j;
    for(int group = 0; group < m; group++){// per tutti i blocchi (gruppi di citta') (gruppi di amici)
        for (i = 0; i < n; i++) {//per tutti gli oggetti (le citta') (le persone)
            if(sol[i] != group) continue; //se la citta' non appartiene a quel gruppo, continua passando al prossimo
            for (j = 0; j < n; j++) { //parto dalla colonna dopo di citta'   --> citta' B
                //devo controllare che la città i sia connessa con tutte le altre citta di sol
                if (i == j || sol[j] != group) continue; //se citta' B non appartiene al gruppo e se citta' A e' la stessa di citta' B, continua passando al prossimo
                int connected = 0;
                if (cityDirectlyConnected2(mat, i, j) == -1) {
                    //non direttamente connessi i e j
                    //cerchiamo una interconnessione attraverso k
                    for (int k = 0; k < n; k++) {
                        if (k == j || k==i || cityDirectlyConnected2(mat,i,k)==-1 || sol[k]!=sol[i]) continue; //non possiamo provare k se k e' gia' connesso in modo intermedio
                        if (cityInterconneted2(mat, i, j, k) != -1) {
                            connected = 1;
                            break;
                        }
                    }
                    if(connected==0)
                        return -1;
                }
            }
        }
    }
    return 1;
}

int **malloc2d(int r, int c){ //queto e' il malloc correto
    int **m=malloc(r*sizeof (int *));
    for(int i=0;i<r;i++){
        m[i]= malloc(c*sizeof (int));
    }
    return m;
}

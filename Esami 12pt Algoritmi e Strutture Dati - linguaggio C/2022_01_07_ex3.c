#include <stdio.h>
#include <stdlib.h>

void wrapper(int M[4][4],int N);
void algoritmoER(int N,int m,int pos,int *sol,int M[4][4],int *stop, int *bestInsiemi);
void controlla(int M[4][4],int *sol,int m,int N,int **stop, int **bestInsiemi);

/*NOME VERO: 2022_01_27_ex3
 * ho utilizzato l'algoritmo di Er
 *
 * trova il minor gruppo di persone possibili facendo in modo che tutte le persone in un gruppo siano mutalmente amiche
 *
 * gruppo persone mutualmente amiche = TUTTI IN UN GRUPPO DEVONO ESSERE AMICI CON TUTTI
 *
 */

int main(){
   /* int M[4][4] = {
            {1,1,0,1},
            {1,1,0,1},
            {0,0,1,0},
            {1,1,0,1}
    };*/
    int M[5][5] = {
            {1,1,0,0,0},
            {1,1,1,0,0},
            {0,1,1,1,1},
            {0,0,1,1,0},
            {0,0,1,0,1},
    };
    int N=5;
    wrapper(M,N);
    return 0;
}

void wrapper(int M[4][4],int N){
    int *sol = (int *) malloc(N*sizeof(int));
    int m=0;
    int pos=0;
    int stop=0;
    int bestInsiemi=100;
    algoritmoER(N,m,pos,sol,M,&stop, &bestInsiemi);//uso variabile stop in modo tale che quando trovo una soluzione valida mi fermo, in quanto


    free(sol);
}

void algoritmoER(int N,int m,int pos,int *sol,int M[4][4],int *stop, int *bestInsiemi){
    int i;

    //  if(*(stop)==1) return;

    if(pos>=N){
        controlla(M,sol,m,N,&(stop), &bestInsiemi);
        return;
    }

    for(i=0;i<m;i++){
        sol[pos]=i;
        algoritmoER(N,m,pos+1,sol,M,stop, bestInsiemi);
    }
    sol[pos]=m;
    algoritmoER(N,m+1,pos+1,sol,M,stop, bestInsiemi);
}

void controlla(int M[4][4],int *sol,int m,int N,int **stop, int **bestInsiemi) {
    int i, j,k;
    int flag = 1;
    int numInsiemi=0;

    for (i = 0; i < m; i++) { // per tutti i blocchi (gruppi di amici)
        for (j = 0; j < N; j++) { //per tutti gli oggetti  (le persone)   --> persona A
            if (sol[j] == i){ //se la persona appartiene a quel gruppo di amici (oggetto appartiene a blocco)
                for (k = j + 1; k < N; k++){ //parto dalla colonna dopo di persone   --> persona B
                    if (sol[k] == i){ //se la persona dopo appartiene allo stesso gruppo di amici
                        if (M[j][k] == 0){ //se  esiste corrispondenza tra la persona A e quella B
                            flag = 0; //collegamento non trovato
                            numInsiemi=m+1;
                        }
                    }
                }
            }
        }
    }

    if(numInsiemi< **bestInsiemi){
        **bestInsiemi=numInsiemi;
        if (flag == 1) {
            (**stop) = 1; //ho trovato una soluzione posso terminare alla prima
            for (i = 0; i < m; i++) {//stampo soluz
                printf("{ ");
                for (j = 0; j < N; j++){
                    if (sol[j] == i){
                        printf("%d ", j);
                    }
                }
                printf("}  ");
            }
            printf("\n  ");
        }
    }
}

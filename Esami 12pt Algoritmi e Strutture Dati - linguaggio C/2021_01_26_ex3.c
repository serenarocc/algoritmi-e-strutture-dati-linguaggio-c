#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int check(int *sol,int indiceDict,char dict[8][5],char *str);
void solve(char *str, char dict[8][5], int n, int s);
void solveR(char *str,char dict[8][5],int n,int *sol,int pos,int indiceDict,int *mark, int *stop, int s);



//se debuggo funziona tutto e stampa ma una volta che faccio play non stampa



int main() {
    char str[] = "abracadabra";
    int n=8;
    char dict[8][5]= {
            {"a"},
            {"ab"},
            {"cada"},
            {"abra"},
            {"ra"},
            {"da"},
            {"ca"},
            {"bra"}
    };
    int s=3;
    solve(str,dict,n,s);
    return 0;
}
void solve(char *str,char dict[8][5], int n, int s){
    int *sol=malloc(n*sizeof(int));
    int *mark=malloc(n*sizeof(int));

    int pos=0;
    int stop=0;
    for(int indiceDict=0; indiceDict<n && stop==0; indiceDict++){
            solveR(str,dict,n,sol,pos,n,mark, &stop,s);
    }
    free(sol);
    free(mark);
    return;
}
// DISPOSIZIONI CON RIPETIZIONE
void solveR(char *str,char dict[8][5],int n,int *sol,int pos,int indiceDict,int *mark, int *stop, int s){

    int i;
    if((*stop)==1) return;
    if(pos>=indiceDict){
        if(check(sol,indiceDict,dict,str)==1){
            for(i=0;i<indiceDict;i++){
                printf("%s ",dict[sol[i]]);
            }
            printf("\n");
            (*stop)=1;
        }
        return;
    }
    for(i=0;i<n;i++){
        if(mark[i]<s){
            mark[i]++;
            sol[pos]=i;
            solveR(str,dict,n,sol,pos+1,indiceDict,mark, stop,s);
            mark[i]--;
        }
    }
    return;
}

int check(int *sol,int indiceDict,char dict[8][5],char *str){
    int i,j;
    int k=0;
    int lunghezzastr= strlen(str);
    int lunghezzaParolaDict;

    for(i=0;i<indiceDict;i++){
        lunghezzaParolaDict= strlen(dict[sol[i]]);
        for(j=0;j<lunghezzaParolaDict;j++){
            if(dict[sol[i]][j] != str[k]) return 0;
            k++;
            if(k>lunghezzastr) return 0;
        }
    }
    if(k==lunghezzastr) return 1;
    return 0;
}

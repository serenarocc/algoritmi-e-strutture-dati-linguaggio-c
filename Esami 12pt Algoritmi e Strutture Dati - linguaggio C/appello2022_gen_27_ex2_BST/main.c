#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bst.h"

//TUTTO CIO' CHE C'E' DA SAPERE SU BST E TUTTE LE POSSIBILI OPERAZIONI SOPRA

int main() {
    /*
                   20
                 /    \
                /      \
               5       30
             /   \     /\
            /     \   /  \
           1      15 25  40
                /          \
               /            \
              9             45
            /   \          /
           /     \        /
          7      12      42
    */
    link root=NULL;
    root = new_node(20);
    //inserimento nodi in BST
    insert(root,5);
    insert(root,1);
    insert(root,15);
    insert(root,9);
    insert(root,7);
    insert(root,12);
    insert(root,30);
    insert(root,25);
    insert(root,40);
    insert(root, 45);
    insert(root, 42);




    //RICHIESTA ESERCIZIO 2 APPELLO 27 GEN 2022
    //verifica se albero e' o no un BST
    isBST(root);
    printf("\n");

    //RICHIESTA ESERCIZIO 2 APPELLO 9 FEBBRAIO 2022
    //decodifica di huffman
    char str[7];
    printf("inserisci stringa huffman:\n");
    scanf("%s",str);
    decode(root,str);


    printf("INORDER:\n");
    inorder(root); //lettura inorder
    printf("\n");

    printf("PREORDER:\n");
    preorder(root);
    printf("\n");


    printf("POSTORDER:\n");
    postorder(root);
    printf("\n");


    search(root,30);//ricerca del nodo in BST
    printf("\n");

    find_minimum(root);//ricerca del valore minimo in BST
    printf("\n");
    find_max(root);//ricerca del valore max in BST
    printf("\n");

    insert_radice(root,21);//inserzione nuovo elemento in radice
    inorder(root); //lettura inorder
    printf("\n");

    //contatore tutti i nodi di un albero
    int n;
    n=count(root);
    printf("Ci sono %d nodi nel BST\n",n);

    //controllo se BST e' vuoto
    isempty(root);






    return 0;
}

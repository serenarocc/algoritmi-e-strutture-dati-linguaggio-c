//
// Created by seren on 14/02/2023.
//

#ifndef MAIN_C_BST_H
#define MAIN_C_BST_H

typedef struct BSTnode *link;
struct BSTnode{
    int valore;
    link left;
    link right;
};

 link new_node(int val);
 link insert(link root, int x);
void inorder(link root);
void preorder(link root);
void postorder(link root);
link search (link root, int x);
link find_minimum(link root);
link find_max(link root);
link insert_radice(link root,int x);
int count(link root);
void isempty(link root);
void isBST(link root);
int isBST_visita(link root);
char decode (link root, char *str);
#endif //MAIN_C_BST_H

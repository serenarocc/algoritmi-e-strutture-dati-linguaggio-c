//
// Created by seren on 14/02/2023.
//

#include "bst.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


link new_node(int x){
    link p;
    p= malloc(sizeof (*p));
    p->valore=x;
    p->left=NULL;
    p->right=NULL;
    return p;
}

//inserzione n BST METODO RICORSIVO
 link insert(link root, int x){
    //searching for the place to insert
    if(root==NULL)//al posto di nodo sentinella ho NULL
        return new_node(x);
    else if(x>root->valore) // x is greater. Should be inserted to right
        root->right = insert(root->right, x);
    else // x is smaller should be inserted to left
        root->left = insert(root->left,x);
    return root;
}


void inorder(link root)
{
    if(root!=NULL) // checking if the root is not null
    {
        inorder(root->left); // visiting left child
  //      left>root_>val no bst
        printf(" %d ", root->valore); // printing data at root
  //      right<root->val no bst
        inorder(root->right);// visiting right child
    }
    return;
}

void preorder(link root)
{
    if(root!=NULL) // checking if the root is not null
    {
        printf(" %d ", root->valore); // printing data at root
        preorder(root->left); // visiting left child
        preorder(root->right);// visiting right child
    }
}

void postorder(link root)
{
    if(root!=NULL) // checking if the root is not null
    {
        postorder(root->left); // visiting left child
        postorder(root->right);// visiting right child
        printf(" %d ", root->valore); // printing data at root
    }
}




link search(link root, int x){
    if(root==NULL||root->valore==x){//if root->valore is x then the element is found
        printf("valore trovato %d", root->valore);
        return root;
    }
    else{
        if(x>root->valore) // x is greater, so we will search the right subtree
            return search(root->right,x);

        else//x is smaller than the data, so we will search the left subtree
            return search(root->left, x);
    }
}


//function to find the minimum value in a node
link find_minimum(link root){
    if(root==NULL){
        return NULL;
    }
    else{
        if(root->left!=NULL) // node with minimum value will have no left child
            return find_minimum(root->left); // left most element will be minimum
    }
    printf("il valore minimo e':%d",root->valore);
    return root;
}

//function to find the max value in a node
link find_max(link root){
    if(root==NULL){
        return NULL;
    }
    else{
        if(root->right!=NULL) // node with max value will have no right child
            return find_max(root->right); // right most element will be max
    }
    printf("il valore massimo e':%d",root->valore);
    return root;
}


//inserzione in radice
link insert_radice(link root,int x){
    if(root==NULL){
        return new_node(x);
    }
    if(x<root->valore){
        root->left= insert(root->left,x);
    }
    else{
        root->right= insert(root->right,x);
    }
    return root;
}


//contatore tutti i nodi di un albero
int count(link root){
    if(root==NULL)
        return 0;
    return count(root->left)+count(root->right)+1;
}

void isempty(link root){
    int m;
    m=count(root);
    if(m==0)  printf("BST vuoto\n");
    else  printf("BST non vuoto\n");
}




int isBST_visita(link root) { //SEGUO UNA SPECIE DI VISITA INORDER
    if (root != NULL) // checking if the root is not null
    {
        isBST_visita(root->left); // visiting left child
        if(root->left!=NULL){
            if (root->valore < root->left->valore ) {
                return 1;
            }
        }
            printf(" %d ", root->valore); // printing data at root

            if(root->right!=NULL){
                if (root->valore > root->right->valore) {
                    return 1;
                }
            }
            isBST_visita(root->right);// visiting right child

    }
}
void isBST(link root){
    int flagRisposta=0;

    flagRisposta=isBST_visita(root);

    if(flagRisposta==0) printf("E' un BST");
    else printf("Non e' un BST");
}





link huffman_visit(link root, char c){
    if(root==NULL|| c==root->valore) {
        return root;
    }
}


char decode(link root, char *str){
    int valore_nodo;

    int lunghezza = strlen(str);

    for(int i=0;i<lunghezza;i++) {
        if(str[i]!='0'&& str[i]!='1'){
            printf("errore stringa huffman\n");
            return -1;
        }


        if(str[i]=='0'){ //se 0 scendo a sx
            printf("%d\n",root->left->valore);//stampa di controllo
            huffman_visit(root->left, str[i]);
            root=root->left;

        }
        else{//se 1 scendo a dx
            printf("%d\n",root->right->valore);//stampa di controllo
            huffman_visit(root->right, str[i]);
            root=root->right;
        }

    }
    printf("valore trovato: %d\n", root->valore);
}

//
// Created by seren on 22/02/2023.
//

#include "list.h"
#include <stdlib.h>
#include <stdio.h>

typedef struct nodo *link;
struct nodo{
    int val;
    link next;
    link prec;
};

struct lista_s{
    link head;
    int nNodi;
};


static link newnodo(int val, link next, link prec){
    link p;
    p=malloc(sizeof(*p));
    if(p==NULL) return NULL;
    else{
        p->val=val;
        p->next=next;
        p->prec=prec;
    }
    return p;
}

LISTA listInit(){
    LISTA lista=malloc(sizeof(*lista));
    lista->head=NULL;
    lista->nNodi=0;
    return lista;
}


static void nodofree(link head){
    if(head==NULL)
        return;
    nodofree(head->next);
    nodofree(head->prec);
    free(head);
}

void LISTAfree(LISTA lista){
    if (lista==NULL)
        return;
    nodofree(lista->head);
    free(lista);
}


static link listInHead(link h, int val){
    h=newnodo(val,h,NULL);
    return h;
}

void wrapperInHead(LISTA lista,int val){
    lista->head=listInHead(lista->head,val);
}


static link listInTail(link h, int val){
    int lunghezza=0,i;
    link x, xprec;
    if(h==NULL)
        return newnodo(val,NULL,NULL);
    for(x=h;x->next!=NULL;x=x->next){
        lunghezza++; //calcolo lunghezza lista fin ora
    }
    for(x=h,i=0;x->next!=NULL,i<lunghezza;x=x->next,i++);
        xprec=x; //salvo il penultimo nodo

    for(x=h;x->next!=NULL;x=x->next);//inserisco ultimo nodo
            x->next= newnodo(val,NULL,xprec );
    return h;
}


void wrapperTail(LISTA lista, int val){
    lista->head=listInTail(lista->head, val);
    lista->nNodi++;
}


static void stampalista(link x1){
    for(;x1!=NULL;x1=x1->next){
        printf("%d ",x1->val);
    }
}

void stampaWrapper(LISTA lista){
    stampalista(lista->head);
}



void f(LISTA l, int a, int b) {
    link xprec,x1=l->head;
    int i = 0;
    int j;
    for (link x =l->head ;x->next != NULL; x = x->next) {
        i++;
        if (x->val>=a) {
                for(j=0; j<i-2;j++){
                    printf("%d ",x1->val);
                    x1 = x1->next;
                }

                for (link x2 = l->head; x2 != NULL; x2 = x2->next) {
                    if (x2->val >= b) {
                        x1->next = x2;
                        stampalista(x1);
                        return;
                    }
                }
        }
    }
}


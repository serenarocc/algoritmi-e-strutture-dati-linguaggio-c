//
// Created by seren on 22/02/2023.
//

#ifndef ESAME_FEB_EX2_LIST_H
#define ESAME_FEB_EX2_LIST_H

typedef struct lista_s *LISTA;

LISTA listInit();
void LISTAfree(LISTA lista);
void wrapperInHead(LISTA lista,int val);
void wrapperTail(LISTA lista, int val);
void stampaWrapper(LISTA lista);
void f(LISTA x, int a, int b);

#endif //ESAME_FEB_EX2_LIST_H

#include <stdio.h>
#include <stdlib.h>
void wrapper_er(int *vett, int d,int x);
void algoritmoEr(int n,int k,int m,int pos,int *sol, int *val, int *solution_founded);
int check_sol(int n,int k, int *sol,int *val);

int main() {
    int vett[7]={3,2,1,3,2,4,1};
    int d=7;
    int x=2;
    printf("Stampo vettore:\n");
    for(int i=0;i<d;i++){
        printf(" %d ",vett[i]);
    }
    printf("\n");
    wrapper_er(vett,d,x);
    return 0;
}
void wrapper_er(int *vett, int d,int x){
    int *sol;
    int solution_founded=0;
    sol=malloc(d*sizeof (int));
    algoritmoEr(d,x,0,0,sol,vett, &solution_founded);
}

void algoritmoEr(int n,int k,int m,int pos,int *sol, int *val, int *solution_founded){
    int i,j;
    if(*solution_founded==0){
        if(pos>=n){
            if(m==k){
                int ris=check_sol(n,k,sol,val);
                if(ris==1){
                    //METODO 1: stampa per sottoinsiemi
                    printf("Stampa sottoinsiemi:\n");
                    for(i=0;i<m;i++){
                        printf("{");
                        for(j=0;j<n;j++){
                            if(sol[j]==i){
                                printf(" %d ",val[j]);
                            }
                        }
                        printf("} ");
                    }
                    //METODO 2: stampa vett sol
                    printf("\nStampa vett sol:");
                    for(j=0;j<n;j++) {
                        printf(" %d ", sol[j]);
                    }
                    *solution_founded=1;
                    printf("\n");
                }
            }
            return;
        }
        for(i=0;i<m;i++){
            sol[pos]=i;
            algoritmoEr(n,k,m,pos+1,sol,val,  solution_founded);
        }
        sol[pos]=m;
        algoritmoEr(n,k,m+1,pos+1,sol,val,solution_founded);
    }

}


int check_sol(int n,int k, int *sol,int *val){
    int somme[k];
    for(int j=0;j<k;j++)
        somme[j]=0;

  for(int i=0;i<n;i++){  // sol:0,0,1,0,1,1,1
       /* for(int j=0; j<k;j++){  //ho usato j per trovare sol[i] quando lo ho gia'   METODO ALTERNATIVO
            if(sol[i]==j){
                somme[j]=somme[j]+val[i];
            }
        }*/
      somme[sol[i]]=somme[sol[i]]+val[i];
  }
  for(int j=1;j<k;j++){
      if(somme[j]!=somme[j-1])
          return 0;//false
  }
    return 1;
}

#include <stdio.h>
#include "LIST.h"
/*cancello in una lista singola linkata i nodi che si ripetono
 * 1->1->2->2->2->5->7->7
 * 1->2->5->7
 * */
int main() {//GIUSTO
    LIST l=NULL;
    l=wrapper_listInHead( l,1);
    wrapper_listInTail(l,1);
    wrapper_listInTail(l,2);
    wrapper_listInTail(l,2);
    wrapper_listInTail(l,2);
    wrapper_listInTail(l,5);
    wrapper_listInTail(l,7);
    wrapper_listInTail(l,7);

    printf("\nLista prima dell'eliminazione dei nodi che si ripetono:\n");
    printList( l);

    f(l);

    printf("\nLista dopo l'eliminazione:\n");
    printList( l);
    return 0;
}

#ifndef INC_2023_05_11_EX2_LISTASINGOLALINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H
#define INC_2023_05_11_EX2_LISTASINGOLALINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H

typedef struct lista *LIST; //adt 1 classe

typedef struct node *link; //quasi ADT
struct node{
    int val;
    link next;
};

LIST wrapper_listInHead(LIST l, int val);
LIST wrapper_listInTail(LIST l,int val);
void printList(LIST l);
void f(LIST l);

#endif //INC_2023_05_11_EX2_LISTASINGOLALINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H

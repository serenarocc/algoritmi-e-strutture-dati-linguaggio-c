
#include "LIST.h"
#include <stdlib.h>
#include <stdio.h>

struct lista{//adt 1 classe
    link head;
    link tail;
};

link new_node(int val, link next){
    link x;
    x= malloc(sizeof *x);
    if(x==NULL) return NULL;
    else{
        x->val=val;
        x->next=next;
    }
    return x;
}

link listInHead(link head, int val){
    head=new_node(val,NULL);//?????
    return head;
}

LIST wrapper_listInHead(LIST l, int val){
    l=malloc(sizeof *l);
    l->head=listInHead(NULL,val);
    return l;
}

link listInTail(link head, int val){
    link x;
    if(head==NULL)
        return new_node(val,NULL);
    for(x=head;x->next!=NULL;x=x->next);// arriva a fine lista
    x->next= new_node(val,NULL);
    return head;
}

LIST wrapper_listInTail(LIST l, int val){
    l->tail=listInTail(l->head,val);
    return l;
}

void printList(LIST l){
    link curr=l->head;
    while(curr!=NULL){
        printf(" %d ", curr->val);
        curr=curr->next;
    }
}

void f(LIST l) {//LISTA SINGOLA LINKATA
    link nodoCurr;
    link nodoNext;

    if(l!=NULL){
        nodoCurr=l->head; //parto dalla testa della lista

        while(nodoCurr!=NULL && nodoCurr->next !=NULL){ //fino a che non raggiungo la fine della lista
            nodoNext=nodoCurr->next; //passo al nodo successivo

            if(nodoCurr->val==nodoNext->val){ //se nodo corrente e' uguale al nodo successivo
                nodoCurr->next=nodoNext->next; //salto il nodo successivo collegando il next del nodo_corrente al next del nodo_next
                free(nodoNext);//libero nodo next
            }else{
                nodoCurr=nodoNext;//caso in cui nodo corrente non e' uguale al nodo successivo-> il nodo corrente diventa il successivo
            }

        }
    }
}

#include <stdio.h>
#include <stdlib.h>
int cmpfunc (const void * a, const void * b);
void wrapper2(int I[5], int dimI,int S[2], int dimS);
void powerset_combsempl2(int I[5], int dimI,int S[2], int dimS, int *sol, int indiceI,int pos, int start, int *mark, int *indiceS, int *stop);
int check2(int *sol,int *mark,int I[5],int S[2],int  dimI,int dimS,int indiceI, int **indiceS);

int main() {
    int dimI=6, dimS=3;
    int I[]={1,2,3,4,5,6};
    int S[]={1,7,7};
    wrapper2(I,dimI,S,dimS);
    return 0;
}
int cmpfunc (const void * a, const void * b) {
    return ( *(int*)a - *(int*)b );
}
void wrapper2(int I[5], int dimI,int S[2], int dimS){
    qsort(I,dimI,sizeof(int),cmpfunc); //ordino in modo crescente i vettori di partenza
    qsort(S,dimS,sizeof(int),cmpfunc);

    int *sol= malloc(dimI* sizeof (int));
    int *mark=malloc(dimI* sizeof (int));
    for(int i=0; i<dimI;i++){
        mark[i]=0;
    }

    int pos=0;
    int start=0;
    int indiceI;
    int indiceS=0;
    int stop=0;
    for(indiceI=1;indiceI<=dimI && stop==0;indiceI++){
        powerset_combsempl2(I, dimI,S, dimS,  sol, indiceI, pos, start, mark,&indiceS, &stop);
    }
    if(stop==0) printf("Non e' possibile ottenere il risultato voluto");
    else printf("E' possibile ottenere il risultato");
    free(sol);
    free(mark);
}

void powerset_combsempl2(int I[5], int dimI,int S[2], int dimS, int *sol, int indiceI,int pos, int start, int *mark, int *indiceS, int *stop){
    int i;
    if((*stop)==1) return; //mi blocco alla prima soluzione trovata
    if (pos>=indiceI){
        if (check2(sol,mark,I,S, dimI,dimS,indiceI,&indiceS)==1){
            (*stop)=1;
            return;
        }
    }
    for(i=start;i<dimI;i++){
        if(mark[i]==0 && I[i]<=S[(*indiceS)]){ //pruning
            sol[pos]=i;
            powerset_combsempl2(I, dimI,S, dimS,  sol, indiceI,pos+1, i+1, mark, indiceS,stop);
        }
    }
    return;
}

int check2(int *sol,int *mark,int I[5],int S[2],int  dimI,int dimS,int indiceI, int **indiceS){
    int sommaParziale=0;
    for(int i=0; i<indiceI; i++){
        if(mark[sol[i]]!=1){
            sommaParziale=sommaParziale+I[sol[i]];
        }
    }
    if (sommaParziale == S[(**indiceS)]){
        (** indiceS)++; //NB le parentesi
        for(int i=0; i<indiceI;i++){
            mark[sol[i]]=1; //ho preso quell'elemento
        }
        for(int i=0;i<indiceI;i++) printf("%d ",I[sol[i]]);
        printf("\n");
    }
    if((**indiceS)==dimS){
        return 1;
    } else{
        return 0;
    }
}

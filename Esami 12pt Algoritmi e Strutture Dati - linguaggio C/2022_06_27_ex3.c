#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
void dispRip(int pos, char *minuscole, char *maiuscole, int *sol, int n, int k,int countMinuscole, int p );
int check(int p, char *sol, int k);

int main(){
    int n=26;
    char minuscole[26];
    char maiuscole [26];
    for(int i=0; i<n; i++){
        minuscole[i]= (char)('a'+i); //cast
        maiuscole[i]=(char)('A'+i);
    }

    int k=4; //k (parametro del programma) caratteri che rispettino i  vincoli
    char *sol =  malloc(k*sizeof(int));
    int p=2;//lo stesso carattere, senza distinguere tra maiuscolo o minuscolo, non può apparire più di p (parametro del programma) volte consecutivamente.

    int pos=0;
    int countMinuscole=0;

    dispRip(pos,minuscole,maiuscole,sol, n, k, countMinuscole, p );


    free(sol);
    return 0;
}


void dispRip(int pos, char *minuscole, char *maiuscole, int *sol, int n, int k,int countMinuscole, int p ){
    int i;
    if(pos>k){
        if(check(p,sol,k)==1){
            for(int j=0; j<k;j++){
                printf("%c ", sol[j]);
            }
            printf("\n");
        }
        return;
    }
    for(i=0;i<n;i++){
        if(countMinuscole < k/2){
            countMinuscole++;
            sol[pos]=minuscole[i];
            dispRip(pos+1,minuscole, maiuscole,sol,n,k,countMinuscole,p);
            countMinuscole--;
        }
        sol[pos]=maiuscole[i];
        dispRip(pos+1,minuscole, maiuscole,sol,n,k,countMinuscole,p);
    }
    return;
}

int check(int p, char *sol, int k){
    int i;
    int cnt=0; //contatore che conta quante volte un carattere si ripete
    //voglio controllare che un carattere (indipendentemente che sia maiuscolo o minuscolo) si ripeta max p volte consecutive

    //creo un vettore temporaneo dove copio il vettore sol
    //converto tutti gli elemento del vett temporaneo in minuscolo per facilitare il controllo del calcolo di quante volte si ripete lo stesso carattere
    char *tmp =  malloc(k*sizeof(int)); //creo tmp della stessa dimensione di sol
    strcpy(tmp,sol);  //copio in tmp il contenuto di sol
    for(i=0; i<k;i++){
        tmp[i]=(char) tolower(tmp[i]);
    }

    for(i=0; i<k-1; i++){
        if(tmp[i]==tmp[i+1]){
            cnt++;
            if(cnt==p){
                free (tmp);
                return 0;
            }
        }
        else{
            cnt=0;
        }
    }
    free(tmp);
    return 1;
}

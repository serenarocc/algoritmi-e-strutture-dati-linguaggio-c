#include <stdio.h>
#include <stdlib.h>
#define k 3

typedef struct nodo nodo_t;
typedef nodo_t *link;

typedef struct nodo *link;
struct nodo {
    int id; //identificatore nodo
    float wid; //peso nodo
    int nc;//num figli di ogni nodo
    link children[k]; //puntatori a sottoalberi. Ogni nodo può avere l max 3 figli. ogni figlio e' di tipo struct
    float sum; //peso sottoalbero e' la somma dei pesi dei suoi nodi
};


int maxSum(link root, float *maxwt);
float somma(link root);

int main() {
    int id;
    float max=0.00;

    nodo_t n00 = { 0, 15.2, 3};
    nodo_t n01 = { 1, -8.1, 2};
    nodo_t n02 = { 2, 3, 1};
    nodo_t n03 = { 3, 7.27, 1};
    nodo_t n04 = { 4, -20.9, 0};
    nodo_t n05 = { 5, -4, 0};
    nodo_t n06 = { 6, 5.3, 2};
    nodo_t n07 = { 7, -10.8, 3};
    nodo_t n08 = { 8, 2.4, 0};
    nodo_t n09 = { 9, -5, 0};
    nodo_t n10 = {10, -3.1, 0};
    nodo_t n11 = {11, 2, 0};
    nodo_t n12 = {12, 2.8, 0};

    //alloco tutti i figliio
    n00.children[0] = &n01; //nodo 0 ha figli: 1 2 3
    n00.children[1] = &n02;
    n00.children[2] = &n03;
    n01.children[0] = &n04;//nodo 1 ha figli: 4 5
    n01.children[1] = &n05;
    n02.children[0] = &n06;//nodo 2 ha solo figlio 6
    n03.children[0] = &n07;//nodo 3 ha solo figlio 7
    n06.children[0] = &n08;//nodo 6 ha figli: 8 9
    n06.children[1] = &n09;
    n07.children[0] = &n10;//nodo 7 ha figli: 10 11 12
    n07.children[1] = &n11;
    n07.children[2] = &n12;

    somma(&n00);
    printf("\n\n");

    id = maxSum(&n00, &max);

    printf("ID della radice dell'albero di peso massimo (%.2f): %d\n\n", max, id);

    return 0;
}


float somma(link root){
    float s=0;

    for (int i=0; i<root->nc; i++) { //per ogni figlio del nodo
        s=s+somma(root->children[i]);//ricorro per ogni figlio per andare a calcolare la sommma dei nodi
    }
    s =s+ root->wid;
    root->sum = s;
    printf("id: %d --> peso %.2f\n", root->id, s);
    return s;
}

int maxSum(link root, float *maxwt)
{
    int i, id, t;
    float s;

    //printf("id: %d --> peso %.2f\n", root->id, root->sum);
    id = root->id;
    if (root->sum > *maxwt) { //maxwt peso massimo salvato
        *maxwt = root->sum;
       // printf("nuovo id: %d --> nuovo peso %.2f\n", root->id, root->sum);
    }
    for (i=0; i<root->nc; i++) {//per tutti  i figli del nodo
        s = *maxwt;
        t = maxSum(root->children[i], maxwt); //ricorro sul figlio i-esimo per calcolare il suo maxsum
        if (*maxwt > s) {
            id = t; //salvo la radice del sottoalbero a peso max per poi ritornarlo
        }
    }
    return id;//ritorno sottoalbero a peso max
}

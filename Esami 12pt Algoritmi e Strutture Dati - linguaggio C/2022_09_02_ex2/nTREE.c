
#include "nTREE.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nTREE_s{//adt 1 classe
    link root;
};

NTREE BSTinit(){
    NTREE bst=malloc(sizeof *bst);
    return bst;
}

int min(int num1,int num2){
    if(num1<num2) return num1;
    if(num2<=num1) return num2;
}

link new_node(int val, link children[], int lung){
    link x= malloc(sizeof *x);
    x->val=val;
    for(int i=0; i<3 &&  i<lung; i++){// &&x->val!=0 tolgo lung per ciclare
        //il nodo a sentinella a sua volta ha altri children
        x->children[i]=children[i];
    }
    x->lung=min(3,lung);
    return x;

}

void initRoot(NTREE t, link nodoRoot){
    t->root=nodoRoot;
}

void print (link x){
    for(int i=0;i<x->lung;i++){ //ricorro su ogni figlio per partire dalle foglie e poi risalire alla radice
        print(x->children[i]);
    }
    printf(" %d ",x->val);
}

void wrapper_print(NTREE t){
    link x;
    if(t->root!=NULL){
        x=t->root;
        print(x);
    }
}

int countIfRecursive(link x,int k){

    if(x==NULL) return 0;
    int count = 0;
    if(x->lung==k) { //controllo quanti figli ha il nodo radice
        count++;
    }
    for(int i=0; i<x->lung;i++){//controllo quanti figli hanno i figli a loro volta
        if(x->children[i]->lung==k) {
            count++;
        }
        countIfRecursive(x->children[i],k);
    }
    return count;
}

void countIf( NTREE t,int rispostaA,int rispostaB,int rispostaC){
    link x;
    x=t->root;

    rispostaA=countIfRecursive(x,1);
    printf("\n nodi con 1 figlio: %d",rispostaA);

    rispostaB= countIfRecursive(x,2);
    printf("\n nodi con 2 figli: %d",rispostaB);

    rispostaC= countIfRecursive(x,3);
    printf("\n nodi con 3 figli: %d",rispostaC);
}

#include <stdio.h>
#include "nTREE.h"
#include <stdlib.h>
#include <string.h>
/* CHIEDERE A RIPETIZIONI SE è GIUSTO--> RISPOSTA SI
                  7
               /    \
              /      \
             6        5
           / | \      |
          /  |  \     |
         4   3   2    1

  */
int main() {
   NTREE t=NULL;
   t=BSTinit();
   link nodoF= new_node(1,NULL,0);

   link nodoE= new_node(2,NULL,0);
   link nodoD= new_node(3,NULL,0);
   link nodoC= new_node(4,NULL,0);

   link childrenB[1]={nodoF};
   link nodoB= new_node(5,childrenB,1);

   link childrenA[3]={nodoC,nodoD,nodoE};
   link nodoA= new_node(6,childrenA,3);

    link childrenRoot[2]={nodoA,nodoB};
    link nodoRoot= new_node(7,childrenRoot,2);

    initRoot(t,nodoRoot);

    printf("\nstampa albero\n");
    wrapper_print(t);

    int rispostaA=0, rispostaB=0, rispostaC=0;
    countIf( t,rispostaA, rispostaB, rispostaC);
    return 0;
}

#ifndef INC_2022_09_02_EX2_ALBEROGRADO3_NTREECOMEADT1CLASSE_NTREE_H
#define INC_2022_09_02_EX2_ALBEROGRADO3_NTREECOMEADT1CLASSE_NTREE_H
typedef struct nTREE_s *NTREE; //adt 1 classe  ->non vedo dentro dal main

typedef struct node *link;//quasi ADT Vede la struct -> vedo dentro dal main
struct node{
    int val;
    link children[3];
    int lung;
};

NTREE BSTinit();
void initRoot(NTREE t, link nodoRoot);
link new_node(int val, link children[], int lung);
void wrapper_print(NTREE t);
void countIf( NTREE t,int rispostaA,int rispostaB,int rispostaC);

#endif //INC_2022_09_02_EX2_ALBEROGRADO3_NTREECOMEADT1CLASSE_NTREE_H

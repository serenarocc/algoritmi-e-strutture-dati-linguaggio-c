#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define P 2

typedef struct pezzo_s *pez;
struct pezzo_s {
    int val;
    int t;
};

void wrapper_combRip(int T, int V);
void combRip(int k,int pos,int *sol,pez vett, int tempo, int goal, int T, int V,int produzione,int *bestflag,int n,int *bestsol);
int minimiz(int goal,int V, int **bestflag,int *sol,int *bestsol,int n);

int main() {
    int T=16;
    int V=8;
    printf("T=%d  V=%d\n",T,V);
    wrapper_combRip(T,V);
    return 0;
}

void wrapper_combRip(int T, int V){
    int tempo=0, goal=0, produzione=0, bestflag=100, mint=100,n;
    pez vett;
    vett=malloc(P*(sizeof(*vett)));
    if(vett==NULL) return;

    vett[0].val=5; //pezzo di tipologia 0 = A
    vett[0].t=5;
    vett[1].val=2;//pezzo di tipologia 1 = B
    vett[1].t=5;
    vett[2].val=3;//pezzo di tipologia 2 = C
    vett[2].t=3;

    for(int i=0;i<=P;i++){ //trovo il tempo minimo di produzione
        if(vett[i].t<mint){
            mint=vett[i].t;
        }
    }
    n=(T/mint); //dimensione vettore soluzione dato dal tempo totale a disposizione diviso il tempo minimo di produzione. In modo da poter allocare la dimensione del vettore per il caso peggiore

    printf("Elemento di tipo A ha valore: %d e tempo: %d \n",vett[0].val,vett[0].t);
    printf("Elemento di tipo B ha valore: %d e tempo: %d \n",vett[1].val,vett[1].t);
    printf("Elemento di tipo C ha valore: %d e tempo: %d \n",vett[2].val,vett[2].t);

    int *sol;
    sol= malloc(n*sizeof(int));
    for(int i=0;i<n;i++){//inizializzazione sol
        sol[i]=-1;
    }

    int *bestsol;
    bestsol= malloc(n*sizeof(int));
    for(int i=0;i<n;i++){//inizializzazione bestsol
        bestsol[i]=-1;
    }

    int k=n;
    combRip(k,0,sol, vett, tempo, goal, T, V,produzione,&bestflag,n,bestsol);


    printf("\nI pezzi sono di tipologia: "); //STAMPA NON NECESSARIA MA UTILE A CAPIRE IL CONTENUTO DELLA SOLUZIONE BESTSOL
    for (int i = 0; i <= P; i++) {
        for (int j = 0; j < n; j++) {
            if (bestsol[j] == i) {
                printf("%d ", bestsol[j]);
            }
        }
    }
    int contA=0, contB=0, contC=0;
    for(int j=0;j<n;j++){
        if(bestsol[j]==0){
            contA++;
        }
        if(bestsol[j]==1){
            contB++;
        }
        if(bestsol[j]==2){
            contC++;
        }
    }
    printf("\n\nLa soluzione migliore ha:");
    printf("\n%d elementi di tipo A",contA);
    printf("\n%d elementi di tipo B",contB);
    printf("\n%d elementi di tipo C",contC);
}

//uso le combinazioni ripetute perche' non mi interessa l'ordine e gli elementi nella soluzione di possono ripetere
// sono combinazioni ripetute senza PRUNING
void combRip(int k,int pos,int *sol,pez vett, int tempo, int goal, int T, int V,int produzione,int *bestflag,int n,int *bestsol){
    if(pos>=k){
        if(tempo<=T){//entro tempo T
            if(minimiz(goal, V,&bestflag,sol,bestsol,n)==1) { //minimizzare la differenza di obiettivo
            }
        }
        return;
    }
    for(int i=0; i<n; i++){
        sol[pos]=i;
        tempo=tempo+vett[i].t;
        goal=goal+vett[i].val;
        produzione++;
        combRip(k,pos+1,sol,vett,tempo,goal,T, V,produzione,bestflag,n,bestsol); //pruning = togli a prescindere dei rami di ricorsione senza andarli a vedere
        tempo=tempo-vett[i].t;
        goal=goal-vett[i].val;
        produzione--;
    }
    return;
}

int minimiz(int goal,int V, int **bestflag,int *sol,int *bestsol, int n){
    int flag;
    flag=abs(V-goal);
    if(flag<**bestflag){
        **bestflag=flag;
        for(int j=0;j<n;j++){
            bestsol[j]=sol[j];
        }
        return 1;
    }
    return 0;
}

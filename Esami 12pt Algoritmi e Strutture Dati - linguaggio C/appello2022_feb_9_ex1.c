#include <stdio.h>
#include <stdlib.h>
int **malloc2d(int r, int c);
void f(int **M, int r, int c);

int main() {
    int r=3,c=4;
    int **M = malloc2d(r, c);

    M[0][0]=1; M[0][1]=2; M[0][2]=3,M[0][3]=4;
    M[1][0]=5; M[1][1]=6; M[1][2]=7,M[1][3]=8;
    M[2][0]=9; M[2][1]=0; M[2][2]=1,M[2][3]=2;
    f(M, r,c);
    return 0;
}
void f(int **M, int r, int c){
    int savei[r],savej[c];
    int contatorei=0,contatorej=0;
//inizzializzo vettori
    for(int z=0;z<r;z++){
        savei[z]=1;
    }
    for(int z=0;z<c;z++){
        savej[z]=1;
    }



    //salvo le posizioni in cui nella matrice c'è elemento 0
    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            if((i%2!=0)&&(j%2!=0)){ //salvo come 0 gli elem a indici dispari
                savei[i]=0;
                savej[j]=0;

            }
        }
    }

    //conto quanti indici pari ci sono. Saranno le dimensioni della nuova matrice
    for(int z=0;z<r;z++){
        if( savei[z]==1)
        contatorei++;
    }
    for(int z=0;z<c;z++){
        if( savei[z]==1)
            contatorej++;
    }

    //dimensione nuova matrice
    int x=contatorei;
    int y=contatorej;
    int **a= malloc2d(x,y);


    //riempimento nuova matrice
    for(int i=0, k=0;i<r;i++) {
        if(savei[i]==1 ){
            for(int j=0, w=0; j<c;j++){
                if( savej[j]==1){
                    a[k][w++]=M[i][j];
                }
            }
            k++;
        }
    }

    printf("\nstampa M:");
    for(int i=0;i<r;i++) {
        printf("\n");
        for (int j = 0; j < c; j++) {
            printf("%d ",M[i][j]);
        }
    }

    printf("\nstampa a:");
    for(int i=0;i<x;i++) {
        printf("\n");
        for (int j = 0; j<y; j++) {
            printf("%d ",a[i][j]);
        }
    }

}


int **malloc2d(int r, int c){
    int **M = malloc(r*sizeof (int *));
    for(int i=0; i<c; i++)
        M[i]= malloc(c*sizeof (int));
    return M;
}

#include <stdio.h>
#include "LIST.h"
int main() { //GIUSTO
    /*
     * a-a-b-b-b-c-d-d
     * k=3
     * a-a-b-c-d-d
     * */
    LIST l=NULL;

    l=wrapper_listInHead( l, 'a');
    wrapper_listInTail( l,'a');
    wrapper_listInTail( l,'a');
    wrapper_listInTail( l,'a');
    wrapper_listInTail( l,'b');
    wrapper_listInTail( l,'b');
    wrapper_listInTail( l,'b');
    wrapper_listInTail( l,'c');
    wrapper_listInTail( l,'d');
    wrapper_listInTail( l,'d');
    wrapper_listInTail( l,'d');
    wrapper_listInTail( l,'d');
    wrapper_listInTail( l,'d');

    printf("\nLista prima dell'eliminazione\n");
    printList( l);

    //commentare le parti non utilizzate per non manipolare sempre la stessa lista gia' manipolata
    f2_senzaK(l);
    printf("\nLista compattata cancellando tutti i nodi uguali consecutivi ad eccezione del primo di ogni gruppo\n");
    printList( l);

    //METODO 1: FATTO DA ME PIU DIFFICILE, CORRETTO A RIPETIZIONI
    int k=3;
    f_metodo1(l,k);
    printf("\nLista compattata cancellando tutti i nodi uguali consecutivi ad eccezione del primo di ogni gruppo,solo se la sotto-lista di nodi uguali consecutivi e' composta da almeno K elementi.\n");
    printList( l);

    //METODO 2: PIU FACILE, FATTO A RIPETIZIONI
    k=3;
    f_metodo2(l,k);
    printf("\nLista compattata cancellando tutti i nodi uguali consecutivi ad eccezione del primo di ogni gruppo,solo se la sotto-lista di nodi uguali consecutivi e' composta da almeno K elementi.\n");
    printList( l);

    f_sottosequenzaSoloPari( l);
    printf("\nLista compattata cancellando tutti i nodi uguali della sottosequenza, con indice della sottosequenza dispari\n");
    printList( l);

    return 0;
}

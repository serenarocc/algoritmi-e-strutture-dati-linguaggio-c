
#include "LIST.h"
#include <stdlib.h>
#include <stdio.h>

struct lista{
    link head;
    link tail;//doppio linkata-> mi serve tornare indietro
    //nella lista singolo linkata non mi serve tail perche' non posso tornare indietro
};

link new_node(char val, link next, link prec){
    link x;
    x= malloc(sizeof *x);
    if(x==NULL) return NULL;
    else{
        x->val=val;
        x->next=next;
        x->prec=prec;
        if(prec!=NULL){
            prec->next=x;//doppio link
        }
        if(next!=NULL){
            next->prec=x; //metto un nuovo nodo in una lista già esistente così ho il collegamento doppio link
            // a b c
        }
    }
    return x;
}

link listInHead(link head, char val){ //ritorna un nodo
    head=new_node(val,head,NULL);
    return head;
}
LIST wrapper_listInHead(LIST l, char val){//ritorna lista
    if(l==NULL){//chiamata 1 sola volta
        l=malloc(sizeof *l);
        l->head=listInHead(NULL, val);
        l->tail=l->head;
    }
    return l;
}
link listInTail(link OldTail, char val){//ritorna un nodo
    link NewTail= new_node(val, NULL,OldTail);
    OldTail->next=NewTail;
  //  for(x=tail;x->next!=NULL; x=x->next);// arriva a fine lista
  //  OldTail->next= new_node(val, NULL,OldTail);
    return NewTail;
}
LIST wrapper_listInTail(LIST l, char val){//ritorna lista
    if(l->head==NULL) return wrapper_listInHead( l,  val);
    l->tail= listInTail(l->tail,val);
    return l;
}


void printList(LIST l){
    link curr=l->head;
    while(curr!=NULL){
        printf(" %c ", curr->val);
        curr=curr->next;
    }
}





//METODO 1: FATTO DA ME PIU DIFFICILE, CORRETTO A RIPETIZIONI
void f_metodo1(LIST l, int k){ //SBAGLIATO
    link nodoCurr;
    link nodoNext;
    link nodoPrec;
    link tmp;
    link tmp2;
    int count=1;

    if(l!=NULL){
        nodoCurr=l->head;
        nodoPrec=nodoCurr->prec;
        while(nodoCurr!=NULL){//mi fermo a ultimo nodo  x riga 82
          //  nodoNext=nodoCurr->next;
            if(nodoCurr->next!=NULL && nodoCurr->val==nodoCurr->next->val ) {
                if(count==1){
                    tmp=nodoCurr;//prima b
                }
                count++;
            }
            else{
                if(count>=k){
                    link freeNode=tmp->next;
                    for(int i=0;i<count-1;i++) {//elimino
                        freeNode->prec->next = freeNode->next;
                        if(freeNode->next!=NULL)
                        freeNode->next->prec = freeNode->prec;
                        tmp2 = freeNode->next;
                        freeNode = tmp2;
                    }
                }
                count=1;
            }
            nodoCurr=nodoCurr->next;
        }
       int a=5;
    }
}



//METODO 2: PIU FACILE, FATTO A RIPETIZIONI
/*Si scriva una funzione void f(LIST l, int k) che ricevuta in input una lista
rappresentata facendo riferimento ai tipi definiti in precedenza, già ordinata in ordine
alfabetico crescente, compatti i contenuti della lista cancellando tutti i nodi uguali
consecutivi, ad eccezione del primo di ogni gruppo, solo se la sotto-lista di nodi uguali
consecutivi è composta da almeno elementi.*/
void eliminaNodo(link nodoCurr){ // a1 a3 b1, elimino a2. inizio a cancellare da sx verso dx
    if(nodoCurr->prec!=NULL){ //caso primo nodo della lista che ha il prec che punta a NULL. Se non faccio questo controllo vado in errore
        nodoCurr->prec->next=nodoCurr->next;
    }
    if(nodoCurr->next!=NULL){ //caso ultimo nodo della lista, cosi' non devo mettere questo controllo nel while
        nodoCurr->next->prec=nodoCurr->prec;
    }
    free(nodoCurr); //elimino nodo corrente
}

void f_metodo2(LIST l, int k) { //1 for
    link nodoCurr;
    link sequenceHead=NULL;//primo nodo della sottosequenza di nodi uguali
    int num_ricorr=1;

    if(l!=NULL){//se lista esiste
        nodoCurr = l->head;
        while(nodoCurr!=NULL ){ //considero tutti i nodi. Se facessi nodoCurr->next!=NULL non starei considerando d4
           if(sequenceHead==NULL){ //testa lista a1 //caso in cui sono all'inizio e non ho ancora def il valore della testa della sotto sequenza
               sequenceHead=nodoCurr;
              // continue; nel caso del for posso togliere else if
           }//faccio else if perche' sono gia' in una sotto sequenza
           else if(nodoCurr->val==sequenceHead->val){ //a2==a1, a3==a1  //else if serve per il primo caso, ovvero non etrare con a1
               num_ricorr++; //ho trovato una ricorrenza in piu' della lettera
           }
           if(nodoCurr->val!=sequenceHead->val || nodoCurr->next==NULL){ //b1!=sequencehead(a1)
               //1) elimino vecchia sequenza se numricorr >k
               if(num_ricorr>=k){
                   //nodoCurr e' b1
                   link freeNode=sequenceHead->next; //a2
                   for(int i=0;i<num_ricorr-1;i++){ //elinino num_ricorr-1 volte
                       link nextNode=freeNode->next; //a3
                       eliminaNodo( freeNode); //a2
                       freeNode=nextNode; //a3
                   }
               }
               //2) setto variabili in modo da eliminare nuova sequ di b
               sequenceHead=nodoCurr;   //b1
               num_ricorr=1;
           }
           nodoCurr=nodoCurr->next; //vado avanti nella lista a2, a3, b1
        }
    }
}

//COMPATTO LA LISTA
/*Si scriva una funzione void f(LIST l, int k) che ricevuta in input una lista
rappresentata facendo riferimento ai tipi definiti in precedenza, già ordinata in ordine
alfabetico crescente, compatti i contenuti della lista cancellando tutti i nodi uguali
consecutivi, ad eccezione del primo di ogni gruppo*/
void f2_senzaK(LIST l) { //1 for
    link nodoCurr;
    link sequenceHead=NULL;//primo nodo della sottosequenza di nodi uguali
    link nextNode;

    if(l!=NULL) { //se lista esiste
        nodoCurr = l->head; //parto dal primo nodo in testa
        while(nodoCurr!=NULL ){ //considero tutti i nodi. Se facessi nodoCurr->next!=NULL non starei considerando d4
            nextNode=nodoCurr->next; //def nextNode
            if(sequenceHead==NULL || nodoCurr->val!=sequenceHead->val ){ //testa lista a1 //caso in cui sono all'inizio e non ho ancora def il valore della testa della sotto sequenza OPPURE sto passando alla nuova sottosequenza e definendo la nuova testa della prossima sottosequenza
                sequenceHead=nodoCurr;
            } //faccio else if perche' sono gia' in una sotto sequenza
            else if(nodoCurr->val==sequenceHead->val ){ //a2==a1, a3==a1  //else if serve per il primo caso, ovvero non etrare con a1
                eliminaNodo(nodoCurr); //non posso piu accedere a nodoCurr per 189 ho bisogno di salvarmelo preventivamente
            }
            nodoCurr=nextNode; //vado avanti nella lista a2, a3, b1
        }
    }
}

void f_sottosequenzaSoloPari(LIST l){
    link nodoCurr;
    link sequenceHead=NULL;//primo nodo della sottosequenza di nodi uguali
    int indice=0;
    link nextNode;

    if(l!=NULL) {//se lista esiste
        nodoCurr = l->head;//parto dal primo nodo in testa
        while(nodoCurr!=NULL ){ //considero tutti i nodi. Se facessi nodoCurr->next!=NULL non starei considerando d4
            nextNode=nodoCurr->next;//def nextNode
            if(sequenceHead==NULL){ //testa lista a1  //caso in cui sono all'inizio e non ho ancora def il valore della testa della sotto sequenza
                sequenceHead=nodoCurr;
            }
            else if(nodoCurr->val==sequenceHead->val){ //a2==a1, a3==a1  //else if serve per il primo caso, ovvero non etrare con a1
                indice++; //indice
                if(indice%2!=0){ //elimino tutti i nodi della sottosequenza con indice dispari
                    eliminaNodo(nodoCurr);
                }
            }
            if(nodoCurr->val!=sequenceHead->val){ //b1!=sequencehead(a1)
                //2) setto variabili in modo da eliminare nuova sequ di b
                sequenceHead=nodoCurr;   //b1
                indice=0;
            }
            nodoCurr=nextNode; //vado avanti nella lista a2, a3, b1
        }
    }
}

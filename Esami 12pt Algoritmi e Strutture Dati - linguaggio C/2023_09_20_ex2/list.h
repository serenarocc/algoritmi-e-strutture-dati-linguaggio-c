#ifndef INC_2023_09_20_EX2_LISTADOPPIOLINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H
#define INC_2023_09_20_EX2_LISTADOPPIOLINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H

typedef struct lista *LIST;//adt 1 classe

typedef struct node *link;//quasi ADT
struct node{
    char val;
    link next;
    link prec;
};


LIST wrapper_listInHead(LIST l, char val);
LIST wrapper_listInTail(LIST l, char val);
void printList(LIST l);


void f_metodo1(LIST l, int k);
void f_metodo2(LIST l, int k);
void f2_senzaK(LIST l);
void f_sottosequenzaSoloPari(LIST l);
#endif //INC_2023_09_20_EX2_LISTADOPPIOLINKATA_LISTADT1CLASSE_NODOQUASIADT_LIST_H

#include <stdio.h>
#include <stdlib.h>
void wrapper(int **M,int O,int S);
void powerset_comsempl( int **M,int S,int O,int *sol,int *bestsol,int j,int pos,int start,int *stop, int *taken);
int check(int *sol,int *bestsol,int j,int **M,int S, int O, int *taken);
void **malloc2d(int r, int c);

int main() {
    int O=6, S=5;
    int **m=malloc2d(O,S);
    m[0][0]=1; m[0][1]=0; m[0][2]=0; m[0][3]=1; m[0][4]=0; m[0][5]=0;
    m[1][0]=0; m[1][1]=1; m[1][2]=0; m[1][3]=0; m[1][4]=1; m[1][5]=0;
    m[2][0]=0; m[2][1]=1; m[2][2]=1; m[2][3]=0; m[2][4]=0; m[2][5]=0;
    m[3][0]=0; m[3][1]=1; m[3][2]=1; m[3][3]=0; m[3][4]=1; m[3][5]=1;
    m[4][0]=0; m[4][1]=0; m[4][2]=1; m[4][3]=0; m[4][4]=0; m[4][5]=1;

    wrapper(m,O,S);
    return 0;
}
/*
 * obiettivo: trovare insieme a cardinalita' minima che contiene i sottoinsiemi  digiunti in grado di coprire l'insieme universo
 *
 * NON trovare il num di insiemi -> alg er
 *
 * IO qui ho solo un insieme di cui devo andare a vedere dentro
 */
void wrapper(int **M,int O,int S){
    int *sol =  malloc(S*sizeof(int)); //vettore sol contiene al max tutti i sottoinsiemi S
    int *bestsol =  malloc(S*sizeof(int));
    int *taken =  malloc(O*sizeof(int));
    for(int z=0;z<S;z++){  //taken indica se l'oggetto di colonna c e' stato preso
        taken[z]=-1; //inizzializzo taken
    }
    int pos=0;
    int start=0;
    int j;
    int stop=0;

    for(j=1; j<=S && stop==0; j++){ //piano piano aumento il numero di sottoinsiemi che prendo in considerazione (righe)
        powerset_comsempl(M,S,O,sol,bestsol,j,pos,start,&stop, taken);
    }
    // Deallocazione dei vettori dinamici
    free(sol);
    free(bestsol);
    free(taken);
}

//non uso alg er perche io non voglio sapere tutti gli insiemi che compongono la soluzione
// io voglio sapere della soluzione solo l'insieme più grande
void powerset_comsempl( int **M,int S,int O,int *sol,int *bestsol,int j,int pos,int start,int *stop, int *taken){
    int i;

    if((*stop)==1) return; //stampa una sola soluzione

    if(pos>=j){
        if(check(sol,bestsol,j,M,S,O, taken)==1){ //gli passo sol di pos con la possibile soluzione = il sottoinsieme che voglio stampare
              (*stop)=1;
               printf("{");
               for(i=0;i<j;i++) printf("%d ",sol[i]);
               printf("}");
               printf("\n");
        }
        return;
    }
    for(i=start;i<S;i++){ //per tutte gli S sottoinsiemi. i indica il i-esimo sottoinsieme
        sol[pos]=i; //salvo il sottoinsieme preso nel gruppo  che stampo
        powerset_comsempl(M,S,O,sol,bestsol,j,pos+1,i+1,stop, taken);
    }
}


int check(int *sol,int *bestsol,int j,int **M,int S, int O, int *taken){
    int i,k;

    for(i=0;i<j;i++){ //primo dei j-elementi conetenuti nel vettore sol
        for(k=i+1;k<j;k++){ //mi sto muovendo sul prossimo valore contenuto nel vettore sol
            for(int c=0;c<O;c++){ //guardo le colonne della matrice che indicano gli c oggetti
                if(M[sol[i]][c]==1){ //M[sottoinsieme i di sol][oggetto c]
                    taken[c]=1;
                }
                if(M[sol[k]][c]==1){ //M[sottoinsieme k di sol][oggetto c]
                    taken[c]=1;
                }
            }
        }
    }
    int tuttiOggettiPresi=1; //flag per controllare che ho preso tutti gli oggetti
    for(int i=0; i<O;i++){ //controllo per tutti gli oggetti
        if(taken[i]==-1){
            tuttiOggettiPresi=0; //non tutti gli oggetti dell'universo sono stati presi
        }
    }
    if(tuttiOggettiPresi==1){ //tutti gli oggetti dell'universo sono stati presi
        return 1; //vado in stampa
    } else{ //non sono al punto della soluzione buona
        for(int c=0; c<O;c++){ //prima di uscire, rinizzializzo taken prima di uscire
            taken[c]=-1;
        }
        return 0;
    }
}


void **malloc2d(int r, int c){ //queto e' il malloc correto
    int **m=malloc(r*sizeof (int *));
    for(int i=0;i<r;i++){
        m[i]= malloc(c*sizeof (int));
    }
    return m;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct {
    char *s;
    int pos;
    int costo;
}part;

int check(part *S,int *sol,int j, char *target);
void powersetCombSemplici(part *S, int nParts, char *target, int *sol, int *bestsol, int j, int pos, int start, int *blocchi, int *bestCosto);
void solve(char *target, part *S, int nParts);


//si e' cambiato il nome del vettore struct da P a S
int main() {
    char target[]="persona";
    int nParts=10;
    part *S=malloc(10*sizeof (part));
    S[0].s="p";   S[0].pos=0; S[0].costo=1;
    S[1].s="pers";S[1].pos=0; S[1].costo=5;
    S[2].s="er";  S[2].pos=1; S[2].costo=4;
    S[3].s="ers"; S[3].pos=1; S[3].costo=4;
    S[4].s="sa";  S[4].pos=3; S[4].costo=1;
    S[5].s="so";  S[5].pos=3; S[5].costo=2;
    S[6].s="ato"; S[6].pos=0; S[6].costo=1;
    S[7].s="on";  S[7].pos=4; S[7].costo=2;
    S[8].s="ona"; S[8].pos=4; S[8].costo=3;
    S[9].s="a";   S[9].pos=1; S[9].costo=1;
    solve(target, S,nParts);
    return 0;
}

void solve(char *target, part *S, int nParts){
    int *sol=malloc(nParts*sizeof(int));
    int *bestsol=malloc(nParts*sizeof(int));
    int blocchi=0; //contatore delle sottostringhe che uso per trovare target
    int bestCosto=INT_MAX;//devo minimizzare il costo
    int pos=0;
    int start=0;//variabile start del for del powerset

    for(int j=0;j<nParts;j++){ //creo partizioni andando a vedere un po' alla volta tutto il dizionario
        powersetCombSemplici(S, nParts, target, sol, bestsol, j, pos, start, &blocchi, &bestCosto);
    }

    if(blocchi == 0){ //non ho usato nessuna sottostringa del dizionario per trovare target-> target non trovato
        printf("Non e' stato possibile trovare una soluzione");
    }else{//ho trovato target con num di sottostringhe pari a blocchi
        printf("Costo migliore: %d\n",bestCosto); //minor costo tot di trasformazioni
        for(int i=0; i < blocchi; i++){ //stampo tutte le sottostringhe che ho usato
            printf("%s ",S[bestsol[i]].s); //stampo sostituzioni
        }
    }
    free(sol);
    free(bestsol);
}

void powersetCombSemplici(part *S, int nParts, char *target, int *sol, int *bestsol, int j, int pos, int start, int *blocchi, int *bestCosto){
    int i;
    int costoparziale=0;
    if(pos>=j){ // se ho ricorso su tutti gli elementi presi in considerazione del dizionario
        if(check(S,sol, j, target)==1){
            for(i=0;i<j;i++) {// trovo la somma dei costi che ho per tutte le j sottostringhe usate
                costoparziale = costoparziale + S[sol[i]].costo;
            }
            if(costoparziale==(*bestCosto) && j>(*blocchi)){// a parita' di costo sceglierela soluzione che fa uso di piu' sottostringhe del dizionario
                (*blocchi)=j; //nuovo valore di blocchi usati
                for(i=0;i<j;i++) bestsol[i]=sol[i];// salvo nuova soluzione come bestsol
            }else{
                if(costoparziale<(*bestCosto)){ //scelgo il costo minore
                    (*blocchi)=j;//nuovo valore di blocchi usati
                    (*bestCosto) = costoparziale;//nuovo valore di costo tot
                    for(i=0;i<j;i++) {
                        bestsol[i]=sol[i];// salvo nuova soluzione come bestsol
                    }
                }
            }
        }
        return;
    }

    for(i=start; i<nParts;i++){ //non c'e' pruning
        sol[pos]=i; //prendo la posizione i del dizionario
        powersetCombSemplici(S, nParts, target, sol, bestsol, j,  pos+1,  i+1, blocchi, bestCosto);
    }
}

int check(part *S,int *sol,int j, char *target) {
    int targetIndex = 0; // Indice per il target
    int totalLength = 0; // Lunghezza totale delle sottostringhe utilizzate (somma di tutti i caratteri che metto insieme uno alla volta)

    // Scorro tutte le sottostringhe selezionate della partizione
    for (int partIndex = 0; partIndex < j; partIndex++) {
        int partLength = strlen(S[sol[partIndex]].s); // Lunghezza della sottostringa corrente

        // Scorro la sottostringa corrente
        for (int sottoIndex = 0; sottoIndex < partLength; sottoIndex++) {
            totalLength++; // Incremento la lunghezza totale delle sottostringhe utilizzate

            // Controllo se il carattere corrente corrisponde al target
            if (target[targetIndex] != S[sol[partIndex]].s[sottoIndex]) {
                return 0; // Ritorno 0 se i caratteri non corrispondono
            }

            targetIndex++; // Passo al carattere successivo del target

            // Controllo se ho completato tutto il target
            if (target[targetIndex] == '\0') {
                break; // Esco dal ciclo se ho completato il target
            }
        }
    }

    // Controllo se la lunghezza totale delle sottostringhe utilizzate corrisponde alla lunghezza del target
    if (totalLength != strlen(target)) {
        return 0; // Ritorno 0 se le lunghezze non corrispondono
    }
    return 1; // Ritorno 1, indicando che il target è stato trovato

}

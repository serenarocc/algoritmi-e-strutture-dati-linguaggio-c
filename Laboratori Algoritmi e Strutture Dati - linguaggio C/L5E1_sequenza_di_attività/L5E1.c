#include <stdio.h>
#include <stdlib.h> //malloc
typedef struct att_{
    int s;// tempo inizio
    int f;//tempo fine
    int d;//differenza tempi
}att;


att *leggiFile(FILE *fp, int *n);
void attSel(int n, att *v);
void powerset(int pos, int n,att *v, int *sol, int *bestsol, int currDurata, int *bestDurata);
int condizione(int n, att *v, int *sol, int pos) ;


int main(int argc, char **argv){
    int n;
    FILE *fp;
    att *v;

    fp=fopen("../att.txt","r");//apertura file lettura
    if(fp==NULL){//controllo corretta apertura file
        printf("errore apertura file");
        exit (-1);
    }
    v=leggiFile(fp,&n);//leggo dati da file e li salvo

    attSel(n,v);//funzione richiesta dal testo

    fclose(fp);//chiusura file
    return 0;
}

att *leggiFile(FILE *fp, int *n){
    att *v;

    fscanf(fp,"%d\n",n);//leggo prima riga file che indica quante righe ci sono nel file

    v=calloc(*n,sizeof(att)); //alloco vettore calloc della struttura
    printf("Dati:\n");//stampo tutti i dati presi dal file
    for(int i=0;i<*n;i++){
        fscanf(fp,"%d %d\n",&v[i].s,&v[i].f);
        v[i].d=v[i].f-v[i].s;//diffrenza tempo d'inizio e di fine
        printf("(%d,%d) d=%d\n",v[i].s,v[i].f, v[i].d);
    }
  return v;//ritorno v perche' la funzione e' di tipo struct
}

void attSel(int n, att *v) {

    int *sol = calloc(n, sizeof(int));//vettore di n elementi di tipo intero
    int *bestsol = calloc(n, sizeof(int));//vettore di n elementi di tipo intero
    int duration=0,i;

    att *maxv;

    powerset(0,n,v,sol,bestsol,0,&duration);//insieme delle parti disposizioni ripetute

    printf("Soluzione con durata massima:\n");
    for (int i = 0; i < n; i++) {
            if(bestsol[i]==1) {
                printf("(%d,%d)", v[i].s, v[i].f);//stampo soluzione
            }

    }
    free(sol);//libero le calloc
    free(bestsol);
    return;
}

//insieme delle parti disposizioni ripetute. Blocco 3 sl 273
void powerset(int pos, int n,att *v, int *sol, int *bestsol, int currDurata, int *bestDurata){
    int i;
    if (pos >= n) {//condizione di terminazione
            if (*bestDurata < currDurata) {
                *bestDurata = currDurata;
                for (i = 0; i < n; i++)
                    bestsol[i] = sol[i];
            }
        return ;
    }

    sol[pos] = 0;//non prendere l'elemento pos
    powerset(pos+1, n, v, sol, bestsol, currDurata, bestDurata);//ricorri su pos+1
    if (condizione(n, v, sol, pos)==0) {//condizione per avere due attivita' compatibili
           sol[pos] = 1;//backtrack:prendo l'elemento pos
           powerset(pos+1, n, v, sol, bestsol,  currDurata+(v[pos].d), bestDurata);//ricorro pos+1
    }

    return;
}
int condizione(int n, att *v, int *sol, int pos) {
    int i;

    for (i=0; i<pos; i++) {
        if (sol[i] ==1) {
            if (v[i].s < v[pos].f && v[pos].s < v[i].f)//attivita' incompatibili
                return 1;//attivita' incompatibili
        }
    }
    return 0;//attivita' compatibili
}


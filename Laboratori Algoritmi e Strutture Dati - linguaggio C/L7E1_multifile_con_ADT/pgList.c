//
// Created by seren on 19/12/2022.
//
#include <stdlib.h>//per calloc
#include <string.h>
#include <stdio.h>

#include "pgList.h"

//ADT prima classe
typedef struct nodoPg_s{
    pg_t pg;
    struct nodoPg_s *next;
}nodoPg_t, *linkPg;

struct pgList_s{
    linkPg head,tail;
    int nPg;
};

static linkPg newNodoPg(pg_t pg, linkPg next);

//nuovo nodo lista
static linkPg newNodoPg(pg_t pg, linkPg next){
    linkPg n=calloc(1,sizeof(*n));
    if(n==NULL)//Controllo corretto calloc
        return NULL;
    n->pg=pg;
    n->next=next;
    return n;
}

//allocazione pgList
pgList_t pgList_init(){
    pgList_t pgList=calloc(1,sizeof (*pgList));
    return pgList;
}

//inserimento nodo in lista
void pgList_insert(pgList_t pgList, pg_t pg){
    linkPg toAdd;
    toAdd= newNodoPg(pg,NULL);
    if(toAdd==NULL|| pgList==NULL)
        return;
    if(pgList->head==NULL)
        pgList->head=pgList->tail=toAdd;
    else{
        pgList->tail->next=toAdd;
        pgList->tail=toAdd;
    }
}

//apertura per lettura file pg.txt e salvataggio di ciò che ho letto
void pgList_read(char *filename,pgList_t pgList){
    pg_t pg;
    FILE *fp;
    fp=fopen(filename,"r");
    if(pgList==NULL)//controllo corretta allocazione calloc
        return;
    if(fp==NULL)
        printf("errore apertur file\n");
        return;

    while((pg_read(fp,&pg))!=0){ //funz in pg.c
        pgList_insert(pgList,pg);//inserisco ciò che ho letto in lista
        pgList->nPg++;//incremento num di personaggi
    }
    fclose(fp);
}

//stampa lista personaggi
void pgList_print(FILE *fp, pgList_t pgList, invArray_t invArray){
    linkPg iter=NULL;
    if(pgList==NULL)
        return;
    for(iter=pgList->head; iter!=NULL;iter=iter->next)
        pg_print(fp,&iter->pg,invArray);//funz in pg.c
}

//cerco personaggio per codice nella lista
pg_t *pgList_searchByCode(pgList_t pgList, char* cod){
    linkPg iter=NULL;
    if(pgList==NULL)
        return NULL;
    for(iter=pgList->head; iter!=NULL;iter=iter->next){
        if(strcmp(iter->pg.cod,cod)==0){//se trovo corrispondenza tra codici
            return (&iter->pg);//ritorno personaggio
        }
    }
    return NULL;
}

//aggiungo personagio alla lista da tastiera
void pgList_add(pgList_t pgList){
    pg_t pg;

    printf("Codice Nome Classe HP MP ATK DEF MAG SPR: ");
    if (pg_read(stdin, &pg) != 0) {//lettura da tastiera. Funz si trova in pg.c
        pgList_insert(pgList, pg);//inserisco pg nella lista.
    }
}

//elimino personaggio in base al codice
void pgList_remove(pgList_t pgList_t, char* cod){
    linkPg iter, prev;
    for(iter = pgList_t->head, prev = NULL; iter != NULL; prev = iter, iter=iter->next) {//scansione tutta lista
        if (strcmp(iter->pg.cod, cod) == 0) {//se ho trovato corrispondenza tra le stringhe
            if (iter == pgList_t->head && iter == pgList_t->tail) {
                pgList_t->head = pgList_t->tail = NULL;
            } else if (iter == pgList_t->head) {
                pgList_t->head = iter->next;
            } else if (iter == pgList_t->tail) {
                pgList_t->tail = prev;
                prev->next = NULL;
            } else {
                prev->next = iter->next;
            }
            pg_clean(&iter->pg);
            free(iter);
            break;
        }
    }
}

//distruttore lista
void pgList_free(pgList_t pgList) {
    linkPg iter, next;
    for(iter = pgList->head; iter != NULL; iter=next) {
        next = iter->next;
        pg_clean(&iter->pg);
        free(iter);
    }
}

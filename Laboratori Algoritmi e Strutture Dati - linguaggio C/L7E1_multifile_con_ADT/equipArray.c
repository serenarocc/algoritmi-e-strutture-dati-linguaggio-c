//
// Created by seren on 19/12/2022.
//

#include <stdlib.h>
#include <stdio.h>

#include "equipArray.h"


struct equipArray_s{
    int vettEquip[EQUIP_SLOT];
    int inUso;
};

//creazione
equipArray_t equipArray_init(){
    equipArray_t equipArray=calloc(1,sizeof(*equipArray));
    if(equipArray!=NULL){
        for(int i=0; i<EQUIP_SLOT; i++){
            equipArray->vettEquip[i]=-1;//inizializzo vettore a -1
        }
    }
    return equipArray;
}

//liberazione vettore equipaggio
void equipArray_free(equipArray_t equipArray){
    if(equipArray!=NULL){
        free(equipArray);
    }
}

//stampa vettore equipArray su file
void equipArray_print(FILE *fp,equipArray_t equipArray, invArray_t invArray){
    if(equipArray==NULL|| invArray==NULL)//controllo corretta allocazione calloc
        return;
    if(equipArray->inUso>0){
        for(int i=0; i<EQUIP_SLOT;i++){//scandisco vettEquip
            if(equipArray->vettEquip[i]!=-1){
                invArray_printByIndex(fp, invArray,equipArray->vettEquip[i]);//funz inArray.c
            }
        }
    }
}
//quanti equipaggiamenti sono in uso
int equipArray_inUse(equipArray_t equipArray){
    if(equipArray==NULL)//controllo se calloc e' andata a buon fine
        return 0;
    return equipArray->inUso;
}

// modifica equipaggiamento scegliendo un oggetto da inventario
void equiArray_updata(equipArray_t equipArray,invArray_t invArray){
    int comando=-1,eq;
    char nomeEq[100];
    printf("Operazione che vuoi svolgere:\n");
    if(equipArray->inUso>0){
        printf("0 - rimozione");
    }
    if(equipArray->inUso<EQUIP_SLOT){
        printf("1 - aggiungere");
    }
    scanf("%d",&comando);
    if(comando==0 && equipArray->inUso<=0)
        return;
    if(comando==1 && equipArray->inUso >=EQUIP_SLOT)
        return;
    if(comando!=0 && comando!=1)
        return;

    printf("Nome equipagiamento:\n");
    scanf("%s",nomeEq);

    eq= invArray_searchByName(invArray,nomeEq);//funz in invArray.c
    if(eq==-1)
        return;
    switch (comando) {
        case 0:{//rimozione
            for(int i=0;i<EQUIP_SLOT;i++){
                if (equipArray->vettEquip[i] == -1) //se e' vuoto, non procedo e  continuo a iterare sul for
                    continue;
                if (equipArray->vettEquip[i] == eq) {
                    equipArray->inUso--;//decremento equipaggiamento in uso
                    equipArray->vettEquip[i] = -1;//segno come non usato
                    break;
                 }
             }
         } break;
         case 1:{//aggiungere
              for(int i=0;i<EQUIP_SLOT;i++){
                if(equipArray->vettEquip[i]==-1){//se l'equipaggiamento non e' usato
                     equipArray->vettEquip[i]=eq; //salvo equipaggiamento
                     equipArray->inUso++;//incremento equipaggiamenti in uso
                     break;
                }
              }
         }
        default:
            break;
    }
}

int equipArray_getEquipByIndex(equipArray_t equipArray, int indice) {
    if(equipArray==NULL)
        return -1;
    if(equipArray->vettEquip==NULL)
        return -1;
    if(indice<0 || indice >=EQUIP_SLOT)
        return -1;
    return equipArray->vettEquip[indice];
}

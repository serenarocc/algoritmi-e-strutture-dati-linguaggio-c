//
// Created by seren on 19/12/2022.
//

#ifndef STRUTTUREL7E1_GIOCO_DI_RUOLO_ADT_INV_H
#define STRUTTUREL7E1_GIOCO_DI_RUOLO_ADT_INV_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


//#define MIN_STAT 1


/* quasi ADT statistiche */
typedef struct stat_s {
    int hp, mp, atk, def, mag, spr;
} stat_t;

/* quasi ADT oggetto di inventario */
typedef struct inv_s {
    char nome[100];
    char tipo[100];
    stat_t stat;
} inv_t;

/* funzioni di input/output delle statistiche */
void stat_read(FILE *fp, stat_t *statp);
void stat_print(FILE *fp, stat_t *statp, int soglia);

/* funzioni di input/output di un oggetto dell'inventario */
void inv_read(FILE *fp, inv_t *invp);
void inv_print(FILE *fp, inv_t *invp);

/* ritorna il campo stat di un oggetto dell'inventario */
stat_t inv_getStat(inv_t *invp);

/* Si possono aggiungere altre funzioni se ritenute necessarie */
stat_t stat_add(stat_t sa, stat_t sb);

#endif //STRUTTUREL7E1_GIOCO_DI_RUOLO_ADT_INV_H

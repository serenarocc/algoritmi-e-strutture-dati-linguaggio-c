#include <stdio.h>

#include "pgList.h"
#include "invArray.h"
#include "pg.h"

void stampaMenu(char *scelte[], int *selezione);

int main() {

    char *scelte[] = {
            "Uscita programma",
            "Stampa personaggi",
            "Stampa inventario",
            "Cerca personaggio",
            "Aggiungi personaggio",
            "Elimina personaggio",
            "Modifica equipaggiamento"
    };

    char codiceRicerca[100];
    int selezione,fineProgramma;
   FILE *fin;

    pgList_t pgList = pgList_init();//allocazione pgList
    invArray_t invArray = invArray_init();//allocazione invArray

    pg_t *pgp;
    pgList_read("../pg.txt", pgList);//lettura da file e riempimento lista
    pgList_print(stdout, pgList);//stampa lista pg a video

    invArray_read("../inventario.txt", invArray);//lettura file e riempimento inventario
    invArray_print(stdout, invArray);//stampa inventario a video

    /*
     fin = fopen("pg.txt","r");
      pgList_read(fin, pgList);
      pgList_print(stdout,gList);
       fclose(fin);

      fin = fopen("inventario.txt","r");
      invArray_read(fin, invArray);
      invArray_print(stdout, invArray);
      fclose(fin);
  */


    fineProgramma = 0;
    do {
        stampaMenu(scelte, &selezione);//stampo menu' comandi
        switch(selezione) { //selezione e' il comando inserito da tastiera

            case 0: {//uscita
                fineProgramma = 1;
            }
                break;

            case 1: {//stampa lista personaggi a video
                pgList_print(stdout, pgList, invArray); //funz che si trova in pgList.c
            }
                break;

            case 2: {//stampa inventario a video
                invArray_print(stdout, invArray);//funz che si trova in invArray.c
            }
                break;

            case 3: {//cerca personaggio
                printf("Inserire codice personaggio: ");
                scanf("%s", codiceRicerca);
                pgp = pgList_searchByCode(pgList, codiceRicerca); //cerco personaggio per codice nella lista si trova in pgList.c
                if (pgp != NULL) {//se ho trovato stampo
                    pg_print(stdout, pgp, invArray);// si trova in pgList.c
                }
            }
                break;

            case 4: {//aggiungi personaggio alla lista da tastiera
                     pgList_add(pgList);//si trova in pgList.c
                  }
                 break;

            case 5: {//elimina personaggio in base al codice
                printf("Inserire codice personaggio: ");
                scanf("%s", codiceRicerca);
                pgList_remove(pgList, codiceRicerca);//si trova in pgList.c
            } break;

            case 6: {//modifica personaggio
                printf("Inserire codice personaggio: ");
                scanf("%s", codiceRicerca);
                pgp = pgList_searchByCode(pgList, codiceRicerca);//cerco personaggio per codice. Si trova in pgList.c
                if (pgp!=NULL) {
                    pg_updateEquip(pgp, invArray);//si trova in pg.c
                }
            } break;

            default:{
                printf("Scelta non valida\n");
            } break;
        }
    } while(!fineProgramma);

    pgList_free(pgList);//distruttore lista. Si trova in pgList.c

    return 0;
}
void stampaMenu(char *scelte[], int *selezione){//stampa menu' comandi possibili
    int i=0;
    printf("\nMenu':\n");
    for(i=0;i<7;i++)//for per stampare tutti i possibili comandi
        printf("%d -> %s\n",i,scelte[i]);
    scanf(" %d",selezione);//prendo comando da tastiera
}

//
// Created by seren on 19/12/2022.
//
#include <stdlib.h>
#include <stdio.h>

#include "pg.h"


static void pg_updateStatEquip(pg_t *pgp, invArray_t invArray) {
    int eqIndice;
    inv_t *invp;
    stat_t equipStat;
    pgp->eq_stat=pgp->b_stat;
    for(int i=0;i<EQUIP_SLOT;i++){
        eqIndice= equipArray_getEquipByIndex(pgp->equip,i);
        if(eqIndice==-1)
            continue;
        invp= invArray_getByIndex(invArray,eqIndice);
        if(invp==NULL)
            continue;
        equipStat= inv_getStat(invp);
        pgp->eq_stat=stat_add(pgp->eq_stat,equipStat);
    }
}

//lettura file pg.txt o da tastiera quando introduco nuovo personaggio
int pg_read(FILE *fp,pg_t *pgp){
    if(fscanf(fp,"%s %s %s",pgp->cod,pgp->nome, pgp->classe)<3)
        return 0;
    stat_read(fp,&(pgp->b_stat));
    pgp->eq_stat=pgp->b_stat;
    pgp->equip=equipArray_init();
    return 1;

}

//stampo singolo personaggio
void pg_print(FILE *fp,pg_t *pgp, invArray_t invArray){
    if (pgp==NULL)//controllo corretta allocaziona
        return;
    fprintf(fp,"%s, %s [%s]\n Base:",pgp->cod,pgp->nome,pgp->classe);
    stat_print(fp,&pgp->b_stat,1);//funz in inv.c
    if(equipArray_inUse(pgp->equip)>0){//funz in equipArray.c
        fprintf(fp,"  eq:");
        stat_print(fp,&pgp->eq_stat,1);//funz in inv.c
        if(pgp->equip!=NULL)
            equipArray_print(fp, pgp->equip,invArray);//stampa. Funz in equipArray.c
    }
}
//deallocazione
void pg_clean(pg_t *pgp){
    if(pgp->equip!=NULL)
        equipArray_free(pgp->equip);
    pgp->equip=NULL;
}
//modifica personaggio
void pg_updateEquip(pg_t *pgp,invArray_t invArray){
    equipArray_update(pgp->equip,invArray);
    pg_updateStatEquip(pgp,invArray);
}

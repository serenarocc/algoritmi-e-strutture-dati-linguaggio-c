//
// Created by seren on 19/12/2022.
//

#include <stdio.h>

#include "inv.h"

//leggo da file nome e tipo e poi mi collego all'altra funzione per leggere le statistiche
void inv_read(FILE *fp, inv_t *ip){
    fscanf(fp,"%s %s", ip->nome,ip->tipo);
    stat_read(fp,&(ip->stat));//lettura statistiche
}

//leggo da file le statistiche e riempio la struct stat_t
void stat_read(FILE *fp, stat_t *sp){
    fscanf(fp,"%d %d %d %d %d %d",&sp->hp,&sp->mp,&sp->atk,&sp->def,&sp->mag,&sp->spr);
}

//stampa inventario
void inv_print(FILE *fp, inv_t *ip){
    fprintf(fp,"%s %s \n",ip->nome,ip->tipo);
    stat_print(fp,&(ip->stat),0);
}


//stampa
/* si ammette la scelta di malus che portano almeno una statistica a valori negativi o nulli, ma in
questo caso questa statistica viene “mascherata” in fase di stampa, dove si stampa il valore fittizio 1,
 quando il valore è negativo o nullo. */
void stat_print(FILE *fp, stat_t *sp, int soglia){
    if(soglia==1){
        if(sp->hp > 0){
            fprintf(fp,"hp=%d, ",sp->hp);
        }
        else{
            fprintf(fp,"hp=%d, ",1);
        }
        if(sp->mp > 0){
            fprintf(fp,"mp=%d, ",sp->mp);
        }
        else{
            fprintf(fp,"mp=%d, ",1);
        }
        if(sp->atk > 0){
            fprintf(fp,"atk=%d, ",sp->atk);
        }
        else{
            fprintf(fp,"atk=%d, ",1);
        }
        if(sp->def > 0){
            fprintf(fp,"def=%d, ",sp->def);
        }
        else{
            fprintf(fp,"def=%d, ",1);
        }
        if(sp->mag > 0){
            fprintf(fp,"mag=%d, ",sp->mag);
        }
        else{
            fprintf(fp,"mag=%d, ",1);
        }
        if(sp->spr > 0 ){
            fprintf(fp,"spr=%d, ",sp->spr);
        }
        else{
            fprintf(fp,"spr=%d, ",1);
        }
        fprintf(fp,"\n\n");
    }
    else{
        fprintf(fp,"hp=%d,mp=%d,atk=%d,def=%d,mag=%d,spr=%d\n",sp->hp, sp->mp, sp->atk, sp->def, sp->mag, sp->spr);
    }
}
//ritorna il campo stat di un oggetto dell'inventario
stat_t inv_getStat(inv_t *invp) {
    return invp->stat;
}

//aggiornamento sestupla
stat_t stat_add(stat_t sa, stat_t sb) {
    sa.hp += sb.hp;
    sa.mp += sb.mp;
    sa.atk += sb.atk;
    sa.def += sb.def;
    sa.mag += sb.mag;
    sa.spr += sb.spr;
    return sa;
}

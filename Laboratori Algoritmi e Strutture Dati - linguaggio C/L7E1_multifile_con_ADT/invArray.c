//
// Created by seren on 19/12/2022.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "invArray.h"


struct invArray_s{
    inv_t *vettInv;
    int nInv;
    int maxInv;
};

invArray_t invArray_init(){//inizializzazione array
    invArray_t invArray =calloc(1,sizeof (*invArray));
    return invArray;
}

void invArray_read(char *filename, invArray_t invArray){
    FILE *fp;
    fp=fopen(filename,"r");//apertura file in lettura
    if(invArray==NULL){//allocazione calloc errata
        return;
    }
    if (fp==NULL) {//controllo corretta apertura file
        printf("errore apertura file");
        return;
    }
    fscanf(fp,"%d", &invArray->nInv);//prendo il  numero di righe del file
    invArray->vettInv=calloc(invArray->nInv,sizeof(inv_t));//vettore di tipo inv_t luno nInv
    for(int i=0;i<invArray->nInv;i++){
        inv_read(fp,&invArray->vettInv[i]);//si collega alla funz inv.h
    }
    fclose(fp);//chiusura file
    return;
}
//stampa array
void invArray_print (FILE *fp, invArray_t invArray){
    if (invArray==NULL)//controllo corretta allocazione vettore calloc
        return;
    for(int i=0; i<invArray->nInv;i++){ //stampa vettore
        inv_print(fp,&invArray->vettInv[i]);//funz in inv.c
    }
}
//stampa in base all'indice
void invArray_printByIndex(FILE *fp, invArray_t invArray, int indice){
    if(invArray==NULL)//controllo corretta allocazione vettore calloc
        return;
    inv_print(fp, &invArray->vettInv[indice]);//funz in inv.c
}
//ricerca nome nell'inventario
int invArray_searchByName(invArray_t invArray, char *name){
    if(invArray==NULL || name==NULL){ //se c'e' un errore di calloc oppure non ho la parola con cui confrontarlo
        return -1;
    }
    for(int i=0; i<invArray->nInv; i++){//scorrro tutto il vettore
        if(strcmp(invArray->vettInv[i].nome, name)==0){//se trovo una corrispondenza e trovo name
            return i;
        }
    }
    return -1;
}

inv_t *invArray_getByIndex(invArray_t invArray, int indice){
    if (invArray==NULL) //errore allocazione calloc
        return NULL;
    if(invArray->vettInv==NULL)
        return NULL;
    if(indice<0||indice>=invArray->nInv)
        return NULL;
    return &invArray->vettInv[indice]; //ritorno indice
}

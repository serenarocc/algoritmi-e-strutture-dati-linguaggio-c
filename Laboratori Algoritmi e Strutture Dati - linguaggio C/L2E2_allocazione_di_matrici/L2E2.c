#include <stdio.h>
#include <stdlib.h>

void separa(int **mat, int nr, int nc, int *vb,int *vn);
int **malloc2dR (FILE *f, int *nr, int *nc );

int main()   //scelgo che la prima casella sia bianca
{
    printf("Matrice presa da file:\n");

    FILE *fp;
    int i, j,c,r;
    int **m, nc,nr;
    int *vb, *vn;
    m = malloc2dR(fp, &nr, &nc );//funzione allocazione matrice
    //DICHIARI I VETTORI NERO E BIANCO
    vn = (int *) malloc( ((nr*nc)/2) * sizeof(int) );
    vb = (int *) malloc( ((nr*nc)/2)+1 * sizeof(int) ); // se  la matrice e' dispari ha un elemento bianco in piu', se invece e' pari i neri e bianchi sono in numero uguale
    printf("Scelgo che la prima casella sia bianca.\n");
    printf("Dimensione matrice: %d x %d\n",nr, nc);
    separa(m, nr,nc,vb,vn);
    puts("Vettore caselle 'nere':");
    for ( i = 0; i < ((nr*nc)/2); i++ )
    {
        printf("%d ", vn[i] );
    }

    puts("\nVettore caselle 'bianche':");
    for ( i = 0; i < (nr*nc)/2+1; i++ )
    {
        printf("%d ", vb[i] );
    }

    // due for per la free
    // for ( i = 0; i < nr; i++ )
    //  for ( j = 0; j < nc; j++ )
    free(m);
    // free(vb);
    //free(vn);
    return 0;
}
int **malloc2dR (FILE *f, int *nr, int *nc )
{

    int i,j,numr,numc,**mat;
    f=fopen("../mat.txt","r");//apertura file
    if (f == NULL)
        exit(-1);
    fscanf(f, "%d %d", nr, nc);//salvo prima riga del file
    //INIZZIALIZZAZIONE DINAMICA MATRICE
    mat = (int **) malloc( *nr * sizeof(int *) );
    for ( i = 0; i < *nr; i++ )
    {
        mat[i] = (int *) malloc(*nc * sizeof(int));
    }
    // FINE INIZIALIZZAZIONE
    //metti dentro la matrice il file:
    for ( i = 0; i < *nr; i++ )
    {
        for ( j = 0; j < *nc; j++ )
        {
            fscanf( f, "%d ", &mat[i][j] );
            printf("%d  ", mat[i][j] );
        }
        printf("\n");
    }

    fclose(f);
    return mat;
}
void separa ( int **mat, int nr, int nc, int *vb, int *vn ) {
    int i, j; //indici matrice
    int ib = 0; //indice del vett bianco
    int in = 0; //indice del vett nero
    for ( i = 0; i < nr; i++ )
    {
        for (j = 0; j < nc; j++)
        {
            if (i % 2 == 0 && j % 2 == 0) // se i pari e j pari allora e' Bianco
                vb[ib++] = mat[i][j]; //metto il contenuto della casella della scacchiera dentro il vettore

            if (i % 2 == 0 && j % 2 != 0) //nero
                vn[in++] = mat[i][j];

            if (i % 2 != 0 && j % 2 == 0) //nero
                vn[in++] = mat[i][j];

            if (i % 2 != 0 && j % 2 != 0) //bianco�
                vb[ib++] = mat[i][j];
        }
    }
}

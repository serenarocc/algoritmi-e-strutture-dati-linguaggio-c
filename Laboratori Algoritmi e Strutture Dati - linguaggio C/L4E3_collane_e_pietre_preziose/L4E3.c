#include <stdio.h>
#include <stdlib.h> //per malloc

int **malloc2d(int r, int c);
void free2d(int **set, int r);
int readFile(FILE *fp, int **set);
int collana(int pos, int *val, char *sol, int n, int k, int cnt, int flag, char prec_pietra, int *maxDim);
int stampa_soluzione(char *sol,int tot);

int main() {
    int n;//variabile che conterrà il numero di rghe del file
    int **setPietre; //matrice di interi
    int sumPietre=0, maxDim=0;
    char sol[70]; //vettore che contiene le soluzioni

    FILE *fp;
    fp=fopen("../set.txt","r");
    if(fp==NULL){//controllo corretta apertura file
        printf("Errore apetura file");
        return -1;
    }
    fscanf(fp,"%d",&n);//prendo prima rig del file

    setPietre=malloc2d(n,4);//alloco matrice di malloc
    readFile(fp,setPietre);//leggo il file e lo salvo nella matrice malloc chiamata setpietre

    for(int i=0;i<n;i++){
        printf("\nTest numero:%d\n",i);
        sumPietre=setPietre[i][0]+setPietre[i][1]+setPietre[i][2]+setPietre[i][3];//somma delle gemme
        collana(0, setPietre[i], sol, 4, sumPietre, 0, 0, 'n', &maxDim);
        maxDim=0;
    }
    /*
     * pos=inidice del vettore sol all'inizio e' 0
     * val=valori riga matrice che contiene file. Inidica quanti z,r,s,t ci sono nel test
     * sol=vettore soluzione
     * n=4
     * k=dimensione max dei sottoinsiemi. Ovvero la soluzione max che puoi trovare, con il primo test e' 19+6+8+5
     * cnt=registra il numero di soluzioni
     * flag=
     * prec_pietra=pietra precedente
     * maxdim=
    */


    free2d(setPietre,n);
    fclose(fp);
    return 0;
}
int **malloc2d(int r, int c){//funzione di tipo puntatore a vettore int
    int **set;
    int i;

    set=malloc(r*sizeof(int*));//set variabile locale (doppio puntatore): putatore a vettore di puntatori(righe)
    for(i=0;i<r;i++){
        set[i]=malloc(c*sizeof(int));
    }
    return set;
}

int readFile(FILE *fp, int **set){//leggo da file il numero di z zaffiri, r rubini, t topazi, s smeraldi e lo salvo nella matrice malloc
    int i =0;
    while(fscanf(fp,"%d %d %d %d",&set[i][0], &set[i][1], &set[i][2], &set[i][3])>=4){
        i++;
    }
    /*&setofset[i][0] corrisponde z
     * &setofset[i][1] corrisponde r
     * &setofset[i][2] corrisponde t
     * &setofset[i][3] corrisponde s
     */
    return 1;
}

/*
 * pos=inidice del vettore sol all'inizio e' 0
 * val=valori riga matrice che contiene file. Inidica quanti z,r,s,t ci sono nel test
 * sol=vettore soluzione
 * n=4
 * k=dimensione max dei sottoinsiemi. Ovvero la soluzione max che puoi trovare, con il primo test e' 19+6+8+5
 * cnt=registra il numero di soluzioni
 * flag=
 * prec_pietra=pietra precedente
 * maxdim=
 */
int collana(int pos, int *val, char *sol, int n, int k, int cnt, int flag, char prec_pietra, int *maxDim){
//non bisogna per forza finire tutte le pietre quinidi come condizione di uscita si mette che 2 su 4 pietre siano finite(flag)
    if(pos>k){ //se il vett soluzione e' piu' grande del sumPietre non ha senso
        return 1;
    }

  if( flag==1 && pos>(*maxDim)){ //se ho trovato la soluzione di lunghezza massima

        stampa_soluzione(sol,pos); //stampo la soluzione
        *maxDim=pos; //valore massimo della lunghezza della collana
        printf("\nTotale pietre: %d", k);//k=sumPietre
        printf("\nCollana massima di lunghezza %d\n\n", (*maxDim) - 1);
    }

        //creazione collana

        //ZAFFIRO
        //prev_gem=n vuol dire che devo andare a mettere ancora la prima pietra
        if (prec_pietra == 'n' || prec_pietra == 'z') {
            if (val[0] > 0) {//se ho ancora zaffiri
                sol[pos] = 'z'; //metto uno zaffiro nella soluzione
                val[0]--;//decremento il numero di zaffiri perche' ne ho appena usato uno
                //incremento pos perche' ho appena messo una pietra nella collana
                //metto come pietra precedente z
                collana(pos + 1, val, sol, n, k, cnt, 0, 'z', maxDim);//flag=0 perche' val contiene ancora elementi
                sol[pos] = 'n';// non va a buon fine. metto null nella collana e al posto del zaffiro ci vado a mettere il rubino
                val[0]++; //ricremento il num di zaffiri perche' non l'ho usato
            }
            if (val[1] > 0) {//se ho ancora rubini
                sol[pos] = 'r';//metto un rubino nella collana
                val[1]--;//decremento il nnumero di rubini perche' ne ho appena usato uno
                //incremento pos perche' ho appena messo una pietra nella collana
                //metto come pietra precedente r
                collana(pos + 1, val, sol, n, k, cnt, 0, 'r', maxDim);//flag=0 perche' val contiene ancora elementi
                sol[pos] = 'n';// non va a buon fine. metto null nella collana, ricomincio da capo la sua creazione
                val[1]++;//ricremento il num di r perche' non l'ho usato
            }

           if ((val[0] <= 0 && val[1] <= 0) && pos + 1 > (*maxDim)) //se ho finito zaffiri e rubini e ho superato la dimensione massima
                collana(pos + 1, val, sol, n, k, cnt, 1, 'm', maxDim);//flag=1 perche' val non contiene più elementi
                //m inidica che ho finito i 2 tipi di pietre
        }

        //RUBINI
        if (prec_pietra == 'n' || prec_pietra == 'r') {
            if (val[3] > 0) {//se ho ancora smeraldi
                sol[pos] = 's';//metto uno smeraldo nella collana
                val[3]--;//decremeno il num di smeraldi perche' ne ho appena usato uno
                collana(pos + 1, val, sol, n, k, cnt, 0, 's', maxDim);//flag=0 perche' val contiene ancora elementi
                sol[pos] = 'n';//non va a buon fine. metto null nella collana
                val[3]++;//rincremento il num di s perche' non l'ho usato
            }
            if (val[2] > 0) {
                sol[pos] = 't';
                val[2]--;
                collana(pos + 1, val, sol, n, k, cnt, 0, 't', maxDim);//flag=0 perche' val contiene ancora elementi
                sol[pos] = 'n';
                val[2]++;
            }
            if ((val[3] <= 0 && val[2] <= 0) && pos + 1 > (*maxDim))//ho finito smeraldi e topazi
                collana(pos + 1, val, sol, n, k, cnt, 1, 'm', maxDim);
        }
        //TOPAZI
        if (prec_pietra == 'n' || prec_pietra == 't') {
            if (val[0] > 0) {
                sol[pos] = 'z';
                val[0]--;
                collana(pos + 1, val, sol, n, k, cnt, 0, 'r', maxDim);
                sol[pos] = 'n';
                val[0]++;
            }
            if (val[1] > 0) {
                sol[pos] = 'r';
                val[1]--;
                collana(pos + 1, val, sol, n, k, cnt, 0, 'r', maxDim);
                sol[pos] = 'n';
                val[0]++;
            }
            if (val[0] <= 0 && val[1] <= 0 && pos + 1 > (*maxDim))
                collana(pos + 1, val, sol, n, k, cnt, 1, 'm', maxDim);
        }

        //SMERALDI
        if (prec_pietra == 'n' || prec_pietra == 's') {
            if (val[3] > 0) {
                sol[pos] = 's';
                val[3]--;
                collana(pos + 1, val, sol, n, k, cnt, 0, 's', maxDim);
                sol[pos] = 'n';
                val[3]++;
            }
            if (val[2] > 0) {
                sol[pos] = 't';
                val[2]--;
                collana(pos + 1, val, sol, n, k, cnt, 0, 't', maxDim);
                sol[pos] = 'n';
                val[2]++;
            }
           if (val[3] <= 0 && val[2] <= 0 && pos + 1 > (*maxDim))
                collana(pos + 1, val, sol, n, k, cnt, 1, 'm', maxDim);
        }

}


int stampa_soluzione(char *sol,int tot){ //stampo tutto il vettore soluzione per gli pos elementi
   for(int i=0;i<tot-1; i++){
        printf("%c", sol[i]);
    }
    return 1;
}

void free2d(int **set, int r){//free riceve puntatore by value, non restituisce il risultato
    int i;
    for(i=0;i<r;i++){
        free(set[i]);//prima libera le singole righe
    }
    free(set);//poi libera il vettore di puntatori
}

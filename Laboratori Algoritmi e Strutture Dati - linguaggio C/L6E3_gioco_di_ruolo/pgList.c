//
// Created by seren on 15/12/2022.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "pgList.h"

typedef struct nodoPg_s { //nodo nella lista personaggi
    pg_t pg;
    struct nodoPg_s *next;
} nodoPg_t, *linkPg;

//def nodi
struct pgList_s { //lista personaggio testa,coda
    linkPg head, tail;
    int nPg;
};

static linkPg newNodoPg(pg_t pg, linkPg next);

static linkPg newNodoPg(pg_t pg, linkPg next) {//allocazione di un nodo
    linkPg n = calloc(1, sizeof(*n));
    if (n == NULL)
        return NULL;
    n->pg = pg;
    n->next = next;
    return n;
}

pgList_t pgList_init() {
    pgList_t pgList = calloc(1, sizeof(*pgList));
    return pgList;
}

void pgList_insert(pgList_t pgList, pg_t pg) {
    linkPg toAdd;
    toAdd = newNodoPg(pg, NULL);
    if (toAdd == NULL || pgList == NULL)
        return;
    if(pgList->head == NULL)//se la coda e' vuota lo inserisco in testa
        pgList->head = pgList->tail = toAdd;
    else {//aggiungo elemento in lista
        pgList->tail->next = toAdd;
        pgList->tail = toAdd;
    }
}

void pgList_read(char *filename, pgList_t pgList) {//lettura lista e sua compilazione
    pg_t pg;
    FILE *fp = fopen(filename, "r");//apertura file in lettura
    if (pgList == NULL)
        return;
    if (fp == NULL)//se file non e' aperto correttamente
        return;

    while((pg_read(fp, &pg)) != 0) {//finche' leggo un personaggio da file lo inserisco in lista
        pgList_insert(pgList, pg);//inserisco elemento in lista
        pgList->nPg++;
    }

    fclose(fp);//chiusura file
}

void pgList_print(FILE *fp, pgList_t pgList, invArray_t invArray) {//stampa lista
    linkPg iter = NULL;
    if (pgList == NULL)
        return;
    for(iter=pgList->head; iter!=NULL; iter=iter->next)//attraversamento lista
        pg_print(fp, &iter->pg, invArray);//stampa nodo lista
}

pg_t *pgList_searchByCode(pgList_t pgList, char* cod){//cerco nodo nella lista in base al codice
    linkPg iter = NULL;
    if (pgList == NULL)
        return NULL;
    for(iter = pgList->head; iter != NULL; iter=iter->next) {//attraverso tutta la lista
        if (strcmp(iter->pg.cod, cod) == 0) {//se ho trovato il codice del personaggio nella lista
            return (&iter->pg);//ritorno personaggio
        }
    }
    return NULL;//caso in cui il codice non e' trovato
}

void pgList_add(pgList_t pgList){//aggiungo un nodo nella lista scritto da tasiera
    pg_t pg;

    printf("Cod Nome Classe HP MP ATK DEF MAG SPR: ");
    if (pg_read(stdin, &pg) != 0) {
        /* Eventuale invocazione di ricerca per check duplicati */
        pgList_insert(pgList, pg);//inserzione dati da tastiera nel nodo della lista
    }
}

void pgList_remove(pgList_t pgList_t, char* cod){//rimuovo un nodo dalla lista
    linkPg iter, prev;
    for(iter = pgList_t->head, prev = NULL; iter != NULL; prev = iter, iter=iter->next) {//attraverso lista
        if (strcmp(iter->pg.cod, cod) == 0) {//se trovo il codice del personaggio che devo eliminare
            if (iter == pgList_t->head && iter == pgList_t->tail) {
                pgList_t->head = pgList_t->tail = NULL;
            } else if (iter == pgList_t->head) {
                pgList_t->head = iter->next;
            } else if (iter == pgList_t->tail) {
                pgList_t->tail = prev;
                prev->next = NULL;
            } else {
                prev->next = iter->next;
            }
            pg_clean(&iter->pg);
            free(iter);
            break;
        }
    }
}

void pgList_free(pgList_t pgList) {//libero la lista
    linkPg iter, next;
    for(iter = pgList->head; iter != NULL; iter=next) {
        next = iter->next;
        pg_clean(&iter->pg);
        free(iter);
    }
}

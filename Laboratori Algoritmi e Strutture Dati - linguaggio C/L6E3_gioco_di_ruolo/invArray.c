//
// Created by seren on 15/12/2022.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "invArray.h"

struct invArray_s {
    inv_t *vettInv;
    int nInv;
    int maxInv;
};

invArray_t invArray_init() {
    invArray_t invArray = calloc(1, sizeof(*invArray));
    return invArray;
}

void invArray_read(char *filename, invArray_t invArray) {
    int i;
    FILE *fp = fopen(filename, "r");//apertuta file lettura
    if (invArray == NULL)//controllo se calloc e' andata a buon fine
        return;
    if (fp == NULL)//controllo corretta apertura file
        return;
    fscanf(fp, "%d", &invArray->nInv);//num di oggetti O
    invArray->vettInv = calloc(invArray->nInv, sizeof(inv_t));//alloco vett di struct
    for(i=0;i<invArray->nInv;i++) {
        inv_read(fp, &invArray->vettInv[i]);
    }
    fclose(fp);//chiusura file
    return;
}

void invArray_print(FILE *fp, invArray_t invArray) {//stampo array di inventario
    int i;
    if (invArray == NULL)
        return;
    for(i=0;i<invArray->nInv;i++)
        inv_print(fp, &invArray->vettInv[i]);
}

void invArray_printByIndex(FILE *fp, invArray_t invArray, int index) {//stampo array di inventario per indice
    if (invArray == NULL)
        return;
    inv_print(fp, &invArray->vettInv[index]);
}

int invArray_searchByName(invArray_t invArray, char *name) {//cerco nell'array inventario per il nome
    int i;
    if (invArray == NULL || name == NULL)
        return -1;
    for(i=0;i<invArray->nInv;i++) {
        if (strcmp(invArray->vettInv[i].nome, name) == 0)//controllo se ho trovato il nome nell'inventario
            return i;
    }
    return -1;
}

inv_t *invArray_getByIndex(invArray_t invArray, int index) {
    if (invArray == NULL)
        return NULL;
    if (invArray->vettInv == NULL)
        return NULL;
    if (index < 0 || index >= invArray->nInv)
        return NULL;
    return &invArray->vettInv[index];
}

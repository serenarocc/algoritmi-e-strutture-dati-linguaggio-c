//
// Created by seren on 15/12/2022.
//

#ifndef STRUTTUREL6E3_CONSEGNA_INVARRAY_H
#define STRUTTUREL6E3_CONSEGNA_INVARRAY_H


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "inv.h"

typedef struct invArray_s *invArray_t;//puntatore a struct in invArray.c

invArray_t invArray_init();
void invArray_read(char *filename, invArray_t invArray);
void invArray_print(FILE *fp, invArray_t invArray);
void invArray_printByIndex(FILE *fp, invArray_t invArray, int index);
inv_t *invArray_getByIndex(invArray_t invArray, int index);
int invArray_searchByName(invArray_t invArray, char *name);

#endif //STRUTTUREL6E3_CONSEGNA_INVARRAY_H

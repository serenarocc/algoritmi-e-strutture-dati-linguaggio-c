//
// Created by seren on 15/12/2022.
//

#ifndef STRUTTUREL6E3_CONSEGNA_PG_H
#define STRUTTUREL6E3_CONSEGNA_PG_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "equipArray.h"

typedef struct pg_s {//struttura dati personaggi
    char cod[LEN];
    char nome[LEN];
    char classe[LEN];
    stat_t b_stat, eq_stat;
    equipArray_t equip;
} pg_t;

int pg_read(FILE *fp, pg_t *pgp);
void pg_print(FILE *fp, pg_t *pgp, invArray_t invArray);
void pg_updateEquip(pg_t *pgp, invArray_t invArray);
void pg_clean(pg_t *pgp);

#endif //STRUTTUREL6E3_CONSEGNA_PG_H

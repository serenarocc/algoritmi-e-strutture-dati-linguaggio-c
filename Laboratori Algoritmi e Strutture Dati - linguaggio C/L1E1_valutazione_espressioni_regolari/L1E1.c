#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define N 1000
#define M 100
char *cercaRegexp(char *src, char *regexp);
int main()
{
    // stringa regexp con all'interno la cosa da cercare
    char regexp[M] = "\\ar\\A[^aei]b.e[lm]i d. r\\a[^ade]ce";
    printf("%s\n",regexp);

    char src[N] = "I pra I prDoblemi di riicerca di stringhe all’interno di testi e collezioni di stringhe solitamente di dimensione maggiore rispetto alla stringa cercata si basano raramente su un confronto esatto ma molto spesso necessitano di rappresentare in modo compatto non una ma un insieme di stringhe cercate evitandone per quanto possibile l’enumerazione esplicita";
    printf("%s\n",src);

    char *punt;
    punt=cercaRegexp(src, regexp);

    if(punt==NULL)
    {
        printf("Codifica non trovata");
    }
    else
    {
        printf("Codifica trovata!");
    }
    /*COMMENTI SULLE SCELTE CHE HO FATTO:
     - ciò che c'è in regexp va rispettato, quindi se c'è uno spazio deve esserci anche nel testo e viceversa
     - ho misurato le stringhe per ridurre le dim dei cicli  (m e n)
     */


    return 0;
}
char *cercaRegexp(char *src, char *regexp){
    int n=strlen(src);
    int m=strlen(regexp);//dimensioni massime che possono avere i cicli
    int j=0, start = 1,d;
    int iniz=0;
    int flag = 0,i,t;
    for(d=0; d<n && flag==0; d++)
    {
        //uso il for per scorrere src fino a quando dentro il while regexp non si verifica totalmente
        iniz=d;
        j = d; t = 0; i=0; flag = 1; // vero
        while (regexp[i] != '\0' && flag == 1) //per restare nel while flag deve essere uguale a 1
        { //leggo regexp per tutta la sua lunghezza

            if (isalpha(regexp[i]))
            {
                if (src[j] == regexp[i]) // carattere corrispondere a carattere, vai avanti
                {
                    j++;//scorro sr
                    i++;//scorro regexp
                }
                else // falso
                {
                    flag = 0; //falso
                    break;//esce dal while
                }
            }
            else
            { //esamino i casi con i metacaratteri
                if (regexp[i] == '[' && regexp[i + 1] == '^')
                {
                    i = i + 2;
                    for (i = i; regexp[i] != ']'; i++)
                    {
                        if (src[j] == regexp[i])
                        {
                            flag = 0;//esce dal while, errore forma [^fff]
                            break;
                        }
                    }
                    i++;  //salto la parentesi chiusa
                    j++; //aggiorno anche la j
                }

                if (regexp[i] == '[' && regexp[i + 1] != '^')
                {
                    i++;
                    for (i = i; regexp[i] != ']'; i++)
                    {
                        if (src[j] == regexp[i])
                        { t = 1; } // t=1 trovato, mi basta che diventi vero una volta
                    }
                    if (t == 0)
                    {//non è mai stato vero
                        flag = 0;
                    }
                    t = 0; //rimetto a falso t
                    i++;
                    j++;
                }
                if (regexp[i] == '\\' && regexp[i + 1] == 'A')
                {
                    if(isalpha(src[j]) && (src[j]==toupper(src[j])))
                    {
                        j++;
                        i=i+2;
                    }
                    else{flag=0;}
                }
                if (regexp[i] == '\\' && regexp[i + 1] == 'a')
                {
                    if(isalpha(src[j]) && (src[j]==tolower(src[j])))
                    {
                        j++;
                        i=i+2;
                    }
                    else{flag=0;}
                }
                if (regexp[i] == '.')
                {// va bene qualunque carattere
                    if (isalpha(src[j]))
                    {
                        i++;
                        j++;
                    }
                }
                if (isspace(regexp[i]) && isspace(src[j]))
                {
                    i++;
                    j++;
                }
                else
                {
                    if ((isspace(regexp[i]) && !isspace(src[j])) || (!isspace(regexp[i]) && isspace(src[j])))
                    {
                        flag = 0;
                    }
                }
            }
        }
    }

    if (flag==1)
    { //hai trovato tutta regeexp detro str
        char * p= &src[iniz-1]; //codifica trovata
        return p;
    }
    else
    {
        return NULL; //codifica non trovata
    }
}

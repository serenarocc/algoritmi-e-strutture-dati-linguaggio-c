#include <stdio.h>
#include <stdlib.h>
#define MAXDIM 10

int majority(int *a, int N);
void recoursive_count(int *a, int l, int r, int *occ);

int main() {
    int a[MAXDIM]={3, 3, 9, 4, 3, 5, 3};
    int mj_value = 0;
    mj_value = majority(a, 7);
    if(mj_value>=0) {
        printf("\n Elemento maggioritario del vettore: %d", mj_value);
    } else
        printf("\n Non esiste un elemento maggioritario");
    return 0;
}

int majority(int *a, int N){
    int i=0, max=0;
    int mj_value=-1;
    for(i=0; i<N; i++){
        if(a[i]>max){
            max = a[i];
        }
    }
    int *occ = calloc(max, sizeof(int));
    recoursive_count(a, 0, N, occ);
    /*for(i=0; i<max; i++){
        printf(" %d ", occ[i]);
    }*/
    //Ricerca del valore maggioritario
    for(i=0; i<max; i++){
        if(occ[i]>N/2){
            mj_value = i+1;
        }
    }
    return mj_value;
}

void recoursive_count(int *a, int l, int r, int *occ){
    int m = (l + r)/2;
    if(l==r){
        occ[a[l]-1]+=1;
        return;
    }
    recoursive_count(a, l, m, occ);
    recoursive_count(a, m+1, r, occ);
}

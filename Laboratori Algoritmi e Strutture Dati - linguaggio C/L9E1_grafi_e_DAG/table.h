//
// Created by seren on 19/01/2023.
//

#ifndef STRUTTUREL9E1_TABLE_H
#define STRUTTUREL9E1_TABLE_H

struct s_ST{
    char *vertex;
    int maxN;
};
typedef struct s_ST ST;

ST *STinit(int maxN);
void STinsert(ST* tab, char key, int index);
int STSearchByKey(ST tab, char key);
char STSearchByIndex(ST tab, int index);


#endif //STRUTTUREL9E1_TABLE_H

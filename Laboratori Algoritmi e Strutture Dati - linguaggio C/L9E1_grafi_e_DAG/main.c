#include <stdio.h>
#include "grafo.h"

void GenerateSetE(Graph *g);

int main() {
    int i, V, a=1;
    Graph *g = NULL;
    FILE *in;
    int *ts, isAcyclic=1;

    in = fopen("../grafo2.txt", "r");
    g = GRAPHload(in);
    printf("Grafo originale\n");
    GraphStore(g, stdout); //Stampa grafo originale

    GRAPHdfs(g, &isAcyclic);   //Ricerca di archi B per determinare se e' un DAG o no

    if (!isAcyclic) {
        printf("Il grafo originale e' ciclico\n\n");//quindi non e' un dag
        GenerateSetE(g);
        printf("Il DAG risultante e'\n\n");
        GraphStore(g, stdout);
        printf("\n");
    }
    else
        printf("Il grafo originale e' gia' un DAG\n\n");//quindi non e' ciclico



    ts = malloc(g->V * sizeof(int)); //ordinamento topologico del grafo
    //Creo vettore dell'ordinamento topologico del DAG
    DAGrts(g, ts);//riempio vett ts

    printf("Ordinamento topologico\n");
    for (i=0; i<V; i++)
        printf("%c ", STSearchByIndex(*(g->tab), ts[i]));//dato un indice restituisce nome vertice
    printf("\n\n");

     //Funzione per il calcolo delle distanze da ogni nodo sorgente verso ogni altro nodo del DAG
     //da terminare

    return 0;
}

void CombSempl(int pos, int *sol, Graph *g, Edge *vE, int n, int k, int start, int *stop, int *bestSol, int *bestWt) {
    int i, a=1, solWt;
    if (pos >= k) {
        //Rimozione del set di archi ottenuto dal modello delle combinazioni semplici
        for (i=0; i<k; i++)//rimuovi tutti gli archi della soluz trovataa attraverso odello comb semplici
            GRAPHremoveE(g, vE[sol[i]].v, vE[sol[i]].w);//ottengo grafo senza archi della soluzione
        solWt = GRAPHedgesWt(g, vE, sol, k);//peso tot iniseme archi che hai rimosso ovvero peso tot soluzione

        //Verifica se il grafo risultante dal passo precedente è aciclico o no
        GRAPHdfs(g, &a);//per verifivare che ora sia un dag quinidi per verificare la aciclicita

        //a =iascicilic dice se e o no aciclico

        // Se aciclico, imposta condizione di stop per set a cardinalita maggiore di questa e aggiorna best se migliorante
        if (a) {
            *stop = 1; //ho soluz ottima
            if (solWt > *bestWt) {//se peso inisieme e migliore di quello di prima
                *bestWt = solWt;
                for(i=0;i<k;i++)
                    bestSol[i] = sol[i];
            }
        }

        /* Ripristino del set di archi scelti */
        //sto lavorando su grafo originale quinidi ripristino tutti gli archi che ho tolto

        for (i=0; i<k; i++)
            GRAPHinsertE(g, vE[sol[i]].v, vE[sol[i]].w, vE[sol[i]].wt);

        return;
    }
    for (i=start; i<n; i++) {
        sol[pos] = i;
        CombSempl(pos+1, sol, g, vE, n, k, i+1, stop, bestSol, bestWt);
    }
}

void GenerateSetE(Graph *g) { //non e' dag.
    //trova inisieme archi a peso max la cui rimozione genera un dag
    int i, j, E = g->E, V = g->V, stop = 0, bestWt = 0;
    Edge *vE = malloc(E*sizeof(Edge));//vett che conterra archi
    int *sol;
    int *bestSol = calloc(E, sizeof(int));
    int limit = E - (V - 1); //Numero di archi max che è possibile rimuovere per far rimanere un grafo valido


    if (limit <= 0)
        return;
    printf("\nV=%d, E=%d, numero massimo di archi che è possibile rimuovere %d\n", V, E, limit);
    //Funzione che copia in vE[] il set di archi che compongono il grafo
    GraphEdges(g, vE);

    // esclude il set vuoto e itera fino al massimo a cardinalita'  |E|-(|V|-1)
    //provo a rimuovere 1->2-> 3 archi e verifica passo passo
    //prende l'insieme di archi, fa le combinazioni semplici di n elem a k a k
    //k=i
    for(i=1; i<=limit && !stop; i++) {
        sol = malloc(i * sizeof(int));
        printf("Generazione set di cardinalita' %d\n", i);
        CombSempl(0, sol, g, vE, E, i, 0, &stop, bestSol, &bestWt);
    }
    //implementazione funzione di stampa degli archi rimossi

    //costruzione del DAG finale ottimo
    if (stop) //stop =ho trovato dag e gli insieme di archi rimosso e' a peso max
        for (j=0; j<i-1; j++)
            GRAPHremoveE(g, vE[bestSol[j]].v, vE[bestSol[j]].w);//rimuove insieme archi a peso max
    return;
}

//
// Created by seren on 19/01/2023.
//

#include "table.h"

#include "table.h"
#include "stdlib.h"
#include "stdio.h"

//table contine la tabella di simboli

ST *STinit(int maxN){
    ST *st = malloc(sizeof(ST));
    st->vertex= calloc(maxN,sizeof (char));
    st->maxN = maxN;
    return st;
}

void STinsert(ST* tab, char key, int index){
    if(index<tab->maxN){
        tab->vertex[index]=key;
    }
    return;
}

int STSearchByKey(ST tab, char key){
    int i=0;
    for(i=0; i<tab.maxN; i++)
        if(tab.vertex[i]==key)
            return i;
    return -1;
}

char STSearchByIndex(ST tab, int index){
    if(index<tab.maxN)
        return tab.vertex[index];
    return -1;
}

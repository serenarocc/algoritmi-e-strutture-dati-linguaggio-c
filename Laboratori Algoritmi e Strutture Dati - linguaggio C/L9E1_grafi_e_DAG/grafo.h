//
// Created by seren on 19/01/2023.
//

#ifndef STRUTTUREL9E1_GRAFO_H
#define STRUTTUREL9E1_GRAFO_H

#include "table.h"
#include "stdio.h"
#include "stdlib.h"

typedef struct s_node *link;
struct s_node{
    int v;
    int wt;
    link next;
};

struct s_Graph{
    int V;
    int E;
    link *ladj;
    ST *tab;
    link z;
};
typedef struct s_Graph Graph;

struct edge{
    int v;
    int w;
    int wt;
};
typedef struct edge Edge;

Graph *GRAPHload(FILE *in);
void GraphStore(Graph *G,FILE *fout);
void GRAPHdfs(Graph *g, int *isAcyclic);
void GraphEdges(Graph* G, Edge *a);
void GRAPHinsertE(Graph *G, int d1, int d2, int wt);
void GRAPHremoveE(Graph *G, int d1, int d2);
int GRAPHedgesWt(Graph *g, Edge *a, int *subset, int k);
void DAGrts(Graph *g, int *ts);

#endif //STRUTTUREL9E1_GRAFO_H

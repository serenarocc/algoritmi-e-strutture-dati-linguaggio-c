//
// Created by seren on 19/01/2023.
//

#include "grafo.h"

void insertE(Graph *G, Edge e);
void removeE(Graph *G, Edge e);
Graph* GraphInit(int V);
void GRAPHinsertE(Graph *G, int d1, int d2, int wt);
Edge EdgeCreate(int v, int w, int wt);
void dfsR(Graph *g, Edge e, int *time, int *pre, int *post, int *isAcyclic);
void TSdfsR(Graph *g, int v, int *ts, int *pre, int *time);
void DAGmaxPath(Graph *g, int *ts);

Graph *GRAPHload(FILE *in) {
    int i, V, wt, id1, id2;
    char src, dst;
    Graph *g;

    fscanf(in, "%d\n", &V);
    g = GraphInit(V);
    if (g == NULL)
        return NULL;

    for (i=0; i<V; i++) {
        fscanf(in, "%c\n", &src);
        STinsert(g->tab, src, i);
    }

    while(fscanf(in, "%c %c %d\n", &src, &dst, &wt) == 3) {
        id1 = STSearchByKey(*(g->tab), src);
        id2 = STSearchByKey(*(g->tab), dst);
        if (id1 != id2 && id1 >= 0 && id2 >= 0)
            insertE(g, EdgeCreate(id1, id2, wt));
    }

    return g;
}

//Allocazione nuovo arco
link NewNode(int v, int wt, link next){
    link x=malloc(sizeof(*x));
    x->v=v;
    x->wt=wt;
    x->next = next;
    return x;
}

Edge EdgeCreate(int v, int w, int wt)
{
    Edge e;
    e.v = v;
    e.w = w;
    e.wt = wt;
    return e;
}

//Inizializzazione grafo
Graph* GraphInit(int V){
    int v;
    Graph *G = malloc(sizeof(Graph));
    G->V=V;
    G->E = 0;
    G->z = NewNode(-1, -1, NULL);
    G->ladj = malloc(G->V*sizeof(link));
    //Inizializzazione lista adiacenze
    for(v=0; v<G->V; v++){
        G->ladj[v]=G->z;
    }
    G->tab = STinit(G->V);
    return G;
}

//Wrapper function insertE
void GRAPHinsertE(Graph *G, int d1, int d2, int wt){
    insertE(G, EdgeCreate(d1, d2, wt));
}

//Inserimento Arco in testa -> si aggancia al nuovo nodo la precedente head della sottolista
void insertE(Graph *G, Edge e){
    int v=e.v, w=e.w, wt=e.wt;
    G->ladj[v]= NewNode(w, wt, G->ladj[v]);
    G->ladj[w]= NewNode(v, wt, G->ladj[w]);
    G->E++;
}

//Wrapper function removeE
void GRAPHremoveE(Graph *G, int d1, int d2){
    removeE(G, EdgeCreate(d1, d2, 0));
}

void removeE(Graph *G, Edge e){
    int v=e.v, w=e.w;
    link x, p;
    for(x=G->ladj[v], p=NULL; x!=G->z; p=x, x=x->next){
        if(x->v==w){
            if(x==G->ladj[v])
                G->ladj[v]=x->next;
            else
                p->next=x->next;
            break;
        }
    }
    for (x=G->ladj[w], p=NULL; (x) != G->z ;p=x, x=x->next) {
        if(x->v==v){
            if(x==G->ladj[w])
                G->ladj[w]=x->next;
            else
                p->next=x->next;
            break;
        }
    }
    G->E--;
    free(x);
}

void GraphEdges(Graph* G, Edge *a){
    int v, E=0;
    link t;
    for(v=0; v<G->V; v++)
        for(t=G->ladj[v]; t!=G->z; t=t->next)
            if(v<t->v)
                a[E++]= EdgeCreate(v,t->v, t->wt);
    return;
}

void GraphStore(Graph *G,FILE *fout){
    int i=0;
    Edge *a;

    a= malloc(G->E*sizeof(Edge));
    GraphEdges(G, a);
    fprintf(fout, "%d\n", G->V);
    for(i=0; i<G->V; i++)
        fprintf(fout, "%c\n", STSearchByIndex(*G->tab, i));
    for(i=0; i<G->E; i++)
        fprintf(fout, "%c %c %d\n", STSearchByIndex(*G->tab, a[i].v), STSearchByIndex(*G->tab, a[i].w),
                a[i].wt);
    return;
}

//Deep first search, scrive nella variabile *isAcyclic se il grafo è aciclico o no
void GRAPHdfs(Graph *g, int *isAcyclic) {//wrapper di dfsr
    int *pre, *post, v, time = 0;
    if (g == NULL)
        return;
    if (g->ladj == NULL)
        return;
    pre = calloc(g->V, sizeof(int));
    post = calloc(g->V, sizeof(int));

    for(v=0;v<g->V;v++)
        pre[v] = post[v] = -1;

    for(v=0;v<g->V;v++)
        if (pre[v] == -1)
            dfsR(g, EdgeCreate(v, v, 0), &time, pre, post, isAcyclic);

    free(pre);
    free(post);
    return;
}

void dfsR(Graph *g, Edge e, int *time, int *pre, int *post, int *isAcyclic){
    //pre indica il tempo di scoperta
    //post indica il tempo di fine elaborazione
    //v vertice antenato
    link t;
    int v, w=e.w;
    Edge x;
    pre[w]=(*time)++;
    for(t=g->ladj[w]; t!=g->z; t=t->next) {
        if (pre[t->v] == -1)
            dfsR(g, EdgeCreate(w, t->v, 0), time, pre, post, isAcyclic);
        else
        if (post[v] == -1)//se fine elaboraz antenato ancora non e'stato assegnato
            *isAcyclic = 0;
    }
    post[w]=(*time)++;
}

int GRAPHedgesWt(Graph *g, Edge *a, int *subset, int k) {
    int i, totWt=0;
    if (g == NULL)
        return 0;
    for (i=0; i<k; i++)
        totWt +=a[subset[i]].wt;
    return totWt;
}

//Ordinamento topologico DAG
//Funzione wrapper
void DAGrts(Graph *g, int *ts) {
    int v, time = 0, *pre;
    pre = malloc(g->V * sizeof(int));
    //Allocazione di pre e ts
    for (v = 0; v < g->V; v++) {
        pre[v] = -1;
        ts[v] = -1;
    }
    for (v = 0; v < g->V; v++){
        if(pre[v]==-1)
            TSdfsR(g, v, ts, pre, &time);
    }
}

//Creazione vettore dell'ordinamento topologico
void TSdfsR(Graph *g, int v, int *ts, int *pre, int *time){
    link t;
    pre[v]=0;
    for(t=g->ladj[v]; t!=g->z; t=t->next)
        if(pre[t->v]==-1)
            TSdfsR(g, v, ts, pre, time);
    ts[(*time)++]=v;//scrittura ts
    return;
}

char GRAPHgetName(Graph *g, int index) {
    if (g == NULL)
        return NULL;
    if (g->tab == NULL)
        return NULL;
    return g->tab->vertex[index];
}

void DAGmaxPath(Graph *g, int *ts) {
    int v, w, i, j, k;
    int *dist = malloc(g->V * sizeof(int));

    for (i=0; i<g->V;i++) {
        v = ts[i]; //v e' il primo vertice dell'ordinamento topologico
        printf("Start: %c\n", GRAPHgetName(g, v));
        for (j=0; j<g->V; j++)
            dist[j] = -1; //Concettualmente rappresenta la non raggiungibilità
        dist[v] = 0;//vertice da cui parto ha distanza 0 da se stesso
        for (j=i; j<g->V; j++) {
            w = ts[j]; //assegna al secondo vertice dell' arco il valoere ts dell'odinamento topologico. In questo caso prende arco che ritona a se stesso
            if (dist[w] == -1) // non raggiungibile
                continue;
            for (k=0; k<g->V; k++) {
                // da terminare
            }
        }
        for (j=0; j<g->V; j++) {
            if (j==v)
                continue;
            printf("  -> %c [%d]\n", GRAPHgetName(g, v), dist[j]);
        }
    }
    free(dist);
    return;
}

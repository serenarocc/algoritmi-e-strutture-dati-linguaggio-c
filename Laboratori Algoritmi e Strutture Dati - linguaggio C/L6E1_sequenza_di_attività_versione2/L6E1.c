#include <stdio.h>
#include <stdlib.h>//per calloc

typedef struct att_{
    int s;//tempo inizio
    int f;//tempo fine
}att;

att *leggiFile(FILE *fp,int *n);
void MergeSort(att *A, int n);
void MergeSortR(att *A,att *B, int l, int r);
void Merge(att *A, att *B, int l, int q, int r);
void dinamica(att *v, int n);
int compatibile(att *v, int i, int j);
void printLis(att *v,int *P, int i);

int main() {
    att *v;
    int n;

    FILE *fp; //apertura file
    fp=fopen("../att1.txt","r");
    if(fp==NULL){//controllo corretta apertura file
        printf("errore apertura file");
        return -1;
    }

    v=leggiFile(fp,&n);//leggo da file e salvo nel vettore di strutture
    printf("Stampa di attività da file:\n");
    for(int i=0;i<n;i++){//stampo le attivita' in disordine cosi' come sono nel file
    printf("(%d,%d)",v[i].s,v[i].f);
    }
    //ordino le attivita' per tempo di fine crescente
    //capitolo 2 ordinamenti ricorsivi
    MergeSort(v,n);
    printf("\n\nStampa di attivita' ordinate per tempo di fine crescente\n");
    for(int i=0;i<n;i++){//stampo le attivita' in ordine di tempo di fine crescente
        printf("(%d,%d)",v[i].s,v[i].f);
    }
    printf("\n\n Programmazione dinamica");
    dinamica(v,n);//applico programmaz dinamica per trovare gli intervalli di tempo compatibili di durata massima

    fclose(fp);//chiusura file
    return 0;
}

att *leggiFile(FILE *fp,int *n){
    att *v;

    fscanf(fp,"%d\n",n);//leggo quante righe contiene il file
    v=calloc((*n+1),sizeof(att)); //vettore lungo n di strutture att

    for(int i=0;i<*n;i++){
        fscanf(fp,"%d %d\n",&v[i].s,&v[i].f); //alloco la calloc leggendo da file
    }
    return v;//ritorno il vett di strutture
}
//ordinamenti ricorsivi capitolo 2
void MergeSort(att *A, int n){
    int l=0, r=n-1;
    att *B;
    B=(att *)malloc((n)*sizeof(att));//vettore ausiliario
    if(B==NULL){//controllo corretta allocazione malloc
        printf("errore malloc");
        exit(1);
    }
    MergeSortR(A,B,l,r);
}
//ordinamenti ricorsivi capitolo 2
void MergeSortR(att *A,att *B, int l, int r){
    int q;
    if(l>=r)//caso terminazione
        return;
    q=(l+r)/2;//indice elemento di mezzo
    MergeSortR(A,B,l,q);//chiamata ricorsiva
    MergeSortR(A,B,q+1,r);//chiamata ricorsiva
    Merge(A,B,l,q,r);//ricombinazione
}
//tecniche di programmazione cap 6
void Merge(att *A, att *B, int l, int q, int r){
    int i,j,k;
    i=l;
    j=q+1;
    for(k=l;k<=r;k++)
        if(i>q)
            B[k]=A[j++];
        else if(j>r)
            B[k]=A[i++];
        else if (A[i].f<=A[j].f)
            B[k]=A[i++];
        else B[k]=A[j++];
    for(k=l;k<=r;k++)
        A[k]=B[k];
    return;
}
//capitolo 5 slide 85
void dinamica(att *v, int n){
    int i,j, last=1;//last è l indice della longest increasing migliore
    int durata, ris=1;
    int *L;//vettore L salva la durata complessiva delle sequenze di attivita' scelte
    L=calloc((n+1),sizeof(int));
    int *P; //vettore P salva la posizione nel vettore della attivita' precedentemente scelta
    P=calloc((n+1),sizeof(int));

    durata=v[0].f-v[0].s;//durata
    L[0]=durata; //primissima durata
    P[0]=-1;//posizione dell' attivita' precedente

    for(i=1;i<n;i++){
       L[i]=(v[i].f-v[i].s); P[i]=-1;

        for(j=0;j<i;j++){
            if (compatibile(v,i,j)==1){//controllo compatibilita' delle attivita'
                L[i]=L[j]+(v[i].f-v[i].s); //se l'attivita' e' compatibile gli incremento la durata dell'attivita' corrente nel contatore di dutarate delle attivita' che sto prendendo
                P[i]=j;//salvo la posizione della precedente attivit' presa j
            }
        }
        if(ris<L[i]){//se la soluzione che sto considerando e' maggiore del massimo(ris) calcolato fino ad ora
            ris=L[i];//salvo durata nuova soluzione max
            last=i;//indice della longest increasing sequence
        }
    }
    printf("\n");
    for (i=0; i<n; i++){ //stampo vett L
        printf("%d  ", L[i]);
    }
    printf("\n");

    for (i=0; i<n; i++){ //stampo vett P
        printf("%d  ", P[i]);
    }


    printf("\nStampa massimo:\n");//stampa soluzione
    printLis(v, P,last);
}


int compatibile(att *v, int i, int j){ //condizione per cui le due attivita' sono compatibili -> pdf lab 5 ex 1
    if((v[i].s<v[j].f)&&(v[j].s<v[i].f)){
        return 0;
    }
    return 1;
}
//capitolo 5 slide 86
void printLis(att *v,int *P, int i){ //stampo la soluzione
    if(P[i]==-1){
        printf("(%d,%d)", v[i].s,v[i].f);
        return;
    }
    printLis(v,P,P[i]);
    printf("(%d,%d)", v[i].s,v[i].f);
}

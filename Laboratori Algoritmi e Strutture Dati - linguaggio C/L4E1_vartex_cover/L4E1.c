#include <stdio.h>
#include <stdlib.h> //usarto per allocare calloc

typedef struct arco_ {
    int u;
    int v;
} arco;

int powerset_comb_sempl(int start, int pos, int *sol, int k, arco *a, int n, int e, int cnt);
int check(int *sol, int k, arco *a, int e);


int main() {
    int n, e, *sol, k, cnt, tot=0,i;
    arco *a;

    FILE *fp;
    //apertura file scrittura e controllo
    if((fp= fopen("../grafo.txt", "r"))==NULL) {
        printf("\n Errore apertura file grafo.txt");
        return -1;
    }

    //Leggo N->numero di vertici ed E->numero di archi
    fscanf_s(fp, "%d %d", &n, &e);

    a = (arco *)calloc(e, sizeof(arco));//alloco vett dinamico di e strutture archi
    if (a == NULL) //controllo corretta allocazione
        return -2;

    for (i=0; i<e; i++) //leggo da file e alloco l'arco nella forma u e v
        fscanf(fp, "%d %d", &a[i].u, &a[i].v);

    //alloco vettore soluzione
    sol = calloc(n,sizeof(int));


    printf("Vertex cover\n");
    for(k = 1; k <= n; k++) { //k indica la dimensione del vertexcover che va da 1 fino alla dimensione totale del grafo
        printf("di dimensione %d\n", k);
        cnt = powerset_comb_sempl(0, 0, sol, k, a, n, e, 0);
        /*indice start determina a partire da quale valore di val si inizia a riempire sol*/
        if (cnt==0)//cnt registra il numero di soluzioni
            printf("nessuna soluzione \n");
        tot +=cnt;
    }
    printf ("numero di soluzioni: %d \n", tot);

    free(sol);
    free(a);
    return 0;
}


//combinazioni semplici
int powerset_comb_sempl(int start, int pos, int *sol, int k, arco *a, int n, int e, int cnt) {
    int i;
    if (pos >= k) {//terminazione dove k indica la dimensione del vertexcover che voglio calcolare
        if (check(sol, k, a, e)) {//controllo se soluz e' valida
            for(i=0;i<k;i++)
                printf("%d ", sol[i]);
            printf("\n");
            return cnt+1;
        }
        return cnt;
    }
    for (i=start; i<n; i++) {//iterazione sulle scelte
        sol[pos] = i;//il vettore sol viene riempitoa partire dall'indice pos con i valori
        cnt = powerset_comb_sempl(i+1, pos+1, sol, k, a, n, e, cnt);//una volta assegnato a sol il valore val[i], si ricorre con i+1 e pos+1
    }
    return cnt;//cnt registra il numero di soluzioni
}

int check(int *sol, int k, arco *a, int e) {
    int i, j, arcocnt = 0;
    int *arcocheck = calloc(e, sizeof(int));

    for (i=0; i<k; i++) {
        for (j=0; j<e; j++) {
            if (a[j].u == sol[i] || a[j].v == sol[i]) {
                if (arcocheck[j] == 0)
                    arcocnt++;
                arcocheck[j] = 1;
            }
        }
    }
    free(arcocheck);
    return (arcocnt == e);
}

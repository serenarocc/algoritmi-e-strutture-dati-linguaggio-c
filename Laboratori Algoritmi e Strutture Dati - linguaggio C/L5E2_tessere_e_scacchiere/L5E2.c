#include <stdio.h>
#include <stdlib.h> //per malloc

int CNT=1;

typedef struct config_s{//configurazione scacchiera dove ogni tessera ha 2 colori e 2 valori
    char col[2];
    int val[2];
}Config;


typedef struct tile_s {
    Config config[2]; //vettore lungo 2 di struttura config
}Tile;


typedef struct cell_s{ //ogni cella ha
    int tindex;//un indice
    int rot;//una possibile rotazione
}Cell;


Cell **malloc2D(int nr, int nc);
void free2D(Cell**pointer, int dim1);
Tile* readTileSet(int *nTiles);
int readBoard(FILE *fp2, Cell**board, int nr, int nc);
int fillValSet(Cell *val, Cell**board, int nr, int nc);
int findBestVal(Cell**board, Cell*val, Tile *tileSet, int nr, int nc, int valDim, int numTiles);
int disp_smp(int pos, Cell*val, Cell*sol, int *mark, int n, int k, Cell**board, int nr, int nc, Cell*bestSol, int *bestVal, Tile *tileSet);
int checkSol(Cell*sol, int dim);
int evaluateSol(Cell**board, int nr, int nc, Tile *tileSet, Cell *sol, int dimSol);
int fillBoard(Cell *board[], int nr, int nc, Cell *sol, int dimSol);
int evaluateRows(Cell**board, int nr, int nc, Tile *tileSet);
int evaluateColumns(Cell**board, int nr, int nc, Tile *tileSet);
void printBoard(Cell **board, int nr, int nc);
void printCell(Cell*val, int len);

int main() {
    Tile *tileSet;
    Cell **board, *val;
    int nr=0, nc=0, i, j;
    int numTiles=0, valDim=0;

    FILE *fp2;
    fp2 = fopen("../board.txt", "r"); //apro in lettura file board.txt
    if (fp2==NULL){
        printf("errore apertura file");
        return -2;
    }

    //Lettura num tessere del set.
    tileSet = readTileSet(&numTiles);



    //Verifica di aver letto correttamente i file e inizializzato correttamente la struttura dati
    for(i=0; i<numTiles; i++){
        printf("\n");
        for(j=0; j<2; j++){
            printf_s("Tessera #%d configurazione %d: ", i, j);
            printf_s("%c %d %c %d\n", tileSet[i].config[j].col[0], tileSet[i].config[j].val[0],
                     tileSet[i].config[j].col[1], tileSet[i].config[j].val[1]);
        }
    }



    //Lettura file contenente configurazione tavola
    fscanf_s(fp2, "%d %d", &nr, &nc);
    board = malloc2D(nr, nc);//alloco matrice malloc
    readBoard(fp2, board, nr, nc);

    //Stampa tavola da gioco
    printf_s("\nStampo file board.txt: ");
    for(i=0; i<nr; i++){
        printf_s("\n");
        for (int j = 0; j < nc; ++j) {
            printf_s("%d/%d ", board[i][j].tindex, board[i][j].rot);
        }
    }
    printf_s("\n\n");



    //Alloco vettore di celle val da passare alla funzione di esplorazione
    val = calloc(2*numTiles,sizeof(Cell));
    //Creo il vettore di celle ancora INUTILIZZATE
    valDim = fillValSet(val, board, nr, nc);
    //Ciclo su tessere non utilizzate per check valori
    printf_s("\nVettore val ad inzio partita: \n");
    printCell(val, valDim);
    //Trovo la soluzione migliore
    findBestVal(board, val, tileSet, nr, nc, valDim, numTiles);
    free2D(board, nr);

    return 1;
}

Cell **malloc2D(int nr, int nc){//funz di tipo puntatore a vettore Cell
    Cell **board;
    int i=0;

    board = malloc(nr*sizeof(Cell*));//variabile locale puntatore a vettore di puntatori
    for(i=0; i<nr; i++){
        board[i] = calloc(nc, sizeof(Cell));
    }
    return board;//ritorno come risultato
}

void free2D(Cell**pointer, int dim1){//deallocazione matrice malloc
    int i=0;
    for (int i = 0; i < dim1; i++) {
        free(pointer[i]);
    }
    free(pointer);
    return;
}

Tile *readTileSet(int *numEl){
    FILE *fp1;
    Tile *tileSet=NULL;
    int i;

    fp1 = fopen("../tiles.txt", "r");//apertura file
    fscanf(fp1, "%d\n", numEl);//numEl indica quante righe ho nel file

    tileSet = calloc(*numEl, sizeof(Tile));//vettore di strutture lungo numEl

    for(i=0; i<*numEl; i++){
        //salvo elem del file
        fscanf(fp1, "%c %d %c %d\n", &tileSet[i].config[0].col[0], &tileSet[i].config[0].val[0],&tileSet[i].config[0].col[1], &tileSet[i].config[0].val[1]);
       //configurazione ruotata
        tileSet[i].config[1].col[0] = tileSet[i].config[0].col[1];
        tileSet[i].config[1].col[1] = tileSet[i].config[0].col[0];
        tileSet[i].config[1].val[0] = tileSet[i].config[0].val[1];
        tileSet[i].config[1].val[1] = tileSet[i].config[0].val[0];
    }

    return tileSet;
}

int readBoard(FILE *fp2, Cell **board, int nr, int nc){//leggo da file board.txt l'indice e la rotaizone delle celle dalvandole nella matrice board
    int i=0, j=0;
    for(i=0; i<nr; i++){
        for(j=0; j<nc; j++){
            fscanf_s(fp2, "%d/%d",& board[i][j].tindex, &board[i][j].rot);
        }
    }
    return 1;
}

int fillValSet(Cell *val, Cell**board, int nr, int nc){
    int i=0, j=0;
    int x=0;
    Cell copyVal[nr*nc]; //Do per scontato che ci siano tante tessere quante sono le celle della scacchiera

    //Inizializzazione copyVal
    for(i=0; i<nr*nc; i++){
        copyVal[i].tindex = i;
        copyVal[i].rot = 0;
    }

    //Ciclo per eliminare dal set di tiles quelle già poszionate
    for (i = 0; i < nr; i++) {
        for(j=0; j<nc; j++){
            if(board[i][j].tindex != -1){
                for(x=0; x<nr*nc; x++){
                    if(copyVal[x].tindex == board[i][j].tindex){
                        copyVal[x].tindex=-1;
                        break;
                    }
                }
            }
        }
    }
    for(i=0; i<nr*nc; i++){
        printf_s("Tessera %d/%d\n", copyVal[i].tindex, copyVal[i].rot);
    }
    printf_s("\n\n");

    //Ciclo ricopiatura tessere non utilizzate
    for(i=0, x=0; i<nr*nc; i++){
        if(copyVal[i].tindex != -1){
            val[x].tindex = copyVal[i].tindex;
            val[x+1].tindex = copyVal[i].tindex;
            val[x].rot = copyVal[i].rot;
            val[x+1].rot = !copyVal[i].rot;
            x += 2;
        }
    }
    return x;
}

int findBestVal(Cell**board, Cell*val, Tile *tileSet, int nr, int nc, int valDim, int numTiles){
    int bestVal = 0;
    Cell sol[valDim/2], bestSol[valDim/2];
    int *mark;

    mark = calloc(valDim, sizeof(int));
    disp_smp(0, val, sol, mark, valDim, valDim/2, board, nr, nc, bestSol, &bestVal, tileSet);
    printf_s("\nSoluzione migliore: \n");
    printCell(bestSol, valDim/2);
    fillBoard(board, nr, nc, bestSol, valDim/2);
    printf_s("\nTavola completa di massimo valore: \n");
    printBoard(board, nr, nc);
    printf_s("Valore massimo: %d", evaluateSol(board, nr, nc, tileSet, bestSol, valDim/2));

    free(mark);
    //Print soluzione migliore
    return 1;
}
//disposizioni semplici capitolo 3
int disp_smp(int pos, Cell*val, Cell*sol, int *mark, int n, int k, Cell**board, int nr, int nc, Cell*bestSol, int *bestVal, Tile *tileSet) {
    int i = 0;
    int currentVal = 0;
    /*if(pos>=1) {
        printf_s("\nPartial solution: \n");
        printCell(sol, pos);
    }*/
    if (pos > 1 && checkSol(sol, pos) == 0){
        return 1;
    }

    if(pos>=k){ //caso terminazione
        currentVal = evaluateSol(board, nr, nc, tileSet, sol, pos);
        if(currentVal > *bestVal){
            //aggiorno valore totale migliore
            *bestVal = currentVal;
            //Ricopio soluzione migliore
            for(i=0; i<pos; i++)
                bestSol[i]=sol[i];
        }
        return 1;
    }

    for(i=0; i<n; i++){//iterazione sulle n scelte
        if(mark[i]==0){//se l'elemento no l'ho ancora preso
            mark[i]=1;//lo prendo
            sol[pos]=val[i];
            disp_smp(pos+1, val, sol, mark, n, k, board, nr, nc, bestSol, bestVal, tileSet);//ricorsione
            mark[i]=0;//smarcamento
        }
    }
    return 1;
}


int checkSol(Cell*sol, int dim){
    int i=0, j=0;

    for(i=0; i<dim; i++){
        for(j=0; j<dim; j++){
            if(i!=j && sol[i].tindex == sol[j].tindex)
                return 0;
        }
    }
    /* printf_s("\nCurrent solution: \n");
     printCell(sol, dim);*/

    return 1;
}

int evaluateSol(Cell**board, int nr, int nc, Tile *tileSet, Cell *sol, int dimSol){
    int i=0, j=0;
    int rowSum=0, colSum=0;
    Cell **cpyBoard;

    cpyBoard = malloc2D(nr, nc);

    //Ciclo ricopiatura tavola da gioco
    for(i=0; i<nr; i++)
        for(j=0; j<nc; j++)
            cpyBoard[i][j]=board[i][j];

    //Riempio Tavola
    fillBoard(cpyBoard, nr, nc, sol, dimSol);

    /*CNT+=1;
    printf_s("Tavola %d: ", CNT);
    printBoard(cpyBoard, nr, nc);*/

    //Calcolo somma righe
    rowSum = evaluateRows(cpyBoard, nr, nc, tileSet);

    //Calcolo somma colonne
    colSum = evaluateColumns(cpyBoard, nr, nc, tileSet);

    free2D(cpyBoard, nr);

    return rowSum + colSum;
}

int fillBoard(Cell *board[], int nr, int nc, Cell *sol, int dimSol){
    int i=0, j=0, x=0;

    for(i=0; i<nr; i++){
        for(j=0; j<nc; j++){
            if(board[i][j].tindex == -1 && x<dimSol){
                board[i][j] = sol[x];
                x++;
            }
        }
    }

    return 1;
}

int evaluateRows(Cell**board, int nr, int nc, Tile *tileSet){
    int sum=0, tot=0;
    int i=0, j=0;

    for(i=0; i<nr; i++)
    {   sum = tileSet[board[i][0].tindex].config[board[i][0].rot].val[0];
        for(j=1; j<nc; j++){
            if(tileSet[board[i][j].tindex].config[board[i][j].rot].col[0]
               != tileSet[board[i][j-1].tindex].config[board[i][j-1].rot].col[0]){
                sum=0;
                break;
            } else{
                sum += tileSet[board[i][j].tindex].config[board[i][j].rot].val[0];
            }
        }
        tot+=sum;
    }
    return tot;
}

int evaluateColumns(Cell**board, int nr, int nc, Tile *tileSet){
    int sum=0, tot=0;
    int i=0, j=0;

    for(j=0; j<nc; j++)
    {   sum = tileSet[board[0][j].tindex].config[board[0][j].rot].val[1];
        for(i=1; i<nr; i++){
            if(tileSet[board[i][j].tindex].config[board[i][j].rot].col[1]
               != tileSet[board[i-1][j].tindex].config[board[i-1][j].rot].col[1]){
                sum=0;
                break;
            } else{
                sum += tileSet[board[i][j].tindex].config[board[i][j].rot].val[1];
            }
        }
        tot+=sum;
    }
    return tot;
}

void printBoard(Cell **board, int nr, int nc){
    int i=0, j=0;

    printf_s("\n Tavola: \n\n");
    for (int i = 0; i < nr; i++) {
        for (int j = 0; j < nc; j++) {
            printf_s("%d/%d ", board[i][j].tindex, board[i][j].rot);
        }
        printf_s("\n");
    }
    printf_s("\n");
    return;
}

void printCell(Cell*val, int len){
    int i=0;
    for(i=0; i<len; i++){
        printf_s("Tessera %d/%d\n", val[i].tindex, val[i].rot);
    }
}

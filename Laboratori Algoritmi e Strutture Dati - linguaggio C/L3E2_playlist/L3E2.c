#include <stdio.h>
#include <stdlib.h>
typedef struct
{
    int numero;
    char canzone[5][256]; //matrice di caratteri
} amico;

int generaplaylist( amico *a, int *sol, int max, int pos, int count, FILE *f);

/*Ogni amico ha diritto a proporre fino a cinque canzoni tra cui scegliere.
 La playlist viene creata scegliendo tante canzoni quanti sono gli amici,
 e per ognuno di essi scegliendo una ed una sola canzone tra quelle da lui proposte.
 Tutte le canzoni sono distinte.
 Il numero di amici lo dice il file.
*/

int main ()
{
//PRINCIPO DI MOLTIPLICAZIONE=calcolo in quanti modi totali si puo fare una scelta che si puo effettuare in N passi.

    FILE *fp;
    FILE *fp1;
    int na; //numero di amici
    int *sol;
    fp1 = fopen( "../stampa.txt", "w");
    if ( fp1 == NULL )
    {
        printf("Errore apertura file");
        return -1;
    }
    fp = fopen( "../brani.txt", "r");
    if ( fp == NULL )
    {
        printf("Errore apertura file");
        return -2;
    }
    // acquisizione file
    fscanf(fp,"%d",&na); //NUM AMICI
    sol = (int *) malloc( na * sizeof(int) ); //vetore grande 5 perche ci sono 5 amici. viene riscritto, ci sono 900 sol
    amico am[na]; //e' un vettore grande 5, dove ogni suo nodo e' una struttura che corrisponde a 1 amico.
    //Dentro questa struct/nodo ho il num di canzoni proposte dall'amico e i titoli di queste canzoni
    amico *a=&am;//amico *a e' puntatore al vettore am che a sua volta contiene na struct
    for(int i=0; i<na; i++)
    {
        fscanf(fp,"%d",&am[i].numero); //num di canzoni che ogni amico propone
        for(int j=0; j<am[i].numero; j++)
        {
            fscanf(fp,"%s", am[i].canzone[j]); //leggi le canzoni che ogni amico propone
        }
    }
    int tot=generaplaylist(a,sol,na, 0,0,fp1);
    printf("\nCi sono in tutto %d playlist possibili.\n", tot);
    free(sol);
    fclose(fp);
    fclose(fp1);
    return 0;

}
int generaplaylist( amico *a, int *sol, int max, int pos, int count, FILE *fp1)
{
    //amico *a e' puntatore al vettore am che a sua volta contiene na struct
    //il vettore puntatore a sol(*sol) lo uso per stampare tutte le possibili playlist. E' grande 5 come il numero di amici
    //max corrisponde a na=num amici
    //pos e' uguale a 0 all'inizio
    //count e' uguale a 0 all'inizio
    int i;
    if (pos >= max)
    {
        for (i = 0; i < max; i++)
        {   // i indica l'amico
            //sol[pos]  indica l'indice della cnazone che stampo del i-esimo amico
            printf("%s\n ", a[i].canzone[sol[i]]);
            fprintf(fp1, "%s\n", a[i].canzone[sol[i]]);
        }
        printf("\n");
        fprintf(fp1,"\n");
        return count+1;//ho appena trovato e stampato una soluzione allora incremento il contatore delle soluzioni tot
    }
    for (i = 0; i < a[pos].numero; i++)  //a[pos].numero = num canzoni proposte dall' amico in questione
    {
        sol[pos] = i; //indichera' l'indice per la stampa printf
        count = generaplaylist(a,sol,max,pos+1, count, fp1);
        //incremento pos per funzione ricorsiv
    }
    return count;//non ho trovato la soluzione quindi non incremento il contatore delle soluzioni tot
}

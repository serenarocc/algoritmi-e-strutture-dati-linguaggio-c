#include <stdio.h>
#include <stdlib.h>//per malloc


typedef struct elemento_s{
    char nome[100];
    int tipologia; //2=avanti 1=indietro 0=transizione
    int ingresso;// 1=frontale 0=spalle
    int uscita;// 1=frontale 0=spalle
    int precedenza;//0=primo, 1=non e' il primo elemento
    int finale;//1=ultimo elem, 0=non ultimo
    float valore;//valore reale dell'elemento
    int difficolta;//diffiolta esecuzione elemento
}elemento_t;

typedef struct elementi_s{
    elemento_t *vett_e;//puntatore a struct elemento_t
    int numElem;//num di elementi
}elementi_t;


typedef struct diagonale_s{
    int *elementi;
    int numElem;//numero di elementi della diag
    int difficolta; //difficolta della diag
    float valore;//valore reale della diag
}diagonale_t;


typedef struct diagonali_s {
    diagonale_t *vett_d;//puntatore a struct diagonale_t
    int numDiag; //num diag
    int maxd;
} diagonali_t;


diagonali_t *inizializzDiagonali();
elementi_t *leggiElementi();
void generaDiagonali(elementi_t *E, diagonali_t *D, int DD);
void disp_rip(elementi_t *E, diagonali_t *D, int DD, int pos, int diff, int acro, int direzione, int finale, int *diag) ;
void generaProgramma(elementi_t *E, diagonali_t *D, int DP);
void comb_rip(elementi_t *E, diagonali_t *D, int DP, int pos, int *prog, int diff, int *bestprog, float *bestval, int *bestbonus, int *numd, int start);
int checkValido(elementi_t *E, diagonali_t *D, int *prog, int nd, float *val, int *B) ;



int main() {
    int DD=10, DP=20;
    diagonali_t *D=inizializzDiagonali();
    elementi_t *E=leggiElementi();
    printf("DD=%d, DP=%d\n", DD,DP);
    printf("Generazione di diagonali:\n");
    generaDiagonali(E,D,DD);
    printf("Ci sono %d diagonali possibili\n\n",D->numDiag);
    printf("Generazione programma migliore:\n");
    generaProgramma(E,D,DP);
    return 0;
}


diagonali_t *inizializzDiagonali(){

    diagonali_t *D;
    D=malloc(1*sizeof (*D));//alloco vett dim 1 di struct
    if(D==NULL){//errore malloc
        return NULL;
    }

    D->vett_d= calloc(1,sizeof (diagonale_t));//alloco dentro D->vett_d un vett di dim 1 di tipo struct diagonale_t
    if(D->vett_d==NULL){//errore calloc
        return NULL;
    }
    D->numDiag=0;//inizzializzo num diag a 0
    D->maxd=1;//inizializzo al max ho 1 diag
    return D;
}



elementi_t *leggiElementi(){
    FILE *fp;
    fp=fopen("../elementi.txt","r");
    if(fp==NULL){
        printf("errore apertura file");
    }

    elementi_t *E;
    E=malloc(1*sizeof (*E)); //alloco vett dim 1 di tipo elementi_t
    if(E==NULL){//errore allocazione malloc
        return NULL;
    }

    fscanf(fp,"%d",&E->numElem);//inidca quante righe ha il file

    E->vett_e=calloc(E->numElem, sizeof (elemento_t));//vettore lungo numElem di tipo struct elemento_t
    if(E->vett_e==NULL){//errore allocazione calloc
        return  NULL;
    }
    for(int i=0;i<E->numElem;i++){//allocazione di E->vett_e con i dati del file
        fscanf(fp,"%s %d %d %d %d %d %f %d\n", E->vett_e[i].nome,&E->vett_e[i].tipologia,&E->vett_e[i].ingresso,&E->vett_e[i].uscita,&E->vett_e[i].precedenza,&E->vett_e[i].finale,&E->vett_e[i].valore,&E->vett_e[i].difficolta);

    }
    fclose(fp);//chiusura file
    return E;//ritorno vett si struct
}



void generaDiagonali(elementi_t *E, diagonali_t *D, int DD){
    int *diag;
    diag=calloc(5,sizeof (int));//ogni diagonale ha al max 5 elementi
    disp_rip(E, D, DD, 0, 0, 0, 1, 0, diag);
}


//disposizione con ripetizione capitolo 3
//uso le disposizioni con ripetizione perche'cosi' ottengo diagonali  dove gli elemeneti sono diversamente ordinati, oppure gli elementi
//che figurano in una diagonale, figurano anche nell'altra diagonale ma sono ripetute in un numero diverso di volte. In piu' ogni elemento puo' essere ripetuto fino a k volte
//n=E->numElem

void disp_rip(elementi_t *E,diagonali_t *D, int DD, int pos, int diff, int acro,int direzione, int finale,int *diag){
    int i;
    if(pos>0){
        //il ginnasta deve includere almeno un elemento acrobatico in ogni diagonale
        if(acro>0){//se ho fatto almeno una acrobazia per diagonale
            //in un programma posso avere al max 3 diag
            //dopo che ho riempito almeno con un elemento la prima diag(pos=1) devo andare a riinizializzare i valori per trovare la prossima diag
            //caso ho rovato 1 diag: D->numDiag =1 e maxd=1 entro nell'if. maxd diventa =2
            //caso ho trovato 2 diag:D->numDiag =2 e maxd=2 entro nell'if. maxd diventa =4
            //caso ho trovato 3 diag (ho finito il programma): D->numDiag =3 e maxd=4 non entro nell'if.
            if(D->numDiag==D->maxd){ //se ho
                D->maxd*=2;
                D->vett_d= realloc(D->vett_d,D->maxd*sizeof (diagonale_t));//rialloco la diagonale per renderla disponibile per trovare la prossima diagonale
                if(D->vett_d==NULL){ //controllo correttezza realloc
                    exit(-1);
                }
            }
            //riinizzializzo tutti i valori per andare a trovare poi la prossima diaonale
            D->vett_d[D->numDiag].difficolta=diff;
            D->vett_d[D->numDiag].numElem=pos;
            D->vett_d[D->numDiag].elementi= malloc(pos*sizeof (int));
            D->vett_d[D->numDiag].valore=0.0;
            for(i=0;i<pos;i++){
                D->vett_d[D->numDiag].valore+=E->vett_e[diag[i]].valore;
                D->vett_d[D->numDiag].elementi[i]=diag[i];
            }
            D->numDiag++;//incremento il num di diag che sto trovando
        }
    }

    //caso terminazione
    if(pos>=5 || (finale==1)) {//se ho messo 5 elementi in diagonale, ho finito
        return;
    }

    for(i=0;i<E->numElem;i++){ //iterazione sulle N scelte
        //se la condizione dentro if non e' rispettata, continue ti fa iterare nel for con i++

        if(direzione !=E->vett_e[i].ingresso) continue;
        if((diff + E->vett_e[i].difficolta)>DD ) continue;   //se la difficolta della diag fin ora piu' la difficolta dell'elemento corrente supera DD allora passo ad un altro elemento con for
        if(pos==0 && E->vett_e[i].precedenza==1) continue;// se e' il primo elemento della diag ma il requisito di precedenza dice che l'elemento e' preceduto da un altro elemento allora continue
        diag[pos]=i; //scelta elemento
        disp_rip(E,D,DD,pos+1,diff + E->vett_e[i].difficolta, acro +E->vett_e[i].tipologia, E->vett_e[i].uscita, E->vett_e[i].finale,diag);

    }
}





void generaProgramma(elementi_t *E, diagonali_t *D, int DP){
    int i,j,bonus,d,numd=0;
    float bestval=-1.0;

    int *prog= malloc(3*sizeof (int));//programma deve avere 3 diagonali da consegna
    int *bestprog= malloc(3*sizeof (int));//bestprogramma(soluzione) deve avere 3 diagonali da consegna

    comb_rip(E,D,DP,0,prog,0,bestprog,&bestval,&bonus,&numd,0);

    if(numd>0){//se ho trovato almento una diagonale
        printf("Totale punteggio= %.3f\n\n",bestval);//punteggio max trovato
        for(i=0;i<numd;i++){//stampo diagonali trovate
            d=bestprog[i];

            if(bonus == i){
                printf("Diagonale %d di valore: %.3f ha moltiplicazione *1.5 in quanto l'ultima diagonale ha un elemento di difficolta >=8\n",d,D->vett_d[d].valore);
            }
            else{
                printf("Diagonale %d di valore: %.2f \n",d,D->vett_d[d].valore);
            }

            for(j=0;j<D->vett_d[d].numElem;j++){
                printf("%s ",E->vett_e[D->vett_d[d].elementi[j]].nome);//stampo nomi elementi nella diag

            }
            printf("\n\n");
        }
    }
}


//combinazioni con ripetizione
//scelgo combinazioni con ripetizione in quanto due raggruppamenti sono diversi se uno di essi contiene aleno un oggetto che non figura nell'altro oppure gli oggetti
//che figurano in uno figurano anche nell'altro ma sono ripetuti un numero diverso di volte
void comb_rip(elementi_t *E, diagonali_t *D, int DP, int pos, int *prog, int diff, int *bestprog, float *bestval, int *bestbonus, int *numd, int start) {
    int i,bonus;
    float val=0.0;
    if(pos>=3){//caso terminazione ho trovato 3 diagonali
        if (checkValido(E, D, prog, pos, &val, &bonus)==1) {
            if (val > *bestval) {//se il valore del prog trovato e' maggiore della best soluzione trovata fin ora, riinizzializzo tutti i valori
                *numd = pos; //nuovo numero di diagonali
                *bestval = val;//nuovo bestval
                *bestbonus = bonus;//nuovo bestbonus

                for(int i=0;i< *numd;i++){
                   bestprog[i]=prog[i];
                }

            }
        }
        return;
    }

    for(i=start;i<D->numDiag;i++) {//iterazione sulle scelte
        if (diff+D->vett_d[i].difficolta> DP) continue;//se la difficolta tot del programma e' maggiore di DP, scarto la soluzione, eseguo continue, itero il for con i incrementata
        prog[pos] = i; //scelta della diagonale per il prog
        comb_rip(E, D, DP, pos+1, prog, diff+D->vett_d[i].difficolta, bestprog, bestval, bestbonus, numd, i);//ricorsione sulla prossima posizione
    }
}


//controllo che la soluzione sia valida
int checkValido(elementi_t *E, diagonali_t *D, int *prog, int nd, float *val, int *B) {
    int i, j, bonus = -1;
    int avanti = 0, indietro = 0, seq = 0;

    for(i=0;i<nd;i++) {
        int dseq = 0;
        for(j=0;j<D->vett_d[prog[i]].numElem;j++) {//per tutti gli elementi
            if (E->vett_e[D->vett_d[prog[i]].elementi[j]].tipologia == 2) {//se l'elem e' acrobatico in avanti
                avanti = 1; //flag avanti a 1
                dseq++;
            } else if (E->vett_e[D->vett_d[prog[i]].elementi[j]].tipologia == 1) {//se l'elem e' acrobatico indietro
                indietro = 1;// flag indietro a 1
                dseq++;
            } else {//tipologia transizione
                dseq = 0; //gli elementi non sono piu' in sequenza. Come condizione in una diag devo avere almeno 2 elem in sequenza
            }

            // se il ginnasta include un elemento finale di difficoltà 8 o superiore nell'ultima diagonale presentata
            //in gara, il punteggio complessivo della diagonale viene moltiplicato per 1.5
            if (j==(D->vett_d[prog[i]].numElem-1) && (E->vett_e[D->vett_d[prog[i]].elementi[j]].tipologia > 0) && (E->vett_e[D->vett_d[prog[i]].elementi[j]].difficolta > 7)) {
                if (bonus == -1 || (D->vett_d[prog[bonus]].valore < D->vett_d[prog[i]].valore)) {
                    bonus = i;
                }
            }
            if (dseq >= 2) seq = 1;//il ginnasta deve presentare almeno una diagonale in cui compaiono almeno due elementi
            //acrobatici in sequenza
        }
    }
    if ((avanti==1) && (indietro==1) && (seq==1)) {//il ginnasta deve includere almeno un elemento acrobatico avanti e almeno un elemento acrobatico
        //indietro nel corso del suo programma, ma non necessariamente nella stessa diagonale. In piu' il ginnasta deve presentare almeno una diagonale in cui compaiono almeno due elementi
        //acrobatici in sequenza
        for(i=0;i<nd;i++) {
            if(i==bonus){
                (*val) += D->vett_d[prog[i]].valore * 1.50;
            }
            else{
                (*val) += D->vett_d[prog[i]].valore * 1.00;
            }
        }
        *B = bonus;
        return 1;//soluzione si puo' tenere
    }
    return 0;//soluzione da scartare perche' vincoli non sono soddisfatti
}

#include<stdio.h>
#include<stdlib.h>//per malloc

void solve(char *gemme, int *numGemme, int lungMax);
int fZ(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) ;
int fR(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) ;
int fT(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) ;
int fS(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) ;
int ****alloca4d(int *numGemme, int init);
int max (int a, int b);


int main(){
    int numTest, lungMax;
    int numGemme[4];//vettore che contiene il num di ogni pietra per ogni indice. numGemme[0]=2,numGemme[1]=1 ecc..
    char gemme[4]={'z','r','t','s'};//vettore char che indica le 4 pietre

    FILE *fp;
    fp=fopen("../hard_test_set.txt","r");//apertura file in lettura
    if(fp==NULL){//controllo corretta apertura file
        printf("errore apertura file\n");
        exit(-1);
    }

    fscanf(fp,"%d",&numTest);//leggo prima riga del file che indica quanti test andro' a fare
    for(int i=0;i<numTest;i++){
        printf("\nTest n:%d\n",i+1);//stampo numero del test
        lungMax=0;
        for(int j=0;j<4; j++){//prendo le 4 pietre della riga del file e le sommo tutte per trovare la lunghezza massima possibile
            fscanf(fp,"%d",&numGemme[j]);//salvo per ogni indice il numero di quel tipo di pietra che si ha a disposizione
            lungMax=lungMax+numGemme[j];
        }
        printf("La massima lunghezza possibile e':%d\n",lungMax);//max lunghezza possibile e' data dalla somma di tutte le pietre a disposizione
        solve(gemme,numGemme,lungMax);
    }
    fclose(fp);
    return 0;
}


void solve(char *gemme, int *numGemme, int lungMax){
    int maxZ, maxR, maxT,maxS, ris;
    int ****Z,****R,****S,****T;
    int z=numGemme[0],r=numGemme[1],t=numGemme[2],s=numGemme[3]; //indicano quanti tipi di pietre ho per ogni tipologia
    printf("Ci sono:\n");
    printf("zaffiri=%d rubini=%d topazi=%d smeraldi%d\n",z,r,t,s);

    //alloco matrici quadri-dimensionali per memoization. Una per ogni tipo di pietre da cui inizio a costruire la collana
    Z = alloca4d(numGemme, -1);
    R = alloca4d(numGemme, -1);
    T = alloca4d(numGemme, -1);
    S = alloca4d(numGemme, -1);

    //fZ, fR, fT, fS calcolo della lunghezza della collana piu' lunga iniziante rispettivamente con uno zaffiro, un rubino, un topazio
    // e uno smeraldo avendo a disposizione z zaffiri, r rubini, t topazi e s smeraldi
    maxZ = fZ(Z, R, T, S, z, r, t, s);
    maxR = fR(Z, R, T, S, z, r, t, s);
    maxT = fT(Z, R, T, S, z, r, t, s);
    maxS = fS(Z, R, T, S, z, r, t, s);

    //trovo la funzione che riporta la lunghezza massima della collana iniziate con pietre diverse
    if(maxT>maxS){
        ris=maxT;
        if(maxR>ris){
            ris=maxR;
            if(maxZ>ris){
                ris=maxZ;
            }
        }
    }
    else {
        ris = maxS;
        if (maxR > ris) {
            ris = maxR;
            if (maxZ > ris) {
                ris = maxZ;
            }
        }
    }

    printf("Collana massima di lunghezza %d\n", ris);
}
int ****alloca4d(int *numGemme, int init) {//struttura quadri-dimensionale dove ogni elemento indica quanto e' lunga la collana con le pietre indicate dai singoli indici
    int i, j, k, l;//z r t s

    int ****f;
    f = malloc((1+numGemme[0]) * sizeof(int***));//f variabile locale doppio puntatore, puntatore a vettore di puntatori(righe)
    for(i=0;i<=numGemme[0];i++) {
        f[i] = malloc((1+numGemme[1]) * sizeof(int**));//alloco vettore per rubini dentro la 2d
        for(j=0;j<=numGemme[1];j++) {
            f[i][j] = malloc((1+numGemme[2]) * sizeof(int*));//alloco vettore per topazi dentro la 3d
            for(k=0;k<=numGemme[2];k++) {
                f[i][j][k] = malloc((1+numGemme[3]) * sizeof(int));//alloco vettore per smeraldi dentro la 4d
                for(l=0;l<=numGemme[3];l++) {
                    f[i][j][k][l] = init;
                }
            }
        }
    }
    return f;
}


int fZ(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) {
    int nextZ, nextR;
    if (z <= 0)//se ho finito gli zaffiri
        return 0;
    if (Z[z][r][t][s] != -1)// se ha gia' un valore salvato-->memoization
        return Z[z][r][t][s];//ritorno il valore gia' salvato in precedenza
    /* Z seguito da Z o R */
    nextZ = fZ(Z,R,T,S,z-1,r,t,s);//tolgo uno z da quelli che ho a disposiziome perche' l'ho appena messo e la prossima mossa sara' mettere uno zaffiro
    nextR = fR(Z,R,T,S,z-1,r,t,s);//tolgo uno z da quelli che ho a disposiziome perche' l'ho appena messo e la prossima mossa sara' mettere un rubino
    Z[z][r][t][s] = 1+max(nextZ,nextR);//incremento la soluzione andando a prendere il valore massimo tra fz e fr
    return Z[z][r][t][s];//ritorno la soluzione
}

int fR(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) {
    int nextS, nextT;
    if (r <= 0)//se ho finito i rubini
        return 0;
    if (R[z][r][t][s] != -1)// se ha gia' un valore salvato-->memoization
        return R[z][r][t][s];//ritorno il valore gia' salvato in precedenza
    /* R seguito da S o T */
    nextS = fS(Z,R,T,S,z,r-1,t,s);//tolgo un r da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere uno smeraldo
    nextT = fT(Z,R,T,S,z,r-1,t,s);//tolgo un r da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere un topazio
     R[z][r][t][s] = 1+max(nextS,nextT);//incremento la soluzione andando a prendere il valore massimo tra fs e ft
    return R[z][r][t][s];//ritorno la soluzione
}

int fT(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) {
    int nextZ, nextR;
    if (t <= 0)//se ho finito i topazi
        return 0;
    if (T[z][r][t][s] != -1)// se ha gia' un valore salvato-->memoization
        return T[z][r][t][s];//ritorno il valore gia' salvato in precedenza
    /* T seguito da Z o R */
    nextZ = fZ(Z,R,T,S,z,r,t-1,s);//tolgo un t da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere uno zaffiro
    nextR = fR(Z,R,T,S,z,r,t-1,s);//tolgo un t da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere uno rubino
     T[z][r][t][s] = 1+max(nextZ,nextR);//incremento la soluzione andando a prendere il valore massimo tra fz e fr
    return T[z][r][t][s];//ritorno la soluzione
}

int fS(int ****Z, int ****R, int ****T, int ****S, int z, int r, int t, int s) {
    int nextS, nextT;
    if (s <= 0)//se ho finito gli smeraldi
        return 0;
    if (S[z][r][t][s] != -1)// se ha gia' un valore salvato-->memoization
        return S[z][r][t][s];//ritorno il valore gia' salvato in precedenza
    /* S seguito da S o T */
    nextS = fS(Z,R,T,S,z,r,t,s-1);//tolgo un s da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere uno s
    nextT = fT(Z,R,T,S,z,r,t,s-1);//tolgo un s da quello che ho a disposizione perche' l'ho appena messo e la prossima mossa sara' mettere uno t
    S[z][r][t][s] = 1+max(nextS,nextT);//incremento la soluzione andando a prendere il valore massimo tra fs e ft
    return S[z][r][t][s];//ritorno la soluzione
}

int max (int a, int b) {//trovo il massimo tra a e b
    if (a > b)
        return a;
    return b;
}

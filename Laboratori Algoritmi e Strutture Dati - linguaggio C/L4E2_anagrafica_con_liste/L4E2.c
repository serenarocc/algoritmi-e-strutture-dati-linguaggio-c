#include <stdio.h>
#include <string.h>
#include <stdlib.h> //per malloc

#define LEN1 50//lunghezza stringhe
#define LEN2 6//lunghezza per codice
#define LEN3 15 //lunghezza stringa data
#define NUM_SCELTE 8//scelte menu che vado a stampare

typedef struct data_struct{
    int gg;
    int mm;
    int aa;
} data_t;

typedef struct indirizzo_struct{
    char citta[LEN1];
    char via[LEN1];
    int cap;
}indirizzo_t;

typedef struct{
    char codice[LEN2];
    char nome[LEN1];
    char cognome[LEN1];
    char data_str[LEN3];//stringa di data di nascita
    data_t data;
    indirizzo_t indirizzo;
}Item;//chiamata come vuole il testo. Dato in lista

typedef struct nodo_struct *link;//def nodo 02cap4 sl 24

typedef struct nodo_struct{//def nodo 02cap4 sl 24
    Item val;
   link next;
}nodo_t;

int stampaMenu(char *scelte[]);
link caricaAnagrafica(link head, char *file, int i);
link insertOrdinato(link h,Item val);
int comparaData(data_t d1, data_t d2);
link newNode(Item val, link next);
Item leggiItem(FILE *fp);
void scomponiData(char strData[], data_t *data);
link aggiungi(link head);
void stampaAnagrafica(link head, char* file); /* char* file = NULL per stampare sullo stdout */
void stampaItem(Item it, FILE *fp);
void stampaIndirizzo(indirizzo_t ind, FILE *fp);
Item ricercaCodice(link head, char* codice);
Item elimina(link *head, char* codice);
Item eliminaTraDate(link *head, data_t d1, data_t d2);
void freeAnagrafica(link head);
Item ItemSetVoid(void);

int main() {

     char *scelte[] ={
           "Uscita",
           "Carica da file",
           "Aggiungi persona (tastiera)",
           "Stampa a video",
           "Stampa su file",
           "Ricerca per codice",
           "Elimina dato codice",
           "Elimina tra date"
     };
     int fineProgramma=0, selezione;
     link head = NULL; //puntatore alla testa
     char nomeFile[25],codiceRicerca[LEN1], data_str1[LEN3], data_str2[LEN3];
     Item it;
     data_t d1, d2;
     FILE *fp1;
     int i=0; //contatore righe file
     do{

        selezione=stampaMenu(scelte);
        switch(selezione){
            case 0:{// uscita dal programma
                fineProgramma=1;
            }break;
            case 1:{//leggi file
                printf("Inserisci nome file nel formato ../anang.txt\n");
                scanf("%s", nomeFile);
                fp1=fopen(nomeFile,"r");//apro file in lettura
                if(fp1 == NULL) { //controllo corretta apertura file
                    printf("Errore nell'apertura del file\n ");
                    exit(-1);
                }
                while(fscanf(fp1,"%s %s %s %s %s %s %d", it.codice, it.nome, it.cognome, it.data_str, it.indirizzo.via, it.indirizzo.citta, &it.indirizzo.cap)!=EOF)
                {
                    i++; //calcolo quante righe ha il file
                }
                fclose(fp1);
                head=caricaAnagrafica(head, nomeFile, i);
            }break;
            case 2:{//aggiungi persona
                head = aggiungi(head);

            }break;
            case 3: { //stampa a video
                stampaAnagrafica(head, NULL); //in questa funzione non ho il nome del file perchè devo stampare a vide, quindi passp NULL
            } break;
            case 4: { //stampa su file
                printf("Inserire nome file nel formato: ../output.txt\n");
                scanf("%s",nomeFile);
                stampaAnagrafica(head, nomeFile);
            } break;
            case 5:{//ricerca per codice (NON ELIMINAZIONE PER CODICE)
                printf("Inserire codice nel formato AXXXX: ");
                scanf("%s", codiceRicerca); //prendo da tastiera il codice da cercare
                it = ricercaCodice(head, codiceRicerca);
                   stampaItem(it, stdout); //stampo item a video
            } break;
            case 6: { //elimina dato per codice
                printf("Inserire codice: ");
                scanf("%s", codiceRicerca);
                it = elimina(&head, codiceRicerca);
                stampaItem(it, stdout); //stampo item a video

            } break;
            case 7: { //eliminazione elementi tra due date
                printf("Inserire data 1: ");
                scanf("%s", data_str1);
                printf("Inserire data 2: ");
                scanf("%s", data_str2);
                scomponiData(data_str1, &d1);
                scomponiData(data_str2, &d2);
                if (comparaData(d1, d2) > 0) {//se le date non sono messe in ordine cronologico, le inverto
                    data_t tmp;
                    tmp = d1;
                    d1 = d2;
                    d2 = tmp;
                }
                do {
                    it = eliminaTraDate(&head, d1, d2);
                    stampaItem(it, stdout);

                } while(fineProgramma==1);
            } break;
            default:{
                printf("Scelta non valida\n");
            } break;
        }
     }while(fineProgramma==0);
    freeAnagrafica(head);
    head = NULL;
     return 0;
}


int stampaMenu(char *scelte[]){//stampo a video il menù delle possibili scelte
    int selezione;
    printf("\nMenu'\n");
    for (int i=0; i<NUM_SCELTE; i++){
        printf("%2d > %s\n",i,scelte[i]);
    }
    scanf("%d",&selezione); //acquisisco da tastiera il comando e lo ritorno
return selezione;
}

link caricaAnagrafica(link head, char *file, int n){
    Item it;

    FILE *fp;
    fp=fopen(file,"r");//apro file in lettura
    if(fp == NULL) { //controllo corretta apertura file
        printf("Errore nell'apertura del file\n ");
        exit(-1);
    }
    for(int j=0; j<n; j++){
        it = leggiItem(fp);
        head = insertOrdinato(head, it);

   }
    fclose(fp);//chiusura file
    return head;//ritorno lista
}

Item leggiItem(FILE *fp) {//lettura dell'elemento da file
    Item it;

    if (fscanf(fp,"%s %s %s %s %s %s %d", it.codice, it.nome, it.cognome, it.data_str, it.indirizzo.via, it.indirizzo.citta, &it.indirizzo.cap)==7)
     {
        scomponiData(it.data_str, &(it.data)); //scompongo la data e la vado a salvare nella struttura
        return it;
    }
    return ItemSetVoid(); //e' come mettere un flag per definire un item vuoto
}

void scomponiData(char strData[], data_t *data) { //scomponi la data presa da file
    sscanf(strData, "%d/%d/%d", &data->gg, &data->mm, &data->aa);
    /*
     *  sscanf legge da una stringa
     *  "%d/%d/%d" quando non ha davanti % e legge un altro carattere, in questo caso 7, questo carattere viene saltato
     *
     *  int sscanf(const char *str, const char *format, ...)
     *  Parametri:
     *  str - Questa è la stringa C che la funzione elabora come fonte per recuperare i dati.
     *  format - Questa è la stringa C che contiene uno o più dei seguenti elementi: carattere di spazio bianco, carattere non di spazio bianco e identificatori di formato
      */
}

Item ItemSetVoid(void) { //item vuoto
    Item it = {"","",""}; /* codice nome e cognome vuoti */
    return it;
}

link insertOrdinato(link h, Item val) {//inserisco l'elemento nella lista in modo oridnato rispetto alla data
    link p = NULL, x = NULL;

    if(h==NULL || comparaData(h->val.data,val.data) > 0) //se sono alla testa della lista e la differenza di date è valida
        return newNode(val, h); //aggiungo nuovo nodo partendo dalla testa
    for (x=h->next, p=h; x!=NULL && comparaData(val.data, x->val.data) > 0; p=x, x=x->next); //continua fino a quando non sei alla fine della lista e fino a quando la differenza di date non è valida per inserire l'elem in modo ordinato per date nella lista
    p->next = newNode(val, x);
    return h;
}

int comparaData(data_t d1, data_t d2) {
    if (d1.aa != d2.aa)
        return (d1.aa-d2.aa); //riporto differenza anni
    else if (d1.mm != d2.mm)
        return (d1.mm-d2.mm); //riporto differenza mesi
    else if (d1.gg != d2.gg)
        return (d1.gg-d2.gg); //riporto differenza gg
    else return 0; //stessa data
}
link newNode(Item val, link next) {
    link x = malloc(sizeof(*x)); //alloco il nodo 02cap4 slide 28
    if (x == NULL)
        return NULL;
    x->val = val; //inserimento nuovo nodo nella lista
    x->next = next;
    return x;
}

link aggiungi(link head){
    Item it;
    printf("Inserisci elemento nella lista nel formato: \n ");
    printf(" Codice[AXXXX] Nome Cognome Data[gg/mm/aaaa] Via[scritto tutto attaccato] Citta Cap: \n");
    it = leggiItem(stdin);//lettura elem della struttura, questa volta non ho il nome del file quindi passo stdin al posto del nome del file
       head = insertOrdinato(head, it);
    return head;
}

void stampaAnagrafica(link head, char* file){ //passo come parametri la testa della lista e il file
    link x = NULL;
    FILE *fp;
    if(file == NULL)
        fp = stdout;  //caso in cui devo stampare a video
    else
        fp = fopen(file,"w"); //apro file in scrittura

    for(x = head; x != NULL; x=x->next) { //ciclo fino a quando non arrivo alla fine della lista ed incremento ogni passo di un nodo
        stampaItem(x->val, fp);
    }
    if(file!=NULL)
        fclose(fp);
}

void stampaItem(Item it, FILE *fp) {
    fprintf(fp, "%s, %s %s, %s ", it.codice, it.cognome, it.nome, it.data_str);
    stampaIndirizzo(it.indirizzo, fp);
}


void stampaIndirizzo(indirizzo_t ind, FILE *fp) {
    fprintf(fp, "%s %s %05d\n", ind.via, ind.citta, ind.cap);
}

//ricerca di una chiave 02Capitolo4 slide 63
Item ricercaCodice(link head, char* codice){//passo la testa della lista e il codice da cercare nella lista
    link x;
    for(x=head; x!=NULL; x=x->next){//scandisco tutta la lista elemento per elemento
        if(strcmp(x->val.codice, codice)==0){//Confronta strcmp()due stringhe carattere per carattere. Se le stringhe sono uguali, la funzione restituisce 0.
          //se la chiave c'e' si ritorna il dato che la contiene
            return x->val;
        }
    }
    return ItemSetVoid(); //se la chiave non c'è si ritorna il dato nullo
}

//02capitolo4-liste estrazione nodo con chiave data
Item elimina(link *head, char* codice) { //elimino item da lista
    link x,p;
    Item it;
    it=ItemSetVoid();//la funzione ritorna il dato nullo se la lista e' vuota o la chiave non e' presente
    for(x=*head, p=NULL;x!=NULL; p=x, x=x->next){
        //nel ciclo di attraversamentosi verifica se si trova la chiave, in caso affermativo se ne salva il puntatore e il dato,
        //si avanza nella lista ed infine si libera il nodo estratto
        if(strcmp(x->val.codice,codice)==0){
            if(p==NULL){
                *head=x->next;
            }else{
                p->next=x->next;
            }
            it=x->val;
            free(x);
            break;
        }
    }
    return it;
}

Item eliminaTraDate(link *head, data_t d1, data_t d2){
    link x, p;
    Item it;

    it=ItemSetVoid();//la funzione ritorna il dato nullo se la lista e' vuota o la chiave non e' presente
    for(x=*head, p=NULL; x!=NULL && comparaData(d2, x->val.data) >= 0; p=x, x=x->next) {
        if (comparaData(d1, x->val.data) <= 0) {
            if (p == NULL) {
                *head = x->next;
            } else {
                p->next=NULL;
            }
           it = x->val;
            free(x);
            break;
        }
    }
    return it;
}

void freeAnagrafica(link head) {
    link x, t;

    for(x = head; x != NULL; x=t) {
        t = x->next;
        free(x);
    }
}

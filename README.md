# Algoritmi e strutture dati -linguaggio C / Algorithms and Data Structures

La cartella Algoritmi e strutture dati -linguaggio C contiene lo svolgimento dei laboratori e di alcuni temi d'esame del corso di Algoritmi e strutture dati del Politecnico di Torino dell'anno 2022.

I possibili temi d'esame di programmazione sono di due tipo:
- prova da  12pt: primo esercizio riguarda la manipolazione di matrici, secondo esercizio coinvolge l'uso di liste, alberi o grafi e il terzo esercizio concerne l'utilizzo del calcolo combinatorio di probabilità;
- prova da 18pt: è costituita da un unico esercizio.


Linguaggio utilizzato: C.

L'ide impiegato è CLion 2022.2.

-----------------------------------------------------------------
The Algorithms and Data Structures directory contains some projects carried out during the course of Algorithms and Data Structures at the Politecnico di Torino in 2022.

There are two type of exames:
- type A: first exercise concerns the handling of matrices, second exercise involves the use of lists, trees or graphs and the third exercise concerns the use of combinatorial probability calculation;
- type B: consists of a single exercise.

Language used is C.

Ide used is CLion 2022.2.



## Link ad altri miei progetti personali / Links to other personal projects

- [ ] [Tecniche di Programmazione/ Programming Techniques](https://gitlab.com/serenarocc/tecniche-di-programmazione-linguaggio-c)
- [ ] [Algoritmi linguaggio C / Algorithms](https://gitlab.com/serenarocc/algoritmi-linguaggio-c)
- [ ] [Algoritmi e Strutture Dati / Algorithms and Data Structures](https://gitlab.com/serenarocc/algoritmi-e-strutture-dati-linguaggio-c)
- [ ] [Sistemi Operativi/ Operating Systems](https://gitlab.com/serenarocc/sistemi-operativi) 
- [ ] [Calcolatori Elettronici/Computer architecture](https://gitlab.com/serenarocc/calcolatori-elettronici-linguaggio-assembly-mips)
- [ ] [Basi di Dati /Databases](https://gitlab.com/serenarocc/basi-di-dati)
- [ ] [Programmazione a oggetti /Object Oriented Programming](https://gitlab.com/serenarocc/object-oriented-programming-java)
